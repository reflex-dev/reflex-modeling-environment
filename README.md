REFLEX Modeling Environment
===========================
The REFLEX Modeling Environment contains several command-line tools that ease
and speed-up application development with the REFLEX framework. It uses the
REFLEX modeling language (RML), a domain-specific language to express the
structure of applications and components.

User Documentation
------------------
[Documentation is provided at ReadTheDocs](http://rme.readthedocs.org).
We use Sphinx for generation.

Community
---------
 * Mailing List: reflex-developer(at)informatik.tu-cottbus.de
 * [Subscribe](mailto:majordomo@informatik.tu-cottbus.de?body=subscribe%20reflex-developer)
   to the mailing list.
 * [Mailing List Archive](http://www.mail-archive.com/reflex-developer@informatik.tu-cottbus.de/) at mail-archive.com

Version Control
---------------
We use git as version control system.

License
-------
Rme is being developed at the Brandenburg University of Technology, Distributed
Systems / Operating Systems Group. It is Free Libre Open Source Software available
under the GPL License. See the LICENSE file for details.
