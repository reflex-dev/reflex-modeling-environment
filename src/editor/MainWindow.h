/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "DragDropReflexModel.h"
#include "LibraryListModel.h"
#include ".moc/ui_MainWindow.h"

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QUndoGroup>

namespace editor
{

class MainWindow: public QMainWindow
{
Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void newFile();
	void open();
	bool save();
	bool saveAs();
	void setDocumentModified(bool isModified);
	void setDocumentFile(const QString& fileName = "");
	void settings();

public:
	static QUndoGroup undoGroup;

protected:
	void closeEvent(QCloseEvent* event);

private:
	void loadConfig();
	void loadFile(const QString& fileName);
	bool saveFile(const QString& fileName);
	bool saveAndContinue();

	void parseCommandLineOptions();

	Ui::MainWindowClass ui;
	LibraryListModel libraryModel;
	DragDropReflexModel appModel;
	QString currentFileName;
};

}


#endif // MAINWINDOW_H
