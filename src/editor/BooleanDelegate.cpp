/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "BooleanDelegate.h"
#include <QtGui>
#include <QtWidgets/QApplication>

static QRect CheckBoxRect(const QStyleOptionViewItem &view_item_style_options)
{
	QStyleOptionButton check_box_style_option;
	QRect check_box_rect = QApplication::style()->subElementRect(QStyle::SE_CheckBoxIndicator,
			&check_box_style_option);
	QPoint check_box_point(
			view_item_style_options.rect.x(),
			view_item_style_options.rect.y() + view_item_style_options.rect.height() / 2
					- check_box_rect.height() / 2);
	return QRect(check_box_point, check_box_rect.size());
}

BooleanDelegate::BooleanDelegate(QObject *parent, bool defaultValue) :
	QStyledItemDelegate(parent)
{
}

void BooleanDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.flags() == Qt::NoItemFlags)
	{
		return;
	}

	bool checked = index.model()->data(index, Qt::DisplayRole).toBool();

	QStyleOptionButton check_box_style_option;
	check_box_style_option.state |= QStyle::State_Enabled;
	if (checked)
	{
		check_box_style_option.state |= QStyle::State_On;
	}
	else
	{
		check_box_style_option.state |= QStyle::State_Off;
	}
	check_box_style_option.rect = CheckBoxRect(option);

	QApplication::style()->drawControl(QStyle::CE_CheckBox, &check_box_style_option, painter);
}

bool BooleanDelegate::editorEvent(QEvent *event, QAbstractItemModel *model,
		const QStyleOptionViewItem &option, const QModelIndex &index)
{
	if ((event->type() == QEvent::MouseButtonRelease) || (event->type() == QEvent::MouseButtonDblClick))
	{
		QMouseEvent *mouse_event = static_cast<QMouseEvent*> (event);
		if (mouse_event->button() != Qt::LeftButton || !CheckBoxRect(option).contains(mouse_event->pos()))
		{
			return false;
		}
		if (event->type() == QEvent::MouseButtonDblClick)
		{
			return true;
		}
	}
	else if (event->type() == QEvent::KeyPress)
	{
		if (static_cast<QKeyEvent*> (event)->key() != Qt::Key_Space && static_cast<QKeyEvent*> (event)->key()
				!= Qt::Key_Select)
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	bool checked = index.model()->data(index, Qt::DisplayRole).toBool();
	return model->setData(index, !checked, Qt::EditRole);
}
