/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _READONLYREFLEXMODEL_H
#define _READONLYREFLEXMODEL_H

#include <QtCore/QVariant>
#include "ReflexModelFragment.h"
#include <QtCore/QScopedPointer>
#include <QtCore/QAbstractItemModel>

class ReflexModelItem;
class ReflexModelFragmentIterator;

class ReadOnlyReflexModel: public QAbstractItemModel, public ReflexModelFragment
{
public:
	/*!

	 */
	enum Column
	{
		InvalidColumn = -1, NameColumn = 0, TypenameColumn, T1Column, T2Column, T3Column, DestinationColumn, NrOfColumns
	};

	ReadOnlyReflexModel();

	int columnCount(const QModelIndex & parent) const;

	QVariant data(const QModelIndex & index, int role) const;

	Qt::ItemFlags flags(const QModelIndex & index) const;

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	QModelIndex index(int row, int column, const QModelIndex & parent) const;

	QModelIndex parent(const QModelIndex & index) const;

	int rowCount(const QModelIndex & parent) const;

	void setModel(const ReflexModelFragment & model);

	static int columnForKey(const QString& key);
	static QString keyForColumn(int column);

protected:
	QModelIndex indexForItem(ReflexModelItem * item, int column = NameColumn) const;
	int rowForItem(ReflexModelItem * node) const;

	ReflexModelItem* itemForIndex(const QModelIndex & index) const;

};
#endif
