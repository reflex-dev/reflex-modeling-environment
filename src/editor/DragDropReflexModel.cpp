/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "DragDropReflexModel.h"
#include "TypeLibrary.h"

#include <QtCore/QDataStream>
#include <QtCore/QMimeData>
#include <QtCore/QStringList>



Qt::ItemFlags DragDropReflexModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = EditableReflexModel::flags(index);

	if (!index.isValid())
	{
		return flags | Qt::ItemIsDropEnabled;
	}

	ReflexModelItem* node = itemForIndex(index);
	if (!node->value("built-in").toBool() && (node->type() == ComponentItem))
	{
		flags |= Qt::ItemIsDropEnabled;
	}

	if (flags & Qt::ItemIsEditable)
	{
		flags |= Qt::ItemIsDragEnabled;
	}

	return flags;
}

Qt::DropActions DragDropReflexModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction;
}


bool DragDropReflexModel::dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column,
		const QModelIndex& parent)
{
	Q_UNUSED(row);
	Q_UNUSED(column);

	if (action == Qt::IgnoreAction)
	{
		return true;
	}
	else if (data->hasFormat("application/rme-model-item"))
	{
		/* Accept drops from the library */
		QByteArray encodedData = data->data("application/rme-model-item");
		QDataStream stream(&encodedData, QIODevice::ReadOnly);
		ReflexModelItem* parentItem = itemForIndex(parent);

		if (row == -1)
		{
			row = parentItem->children().size();
		}

		for (; !stream.atEnd(); row++)
		{
			quintptr addr;
			stream >> addr;
			ReflexModelItem* item = reinterpret_cast<ReflexModelItem*> (addr);
			if (action == Qt::CopyAction)
			{
				ReflexModelFragment::insert(clone(item), parentItem, row);
			}
			else
			{
				move(item, parentItem, row);
			}
		}
		return true;
	}
	else if (data->hasFormat("application/rme-library-item"))
	{
		QByteArray encodedData = data->data("application/rme-library-item");
		QDataStream stream(&encodedData, QIODevice::ReadOnly);
		ReflexModelItem* parentItem = itemForIndex(parent);

		if (row == -1)
		{
			row = parentItem->children().size();
		}

		for (; !stream.atEnd(); row++)
		{
			QHash<QString, QVariant> info;
			stream >> info;
			const QString typeName = info.value("type-name").toString();
			const QString name = info.value("default-name").toString();
			const QString T1 = info.value("T1").toString();
			const QString T2 = info.value("T2").toString();
			const QString T3 = info.value("T3").toString();
			ReflexModelFragment fragment = _library->instanciate(typeName, name, T1, T2, T3);
			ReflexModelFragment::insert(fragment, parentItem, row);
		}
		return true;
	}
	else
	{
		return false;
	}

	return true;
}

QMimeData* DragDropReflexModel::mimeData(const QModelIndexList& indexes) const
{
	QMimeData* mimeData = new QMimeData();
	QByteArray data;
	QDataStream out(&data, QIODevice::WriteOnly);

	foreach (const QModelIndex& index, indexes)
		{
			if (index.column() != NameColumn)
			{
				continue;
			}
			if (index.isValid())
			{
				quintptr addr = reinterpret_cast<quintptr> (index.internalPointer());
				out << addr;
			}
		}

	mimeData->setData("application/rme-model-item", data);
	return mimeData;
}

QStringList DragDropReflexModel::mimeTypes() const
{
	return QStringList() << "application/rme-library-item" << "application/rme-model-item";
}
