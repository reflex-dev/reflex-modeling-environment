/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "LibraryListModel.h"
#include <QtCore/QDataStream>
#include <QtCore/QMimeData>
#include <QtCore/QIODevice>

LibraryListModel::LibraryListModel(QObject *parent) :
	QAbstractListModel(parent)
{
}

int LibraryListModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 1;
}

QVariant LibraryListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole)
		return QVariant();

	QString type = _library.values().at(index.row()).root()->value("type-name").toString();
	return type;
}

Qt::ItemFlags LibraryListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
	{
		return 0;
	}

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

QVariant LibraryListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(section);

	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		return tr("Typename");
	}

	return QVariant();
}

int LibraryListModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return _library.size();
}

QModelIndex LibraryListModel::index(int row, int column, const QModelIndex& parent) const
{
	if (hasIndex(row, column, parent))
	{
		ReflexModelFragment* fragment = fragmentForRow(row);
		return createIndex(row, column, fragment);
	}
	return QModelIndex();
}

QMimeData* LibraryListModel::mimeData(const QModelIndexList& indexes) const
{
	QMimeData* mimeData = new QMimeData();
	QByteArray data;
	QDataStream out(&data, QIODevice::WriteOnly);

	foreach (const QModelIndex& index, indexes)
		{
			if (index.isValid())
			{
				int row = index.row();
				Map::ConstIterator i = iteratorForRow(row);
				out << i.value().root()->values();
			}
		}

	mimeData->setData("application/rme-library-item", data);
	return mimeData;
}

LibraryListModel::Map::Iterator LibraryListModel::insert(const QString& type, const ReflexModelFragment& model)
{
	Q_ASSERT(!type.isEmpty());

	uint row = 0;
	for (Map::ConstIterator i = _library.constBegin(); i != _library.constEnd(); i++)
	{
		if (i.key() == type)
		{
			break;
		}
		row++;
	}

	beginInsertRows(QModelIndex(), row, row);
	Map::Iterator position = TypeLibrary::insert(type, model);
	endInsertRows();
	return position;
}

void LibraryListModel::remove(const QString& type)
{
	uint row = 0;
	for (Map::ConstIterator i = _library.constBegin(); i != _library.constEnd(); i++)
	{
		if (i.key() == type)
		{
			beginRemoveRows(QModelIndex(), row, row);
			TypeLibrary::remove(type);
			endRemoveRows();
			return;
		}
		row++;
	}
}

void LibraryListModel::setModel(const TypeLibrary& library)
{
	beginResetModel();
	replaceWith(library);
	endResetModel();
}

ReflexModelFragment* LibraryListModel::fragmentForRow(int row) const
{
	Map::ConstIterator i = iteratorForRow(row);
	return const_cast<ReflexModelFragment*> (&i.value());
}

TypeLibrary::Map::ConstIterator LibraryListModel::iteratorForRow(int row) const
{
	Map::ConstIterator i = _library.constBegin();
	for (int currentRow = 0; currentRow < row; currentRow++)
	{
		i++;
	}
	return i;
}


