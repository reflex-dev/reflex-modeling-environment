/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef DRAGDROPREFLEXMODEL_H_
#define DRAGDROPREFLEXMODEL_H_

#include "EditableReflexModel.h"

/*!
Enhances EditableReflexModel with support for Drag & Drop.
 */
class DragDropReflexModel : public EditableReflexModel
{
public:
	bool dropMimeData(const QMimeData*data, Qt::DropAction action, int row, int column,
			const QModelIndex& parent);

	Qt::ItemFlags flags(const QModelIndex &index) const;
	QMimeData* mimeData(const QModelIndexList& indexes) const;
	QStringList mimeTypes() const;
	Qt::DropActions supportedDropActions() const;

};


#endif /* DRAGDROPREFLEXMODEL_H_ */
