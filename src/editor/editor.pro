! include( ../../common.pri ) {
    error( "Couldn't find the common.pri file!" )
}

TEMPLATE = app
TARGET = rme-editor

CONFIG += \
    resources \
    uic
    
QT += \
    gui \
    svg \
    widgets
    
LIBS += \
    -L$$PWD/../lib -lrme
     
PRE_TARGETDEPS += \
    ../lib/librme.a \
    
INCLUDEPATH += ../lib

HEADERS = DragDropReflexModel.h \
    ReflexModelTreeView.h \
    BooleanDelegate.h \
    ReflexModelItemDelegate.h \
    LibraryListModel.h \
    MainWindow.h \
    EditableReflexmodel.h \
    SettingsDialog.h
    
SOURCES += DisconnectCommand.cpp \
    ConnectCommand.cpp \
    DragDropReflexModel.cpp \
    MoveCommand.cpp \
    ReflexModelTreeView.cpp \
    BooleanDelegate.cpp \
    ReflexModelItemDelegate.cpp \
    ReflexModelCommand.cpp \
    RemoveCommand.cpp \
    InsertCommand.cpp \
    EditableReflexModel.cpp \
    LibraryListModel.cpp \
    MainWindow.cpp \
    ReadOnlyReflexModel.cpp \
    SetCommand.cpp \
    SettingsDialog.cpp \
    main.cpp
    
FORMS = \
    MainWindow.ui \
    SettingsDialog.ui
    
RESOURCES += editor_resources.qrc
