/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef LIBRARYTREEMODEL_H_
#define LIBRARYTREEMODEL_H_

#include <QtCore/QAbstractListModel>
#include <QtCore/QModelIndex>
#include <QtCore/QScopedPointer>
#include <QtCore/QVariant>

#include "TypeLibrary.h"

class LibraryListModel: public QAbstractListModel, public TypeLibrary
{
Q_OBJECT

public:
	LibraryListModel(QObject *parent = 0);

	/* Reimplemented from AbstractItemModel */
	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QMimeData* mimeData(const QModelIndexList& indexes) const;

	/* Reimplemented from TypeLibrary */
	Map::Iterator insert(const QString& type, const ReflexModelFragment& model);
	void remove(const QString& type);
	void setModel(const TypeLibrary& library);


protected:
	ReflexModelFragment* fragmentForRow(int row) const;
	Map::ConstIterator iteratorForRow(int row) const;
};

#endif /* LIBRARYTREEMODEL_H_ */
