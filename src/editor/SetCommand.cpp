/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "EditableReflexModel.h"
#include "ReflexModelItem.h"
#include "SetCommand.h"

SetCommand::SetCommand(EditableReflexModel* model, ReflexModelItem* item, const QString& key,
		const QVariant& value) :
	ReflexModelCommand(model)
{
	this->item = item;
	this->key = key;
	this->value = value;
	if (key == "name")
	{
		setText(QString("Rename %1").arg(item->name()));
	}
	else if (key == "hasActivity")
	{
		if (value.toBool() == true)
		{
			setText(QString("Mark %1 as activity").arg(item->name()));
		}
		else
		{
			setText(QString("Unmark %1 as activity").arg(item->name()));
		}
	}
	else
	{
		int column = model->columnForKey(key);
		QString keyName = model->headerData(column, Qt::Horizontal, Qt::DisplayRole).toString();
		setText(QString("Change %1 of %2").arg(keyName).arg(item->name()));
	}
}

void SetCommand::redo()
{
	QVariant oldValue = item->value(key);
	model->ReflexModelFragment::setValue(item, key, value);
	value = oldValue;
	model->emitDataChanged(item, key);
}

void SetCommand::undo()
{
	QVariant oldValue = item->value(key);
	model->ReflexModelFragment::setValue(item, key, value);
	value = oldValue;
	model->emitDataChanged(item, key);
}
