/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelItemDelegate.h"
#include "EditableReflexModel.h"
#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtCore/QObject>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMenu>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QTreeView>

ReflexModelItemDelegate::ReflexModelItemDelegate(EditableReflexModel* model, QTreeView* view) :
	QStyledItemDelegate(view), model(model), view(view)
{
	actions << new QAction(trUtf8("Insert new"), this);
	actions << new QAction(trUtf8("Edit"), this);
	actions << new QAction(trUtf8("Cut"), this);
	actions << new QAction(trUtf8("Copy"), this);
	actions << new QAction(trUtf8("Paste"), this);
	actions << new QAction(trUtf8("Delete Item"), this);
	actions << new QAction(trUtf8("Rename"), this);
	actions << new QAction(trUtf8("Properties"), this);
	actions << new QAction(trUtf8("Change type"), this);

	actions[Edit]->setShortcut(Qt::Key_Return);
	actions[Delete]->setShortcut(QKeySequence::Delete);
	actions[Properties]->setShortcut(Qt::ALT + Qt::Key_Enter);
	actions[Cut]->setShortcut(QKeySequence::Cut);
	actions[Copy]->setShortcut(QKeySequence::Copy);
	actions[Paste]->setShortcut(QKeySequence::Paste);

	//contextMenu.addAction(actions[New]);
	contextMenu.addAction(actions[Edit]);
	contextMenu.addAction(actions[Delete]);
	//_menu.addAction(_actions[Rename]);
	//_menu.addAction(_actions[ChangeType]);
	//contextMenu.addSeparator();
	//contextMenu.addAction(actions[Cut]);
	//contextMenu.addAction(actions[Copy]);
	//contextMenu.addAction(actions[Paste]);
	//contextMenu.addSeparator();
	//contextMenu.addAction(actions[Properties]);
	foreach(QAction* action, actions)
		{
			action->setEnabled(false);
			connect(action, SIGNAL(triggered(bool)), this, SLOT(onActionTriggered(bool)));
		}

}

QWidget* ReflexModelItemDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option,
		const QModelIndex &index) const
{
	Q_UNUSED(option);
	Q_UNUSED(index);
	QLineEdit* editor = new QLineEdit(parent);
	return editor;
}

void ReflexModelItemDelegate::showContextMenu(const QPoint& pos)
{
	QModelIndexList indexes = view->selectionModel()->selectedIndexes();

	if (indexes.isEmpty())
	{
		return;
	}

	QModelIndex index = indexes.first();
	ReflexModelItem* item = static_cast<ReflexModelItem*> (index.internalPointer());

	actions[Properties]->setEnabled(true);

	if ((item->value("built-in").toBool() == false) && (item->type() == ComponentItem))
	{
		actions[New]->setEnabled(true);
	}

	if (index.flags() & Qt::ItemIsEditable)
	{
		actions[Edit]->setEnabled(true);
		actions[Delete]->setEnabled(true);
		actions[Cut]->setEnabled(true);
	}
	actions[Copy]->setEnabled(true);

	QAction* action = contextMenu.exec(view->mapToGlobal(pos));

	foreach(QAction* action, actions)
		{
			action->setEnabled(false);
			action->setChecked(false);
		}
}

void ReflexModelItemDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	QString value = index.data(Qt::EditRole).toString();
	QLineEdit* edit = static_cast<QLineEdit*> (editor);
	edit->setText(value);
}

void ReflexModelItemDelegate::setModelData(QWidget* editor, QAbstractItemModel*, const QModelIndex& index) const
{
	QLineEdit* edit = static_cast<QLineEdit*> (editor);
	QString value = edit->text();
	ReflexModelItem* item = static_cast<ReflexModelItem*> (index.internalPointer());

	QString key = ReadOnlyReflexModel::keyForColumn(index.column());
	if (item->value(key) != value)
	{
		model->setValue(item, key, value);
	}
}

void ReflexModelItemDelegate::onActionTriggered(bool checked)
{
	QModelIndexList indexes = view->selectionModel()->selectedIndexes();

	if (indexes.isEmpty())
	{
		return;
	}

	QModelIndex index = indexes.first();
	ReflexModelItem* item = static_cast<ReflexModelItem*> (index.internalPointer());
	QAction* action = static_cast<QAction*> (sender());

	switch (actions.indexOf(action))
	{
	case Delete:
		model->remove(item);
		break;
	case Edit:
		view->edit(index);
		break;
	case Rename:
		view->edit(index.sibling(index.row(), ReadOnlyReflexModel::NameColumn));
		break;
	case ChangeType:
		view->edit(index.sibling(index.row(), ReadOnlyReflexModel::TypenameColumn));
		break;
	case Properties:
		break;
	case Cut:
		break;
	default:
		break;
	}
}

