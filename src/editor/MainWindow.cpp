/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "BooleanDelegate.h"
#include "ReflexModelItemDelegate.h"
#include "MainWindow.h"
#include "ReflexModelScriptReader.h"
#include "ReflexModelScriptWriter.h"
#include "SettingsDialog.h"

#include <QtCore/QCommandLineOption>
#include <QtCore/QCommandLineParser>
#include <QtCore/QtGlobal>
#include <QtCore/QFile>
#include <QtCore/QSettings>
#include <QtCore/QTimer>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QUndoView>
#include <QtCore/QMetaObject>

#include <QDebug>

using namespace editor;

QUndoGroup MainWindow::undoGroup;

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	parseCommandLineOptions();
	loadConfig();

	ui.setupUi(this);
	setCentralWidget(ui.treeView);
	appModel.setLibrary(&libraryModel);
	//connect(thread(), SIGNAL(started()), this, SLOT(setCurrentFile()));
	QTimer::singleShot(1, this, SLOT(setDocumentFile()));

	/* Load window settings and geometry */
	QSettings settings;
	restoreGeometry(settings.value("mainwindow/geometry").toByteArray());
	restoreState(settings.value("mainwindow/state").toByteArray());
	setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
	setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
	ui.treeView->header()->restoreState(settings.value("apptreeview/state").toByteArray());

	/* File menu */
	QMenu* fileMenu = menuBar()->addMenu(trUtf8("&File"));
	fileMenu->addAction(tr("&New"), this, SLOT(newFile()), QKeySequence::New);
	fileMenu->addAction(tr("&Open..."), this, SLOT(open()), QKeySequence::Open);
	fileMenu->addSeparator();
	fileMenu->addAction(tr("Sa&ve"), this, SLOT(save()), QKeySequence::Save);
	fileMenu->addAction(tr("Save &As..."), this, SLOT(saveAs()), QKeySequence::SaveAs);
	fileMenu->addSeparator();
	fileMenu->addAction(tr("&Quit"), this, SLOT(close()), QKeySequence::Quit);

	/* Edit menu */
	QMenu* editMenu = menuBar()->addMenu(trUtf8("&Edit"));
	QAction* undoAction = undoGroup.createUndoAction(editMenu);
	QAction* redoAction = undoGroup.createRedoAction(editMenu);
	undoAction->setShortcut(QKeySequence::Undo);
	redoAction->setShortcut(QKeySequence::Redo);
	editMenu->addAction(undoAction);
	editMenu->addAction(redoAction);

	/* Tools Menu */
	QMenu* toolsMenu = menuBar()->addMenu(trUtf8("&Tools"));
	toolsMenu->addAction(tr("&Settings"), this, SLOT(settings()), QKeySequence::Preferences);

	/* Application model */
	ui.treeView->setModel(&appModel);
	ReflexModelItemDelegate* modelItemDelegate = new ReflexModelItemDelegate(&appModel, ui.treeView);
	connect(ui.treeView, SIGNAL(customContextMenuRequested(const QPoint&)), modelItemDelegate,
			SLOT(showContextMenu(const QPoint&)));
	ui.treeView->setContextMenuPolicy(Qt::CustomContextMenu);
	ui.treeView->setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::AnyKeyPressed);
	ui.treeView->setSelectionBehavior(QAbstractItemView::SelectItems);
	ui.treeView->setItemDelegate(modelItemDelegate);
	//BooleanDelegate* hasActivityDelegate = new BooleanDelegate(this);
	//ui.treeView->setDragDropMode(QAbstractItemView::DragDrop);
	ui.treeView->setDefaultDropAction(Qt::MoveAction);
	ui.treeView->setDragEnabled(true);
	ui.treeView->setAcceptDrops(true);
	ui.treeView->setDropIndicatorShown(true);
    //appModel.setSupportedDragActions(Qt::CopyAction | Qt::MoveAction);
	//ui.treeView->setTabKeyNavigation(true);

	//	connect(&appModel, SIGNAL(layoutChanged()), this, SLOT(documentWasModified()));
	//	connect(&appModel, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), this,
	//			SLOT(documentWasModified()));
	//	connect(&appModel, SIGNAL(rowsInserted (const QModelIndex&, int, int)), this, SLOT(documentWasModified()));
	//	connect(&appModel, SIGNAL(rowsRemoved(const QModelIndex&, int, int)), this, SLOT(documentWasModified()));
	connect(&appModel, SIGNAL(documentModified(bool)), this, SLOT(setDocumentModified(bool)));

	/* Library model */
	ui.libraryListView->setModel(&libraryModel);
	ui.libraryListView->setDragDropMode(QAbstractItemView::DragOnly);
	ui.libraryListView->setDragEnabled(true);
}

MainWindow::~MainWindow()
{
	/* Save window settings */
	QSettings settings;
	settings.setValue("mainwindow/state", saveState());
	settings.setValue("mainwindow/geometry", saveGeometry());
	settings.setValue("apptreeview/state", ui.treeView->header()->saveState());
	//	settings.setValue("apptreeview/geometry", ui.treeView->header()->saveGeometry());
}

void MainWindow::loadConfig()
{
	QSettings settings;
	QStringList librarySearchPath = settings.value("libpath").toStringList();
	ReflexModelScriptReader::importPaths = librarySearchPath;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	if (saveAndContinue())
	{
		event->accept();
	}
	else
	{
		event->ignore();
	}
}

void MainWindow::newFile()
{
	if (saveAndContinue())
	{
		appModel.setModel(ReflexModelFragment::defaultApplicationModel());
		setDocumentFile("");
	}
}

void MainWindow::setDocumentModified(bool isModified)
{
	if (isWindowModified() != isModified)
	{
		setWindowModified(isModified);
	}
}

bool MainWindow::saveAndContinue()
{
	if (isWindowModified())
	{
		QMessageBox::StandardButton ret;
		ret = QMessageBox::warning(this, QApplication::applicationName(),
				tr("The document has been modified.\n"
					"Do you want to save your changes?"),
				QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
		if (ret == QMessageBox::Save)
		{
			return save();
		}
		else if (ret == QMessageBox::Cancel)
		{
			return false;
		}
	}
	return true;
}

void MainWindow::open()
{
	if (!saveAndContinue())
	{
		return;
	}

	QSettings settings;
	QString lastPath = settings.value("lastOpenFolder", QDir::homePath()).toString();

	QString fileName = QFileDialog::getOpenFileName(this, QString(), lastPath, "*.rml");
	//QString fileName = "/home/rw/workspace/ReflexModellingTools/src/editor/test.rml";

	if (fileName.isEmpty())
	{
		return;
	}

	loadFile(fileName);
	settings.setValue("lastOpenFolder", QFileInfo(fileName).dir().absolutePath());
}

bool MainWindow::save()
{
	if (QFileInfo(currentFileName).exists() && saveFile(currentFileName))
	{
		setWindowModified(false);
		return true;
	}
	else
	{
		return saveAs();
	}
}

bool MainWindow::saveAs()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), currentFileName,
			tr("Model files (%1)").arg("*.rml"));
	if (fileName.isEmpty())
	{
		return false;
	}

	if (saveFile(fileName))
	{
		setWindowModified(false);
	}

	QSettings settings;
	setDocumentFile(fileName);
	settings.setValue("lastOpenFolder", QFileInfo(fileName).dir().absolutePath());

	return true;
}

void MainWindow::loadFile(const QString& fileName)
{
	TypeLibrary library;
	ReflexModelScriptReader reader;
	reader.setLibrary(&library);
	QApplication::setOverrideCursor(Qt::WaitCursor);
	reader.setModelFile(fileName);
	QApplication::restoreOverrideCursor();

	if (reader.hasErrors())
	{
		QMessageBox::warning(this, QApplication::applicationName(),
				tr("Error reading model file:\n%1.").arg(reader.errorString()));
		return;
	}

	libraryModel.setModel(library);
	appModel.setModel(reader.model());

	setDocumentFile(fileName);
	statusBar()->showMessage(tr("File loaded"), 2000);

	ui.treeView->expandToDepth(0);
}

bool MainWindow::saveFile(const QString& fileName)
{
	QFile file(fileName);

	if (!file.open(QIODevice::WriteOnly))
	{
		QMessageBox::warning(this, QApplication::applicationName(),
				tr("Cannot write file %1:\n%2.") .arg(fileName) .arg(file.errorString()));
		return false;
	}

	ReflexModelScriptWriter writer(&file);
	QApplication::setOverrideCursor(Qt::WaitCursor);
	writer.writeModel(appModel);
	QApplication::restoreOverrideCursor();

	if (writer.hasErrors())
	{
		QMessageBox::warning(this, QApplication::applicationName(),
				trUtf8("Failed to write model to %1. %2").arg(file.fileName()).arg(writer.errorString()));
		return false;
	}

	statusBar()->showMessage(tr("File saved"), 2000);

	return true;
}

void MainWindow::setDocumentFile(const QString& fileName)
{
	static int sequenceNumber = 1;

	if (fileName.isEmpty())
	{
		currentFileName = tr("untitled%1.rml").arg(sequenceNumber++);
	}
	else
	{
		currentFileName = QFileInfo(fileName).canonicalFilePath();
	}

	setWindowModified(false);
	setWindowFilePath(currentFileName);
}

void MainWindow::parseCommandLineOptions()
{
	QCommandLineParser parser;
	parser.addHelpOption();
    parser.process(*QApplication::instance());
}

void MainWindow::settings()
{
	SettingsDialog dialog;
	if (dialog.exec() == QDialog::Accepted)
	{
		dialog.saveSettings();
		loadConfig();
	}
}

