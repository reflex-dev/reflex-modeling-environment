/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _EDITABLEREFLEXMODEL_H
#define _EDITABLEREFLEXMODEL_H

#include "ReadOnlyReflexModel.h"
#include <QtWidgets/QUndoStack>

class TypeLibrary;

/*!
Enhances ReadOnlyReflexModel with undo/redo functionality.
 */
class EditableReflexModel: public ReadOnlyReflexModel
{
Q_OBJECT

	friend class ReflexModelCommand;
	friend class MoveCommand;
	friend class InsertCommand;
	friend class RemoveCommand;
	friend class SetCommand;

public:
	EditableReflexModel();
	void setLibrary(TypeLibrary* library);
	void setModel(const ReflexModelFragment& fragment);
	QUndoStack* undoStack();

	/* Methods that implement the reflex-model interface */
	ReflexModelConnection connect(ReflexModelItem* sourceItem, ReflexModelItem* destinationItem);
	void disconnect(const ReflexModelConnection& connection);
	void insert(const QSharedPointer<ReflexModelItem>& item, ReflexModelItem* parent, int row = -1);
	void move(ReflexModelItem* item, ReflexModelItem* parentItem, int row);
	void remove(ReflexModelItem* item);
	void setValue(ReflexModelItem* item, const QString& key, const QVariant& value);

	/* Methods that re-implement QAsbtractItemModel */
	Qt::ItemFlags flags(const QModelIndex &index) const;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);

signals:
	void documentModified(bool);

protected slots:
	void changeCleanState(bool isClean);

protected:
	void emitDataChanged(ReflexModelItem* item, const QString& key);

	QUndoStack _undoStack;
	TypeLibrary* _library;
};
#endif
