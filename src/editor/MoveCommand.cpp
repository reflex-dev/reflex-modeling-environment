/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "EditableReflexModel.h"
#include "MoveCommand.h"
#include "ReflexModelItem.h"

MoveCommand::MoveCommand(EditableReflexModel* model, ReflexModelItem* item, ReflexModelItem* parentItem,
		int row) :
	ReflexModelCommand(model)
{
	this->item = item;
	this->dstParentItem = parentItem;
	this->srcParentItem = item->parent();
	this->dstRow = row;
	this->srcRow = srcParentItem->children().indexOf(item);
}

void MoveCommand::redo()
{
	model->beginMoveRows(model->indexForItem(srcParentItem), srcRow, srcRow,
			model->indexForItem(dstParentItem), dstRow);
	if ((srcParentItem == dstParentItem) && (dstRow > srcRow))
	{
		model->ReflexModelFragment::move(item, dstParentItem, dstRow - 1);
	}
	else
	{
		model->ReflexModelFragment::move(item, dstParentItem, dstRow);
	}
	model->endMoveRows();
}

void MoveCommand::undo()
{
	if (srcParentItem == dstParentItem)
	{
		if (dstRow > srcRow)
		{
			model->beginMoveRows(model->indexForItem(dstParentItem), dstRow - 1, dstRow - 1,
					model->indexForItem(srcParentItem), srcRow);
		}
		else
		{
			model->beginMoveRows(model->indexForItem(dstParentItem), dstRow, dstRow,
					model->indexForItem(srcParentItem), srcRow + 1);
		}
	}
	else
	{
		model->beginMoveRows(model->indexForItem(dstParentItem), dstRow, dstRow,
				model->indexForItem(srcParentItem), srcRow);
	}
	model->ReflexModelFragment::move(item, srcParentItem, srcRow);
	model->endMoveRows();
}
