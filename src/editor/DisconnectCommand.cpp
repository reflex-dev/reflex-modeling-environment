/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "DisconnectCommand.h"
#include "EditableReflexModel.h"


DisconnectCommand::DisconnectCommand(EditableReflexModel* model, const ReflexModelConnection& connection) :
	ReflexModelCommand(model), connection(connection)
{
}

void DisconnectCommand::redo()
{
	parent = connection.parent();
	model->ReflexModelFragment::disconnect(connection);
}

void DisconnectCommand::undo()
{
	parent->_connections.insert(connection);
	connection.setAttached(true);
}
