/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelItem.h"
#include "ReflexModelQuery.h"
#include "ReadOnlyReflexModel.h"

#include <QtGui/QIcon>

ReadOnlyReflexModel::ReadOnlyReflexModel() :
	ReflexModelFragment(ReflexModelFragment::defaultApplicationModel())
{

}

void ReadOnlyReflexModel::setModel(const ReflexModelFragment& model)
{
	Q_ASSERT(model.root()); // model must not be empty!

	beginResetModel();
	ReflexModelFragment::replace(0, model.pointerToItem(model.root()));
	endResetModel();
}

int ReadOnlyReflexModel::columnCount(const QModelIndex& parent) const
{
	Q_UNUSED(parent);
	return NrOfColumns;
}

QVariant ReadOnlyReflexModel::data(const QModelIndex& index, int role) const
{
	if (index.isValid())
	{
		ReflexModelItem* item = itemForIndex(index);
		if ((role == Qt::DisplayRole) || (role == Qt::EditRole))
		{
			switch (index.column())
			{
			case NameColumn:
				return item->name();
			case TypenameColumn:
				return item->value("type-name");
			case T1Column:
				return item->value("T1");
			case T2Column:
				return item->value("T2");
			case T3Column:
				return item->value("T3");
			case DestinationColumn:
				if (!item->outgoing().isNull())
				{
					return item->outgoing().destination()->name();
				}
			}
		}
		else if ((role == Qt::DecorationRole) && (index.column() == NameColumn))
		{
			switch(item->type())
			{
			case ComponentItem:
				return QIcon(":/icons/component16.png");
			case InputItem:
				return QIcon(":/icons/input16.png");
			case OutputItem:
				return QIcon(":/icons/output16.png");
			default:
				break;
			}
		}
	}

	return QVariant();
}

Qt::ItemFlags ReadOnlyReflexModel::flags(const QModelIndex& index) const
{
	ReflexModelItem* node = itemForIndex(index);
	Qt::ItemFlags flags = Qt::ItemIsEnabled;

	if (index.isValid())
	{
		flags |= Qt::ItemIsSelectable;
	}
	return flags;
}

QVariant ReadOnlyReflexModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if ((orientation == Qt::Horizontal) && (role == Qt::DisplayRole))
	{
		switch (section)
		{
		case NameColumn:
			return tr("Name");
		case TypenameColumn:
			return tr("Typename");
		case T1Column:
			return tr("T1");
		case T2Column:
			return tr("T2");
		case T3Column:
			return tr("T3");
		case DestinationColumn:
			return tr("Destination");
		}
	}

	return QAbstractItemModel::headerData(section, orientation, role);

}

QModelIndex ReadOnlyReflexModel::parent(const QModelIndex& index) const
{
	if (!index.isValid())
	{
		return QModelIndex();
	}

	ReflexModelItem* item = itemForIndex(index);
	ReflexModelItem* parentItem = item->parent();
	if (parentItem == root())
	{
		return QModelIndex();
	}

	int row = rowForItem(parentItem);
	return createIndex(row, NameColumn, parentItem);
}

QModelIndex ReadOnlyReflexModel::index(int row, int column, const QModelIndex& parent) const
{
	if (hasIndex(row, column, parent))
	{
		ReflexModelItem* parentItem = itemForIndex(parent);
		QList<ReflexModelItem*> items = parentItem->children().byType(AllItems);
		ReflexModelItem* childItem = items.at(row);
		return createIndex(row, column, childItem);
	}
	return QModelIndex();
}

int ReadOnlyReflexModel::columnForKey(const QString& key)
{
	if (key == "name")
	{
		return NameColumn;
	}
	else if (key == "type-name")
	{
		return TypenameColumn;
	}
	else if (key == "T1")
	{
		return T1Column;
	}
	else if (key == "T2")
	{
		return T2Column;
	}
	else if (key == "T3")
	{
		return T3Column;
	}
	else
	{
		Q_ASSERT(false);
	}
}

QString ReadOnlyReflexModel::keyForColumn(int column)
{
	switch (column)
	{
	case NameColumn:
		return "name";
	case TypenameColumn:
		return "type-name";
	case T1Column:
		return "T1";
	case T2Column:
		return "T2";
	case T3Column:
		return "T3";
	default:
		return "";
	}
}

QModelIndex ReadOnlyReflexModel::indexForItem(ReflexModelItem* item, int column) const
{
	QModelIndex index;
	if ((item != 0) && (item != root()))
	{
		int row = rowForItem(item);
		index = createIndex(row, column, item);
	}
	return index;
}

int ReadOnlyReflexModel::rowCount(const QModelIndex& parent) const
{
	ReflexModelItem* parentItem = itemForIndex(parent);
	return parentItem->children().size();
}

int ReadOnlyReflexModel::rowForItem(ReflexModelItem* item) const
{
	ReflexModelQuery query(*this);
	QList<ReflexModelItem*> items = item->parent()->children().byType(AllItems);
	return items.indexOf(item);
}

ReflexModelItem* ReadOnlyReflexModel::itemForIndex(const QModelIndex& index) const
{
	if (index.isValid())
	{
		return static_cast<ReflexModelItem*> (index.internalPointer());
	}
	else
	{
		return root();
	}
}
