/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ConnectCommand.h"
#include "DisconnectCommand.h"
#include "EditableReflexModel.h"
#include "MainWindow.h"
#include "InsertCommand.h"
#include "MoveCommand.h"
#include "RemoveCommand.h"
#include "SetCommand.h"
#include "TypeLibrary.h"

#include <QtCore/QDebug>

using namespace editor;

EditableReflexModel::EditableReflexModel()
{
	MainWindow::undoGroup.addStack(&_undoStack);
	_undoStack.setActive(true);

	QObject::connect(&_undoStack, SIGNAL(cleanChanged(bool)), this, SLOT(changeCleanState(bool)));
}

void EditableReflexModel::setLibrary(TypeLibrary* library)
{
	_library = library;
}

void EditableReflexModel::setModel(const ReflexModelFragment& model)
{
	ReadOnlyReflexModel::setModel(model);
	_undoStack.clear();
}

Qt::ItemFlags EditableReflexModel::flags(const QModelIndex &index) const
{
	ReflexModelItem* node = itemForIndex(index);
	Qt::ItemFlags flags = ReadOnlyReflexModel::flags(index);

	if (!index.isValid())
	{
		return flags;
	}

	// Name and typeName are normal editable if user elements
	if ((index.column() == NameColumn)
			|| (index.column() == TypenameColumn)
			|| (index.column() == T1Column)
			|| (index.column() == T2Column)
			|| (index.column() == T3Column))
	{
		if (!node->inherits("built-in", true))
		{
			flags |= Qt::ItemIsEditable;
		}
	}

	return flags;
}

ReflexModelConnection EditableReflexModel::connect(ReflexModelItem* sourceItem, ReflexModelItem* destinationItem)
{
	ConnectCommand* command = new ConnectCommand(this, sourceItem, destinationItem);
	_undoStack.push(command);
	return command->connection();
}

void EditableReflexModel::disconnect(const ReflexModelConnection& connection)
{
	DisconnectCommand* command = new DisconnectCommand(this, connection);
	_undoStack.push(command);
}

void EditableReflexModel::insert(const QSharedPointer<ReflexModelItem>& item, ReflexModelItem* parent,
		int row)
{
	_undoStack.push(new InsertCommand(this, item, parent, row));
}

void EditableReflexModel::move(ReflexModelItem* item, ReflexModelItem* parentItem, int row)
{
	Q_ASSERT(item != 0);
	Q_ASSERT(parentItem != 0);
	_undoStack.beginMacro(QObject::trUtf8("Move %1").arg(item->name()));
	_undoStack.push(new MoveCommand(this, item, parentItem, row));
	_undoStack.endMacro();
}

void EditableReflexModel::remove(ReflexModelItem* item)
{
	Q_ASSERT(item != 0);
	_undoStack.beginMacro(trUtf8("Remove %1").arg(item->name()));
	_undoStack.push(new RemoveCommand(this, item));
	_undoStack.endMacro();
}

bool EditableReflexModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
	Q_UNUSED(role);
	setValue(itemForIndex(index), keyForColumn(index.column()), value);
	return true;
}

void EditableReflexModel::setValue(ReflexModelItem* item, const QString& key, const QVariant& value)
{
	Q_ASSERT(item != 0);
	_undoStack.push(new SetCommand(this, item, key, value));
}

/* Helper function for SetCommand */
void EditableReflexModel::emitDataChanged(ReflexModelItem* item, const QString& key)
{
	int column = columnForKey(key);
	QModelIndex i = indexForItem(item, column);
	emit dataChanged(i, i);
}

QUndoStack* EditableReflexModel::undoStack()
{
	return &_undoStack;
}

void EditableReflexModel::changeCleanState(bool isClean)
{
	emit documentModified(!isClean);
}

