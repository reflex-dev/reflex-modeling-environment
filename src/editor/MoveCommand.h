/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef MOVECOMMAND_H_
#define MOVECOMMAND_H_

#include "ReflexModelCommand.h"

class EditableReflexModel;
class ReflexModelItem;

/*!
 Moves a single item inside its parent component.
 */
class MoveCommand : public ReflexModelCommand
{
public:
	MoveCommand(EditableReflexModel* model, ReflexModelItem* item, ReflexModelItem* parentItem, int row);
	void redo();
	void undo();

private:
	ReflexModelItem* item;
	ReflexModelItem* dstParentItem;
	ReflexModelItem* srcParentItem;
	int dstRow;
	int srcRow;
};

#endif /* MOVECOMMAND_H_ */
