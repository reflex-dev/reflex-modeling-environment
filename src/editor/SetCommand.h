/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _SETCOMMAND_H
#define _SETCOMMAND_H

#include "ReflexModelCommand.h"
#include <QtCore/QString>
#include <QtCore/QVariant>

class ReflexModelItem;

class SetCommand: public ReflexModelCommand
{
public:
	SetCommand(EditableReflexModel* model, ReflexModelItem* item, const QString& key, const QVariant& value);
	virtual void redo();
	virtual void undo();

private:
	ReflexModelItem * item;
	QString key;
	QVariant value;
};
#endif
