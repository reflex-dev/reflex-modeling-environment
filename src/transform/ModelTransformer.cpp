/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/

#include "ModelTransformer.h"
#include <ReflexModelQuery.h>
#include <ReflexModelScriptReader.h>

#include <QtCore/QCommandLineOption>
#include <QtCore/QCommandLineParser>
#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QtGlobal>
#include <QtCore/QProcessEnvironment>
#include <QtCore/QStringList>


ModelTransformer::ModelTransformer()
{
}

void ModelTransformer::parseArguments()
{
    QCommandLineParser parser;
    parser.addHelpOption();

    parser.addPositionalArgument("command",
                                 tr("xcore"));

    parser.addPositionalArgument("file",
                                 tr("Reflex model file"));

    QCommandLineOption outputDirOption(
                QStringList() << "output-directory" << "o",
                "Directory where the transformed file will be stored",
                "dir",
                ".");
    parser.addOption(outputDirOption);

    QString searchPath =
              QDir::cleanPath(
                QCoreApplication::applicationDirPath()
                + "/"
                + QLatin1String(RME_RELATIVE_SEARCH_PATH)
                + "share/rme/"
              );

    QCommandLineOption libraryPathOption(
                QStringList() << "library-path" << "L",
                "Add directory to library search path",
                "dir",
                QDir::cleanPath(searchPath + "imports")
    );
    parser.addOption(libraryPathOption);

    parser.process(*QCoreApplication::instance());
    QStringList args = parser.positionalArguments();
    args.removeFirst(); // workaround to remove "transform" command

    if (args.size() < 2)
    {
        parser.showHelp(1);
    }

    _command = args.at(0);
    _filepath = args.at(1);
    _types = args.mid(2, -1);

    _outputDir.setPath(QDir::cleanPath(parser.value(outputDirOption)));
    VERIFY_X(_outputDir.exists(),
             tr("Could not find output directory '%1'").arg(_outputDir.path()));

    /* Setup library search path */
    QProcessEnvironment environment = QProcessEnvironment::systemEnvironment();
    QStringList librarySearchPaths;
    librarySearchPaths << ".";
    librarySearchPaths << environment.value("RME_LIB_PATH").split(":");
    librarySearchPaths << parser.values(libraryPathOption);
    librarySearchPaths.removeDuplicates();
    foreach(const QString& path, librarySearchPaths)
    {
        QDir dir(path);
        VERIFY_X(dir.exists(),
                 tr("library-path '%1' does not exist.").arg(path));
    }
    ReflexModelScriptReader::importPaths = librarySearchPaths;
}


void ModelTransformer::start()
{

}
