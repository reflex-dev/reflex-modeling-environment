import qbs 1.0

StaticLibrary {
    name: "transform"

    Depends { name : "Qt.core" }
    Depends { name : "cpp" }

    Depends { name : "common" }

    cpp.defines : [
        "RME_VERSION=" + '"' + project.version + '"',
        "RME_RELATIVE_SEARCH_PATH=" + '"' + project.relativeSearchPath + '"'
    ]

    files: [
        "ModelTransformer.h",
        "ModelTransformer.cpp"
    ]

    Export {
        Depends { name : "cpp" }
        cpp.includePaths : "."
    }
}
