/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "CodeGenerator.h"
#include "DefaultComponentRenderer.h"
#include "XcoreComponentRenderer.h"
#include "RenderingEnvironment.h"

#include <ReflexModelItem.h>
#include <ReflexModelQuery.h>
#include <TypeLibrary.h>
#include <ReflexModelScriptReader.h>

#include <QtCore/QCommandLineOption>
#include <QtCore/QCommandLineParser>
#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QtGlobal>
#include <QtCore/QProcessEnvironment>
#include <QtCore/QStringList>

#define CODEGEN_APPLICATIONDESCRIPTION "\n\
Codegen translates reflex model files into code. Implemented commands are:\n\
- model-header   : Structural header file containing nested components,\n\
                   inputs, activities, outputs, get-/set-methods\n\
- header-stub    : An empty component header file deriving from a model-\n\
                   header. It contains empty methods for activities and can.\n\
                   be used as a starting-point for implementation.\n\
- source-stub    : An empty component source file related to a header-stub.\n\
- main-source    : A main.cc file setting up a reflex::mcu::CoreApplication\n\
                   object and creating an application object on the stack.\n\
                   The scheduler is also started.\n\
- startup-source : Creates a startup.cc file which initializes all used\n\
                   core threads and connects cross-core event-channels. This\n\
                   can not be done in the main.xc file. This command is\n\
                   available for the xcore architecture only.\
"
CodeGenerator::CodeGenerator()
{
}

void CodeGenerator::parseArguments()
{
    QString rootPath =
              QDir::cleanPath(
                QCoreApplication::applicationDirPath()
                + "/"
                + QLatin1String(RME_RELATIVE_SEARCH_PATH)
                + "share/rme/"
              );

    QCommandLineParser parser;
    parser.addHelpOption();

    parser.setApplicationDescription(CODEGEN_APPLICATIONDESCRIPTION);

    parser.addPositionalArgument("command",tr(
            "Tells the code generator what to do. Allowed values are: "
            "model-header, header-stub, source-stub, main-source, "
            "startup-source"));

    parser.addPositionalArgument("file",
                                 tr("Reflex model file"));

    QCommandLineOption outputFilenameOption(
                QStringList() << "output" << "o",
                "Place the output into <file>",
                "file");
    parser.addOption(outputFilenameOption);

    QCommandLineOption templateDirOption(
                QStringList() << "template-directory" << "t",
                "Directory from which code templates are being read",
                "dir",
                QDir::cleanPath(rootPath + "/tpl/codegen/")
    );
    parser.addOption(templateDirOption);

    QCommandLineOption importPathOption(
                QStringList() << "import-path" << "I",
                "Add the directory <dir> to the list of directories to be "
                "searched for RML files",
                "dir"
    );
    parser.addOption(importPathOption);

    parser.process(*QCoreApplication::instance());
    QStringList args = parser.positionalArguments();
    args.removeFirst(); // workaround to remove "codegen" command

    if (args.size() < 2)
    {
        parser.showHelp(1);
    }

    _command = args.at(0);
    _filepath = args.at(1);
    _outputFilename = parser.value(outputFilenameOption);

    if (_outputFilename.isEmpty())
    {
        if (_command == "model-header")
            _outputFilename = QFileInfo(_filepath).baseName() + ".h";
        else if (_command == "main-source")
            _outputFilename = "main.cc";
        else if (_command == "startup-source")
            _outputFilename = "startup.cc";
        else if (_command == "header-stub")
        {
            QFileInfo info = QFileInfo(_filepath);
            QString dirname = info.path();
            QString filename = info.baseName() + ".h";
            _outputFilename = QDir::cleanPath(dirname + "/" + filename);
        }
        else if (_command == "source-stub")
        {
            QFileInfo info = QFileInfo(_filepath);
            QString dirname = info.path();
            QString filename = info.baseName() + ".cc";
            _outputFilename = QDir::cleanPath(dirname + "/" + filename);
        }
    }

    _templateDir.setPath(QDir::cleanPath(parser.value(templateDirOption)));
    VERIFY_X(_templateDir.exists(),
             tr("Could not find template directory '%1'").arg(_templateDir.path()));

    /* Setup import paths */
    QProcessEnvironment environment = QProcessEnvironment::systemEnvironment();
    QStringList importPaths;
    foreach(const QString& path, parser.values(importPathOption))
    {
        QDir dir(path);
        VERIFY_X(dir.exists(),
                 tr("import-path '%1' does not exist.").arg(path));
        importPaths << dir.absolutePath();
    }
    importPaths << QDir::cleanPath(rootPath + "/imports/");
    importPaths.removeDuplicates();

    ReflexModelScriptReader::importPaths = importPaths;
    RenderingEnvironment::instance()->_command = _command;
    RenderingEnvironment::instance()->_outputFilename = _outputFilename;
    RenderingEnvironment::instance()->_templateDir = _templateDir;
}

void CodeGenerator::start()
{
    if (hasErrors())
    {
        fprintf(stderr, "%s\n", qPrintable(QString("Error: ") + this->errorString()));
        QCoreApplication::exit(1);
        return;
    }

    QString command = RenderingEnvironment::instance()->command();
    if (command == "model-header")
    {
        createModelHeaders();
    }
    else if (command == "main-source")
    {
        createModelHeaders();
    }
    else if (command == "startup-source")
    {
        createModelHeaders();
    }
    else if (command == "header-stub")
    {
        createHeaders();
    }
    else if (command == "source-stub")
    {
        createSources();
    }
    else
    {
        raiseError(tr("Command '%1' is not known.").arg(command));
    }

    if (hasErrors())
    {
        fprintf(stderr, "%s\n", qPrintable(this->errorString()));
        QCoreApplication::exit(1);
        return;
    }

    QCoreApplication::exit(0);
}

void CodeGenerator::createModelHeaders()
{
    readModelFile(_filepath);
    if (hasErrors())
    {
        return;
    }

    RenderingEnvironment::instance()->render(&_model);
}

void CodeGenerator::createHeaders()
{
    readModelFile(_filepath);
    if (hasErrors())
    {
        return;
    }

    QList<ReflexModelItem*> components = ReflexModelQuery(_model)
            .subtree(ComponentItem, ReflexModelQuery::Recursive);
    foreach (ReflexModelItem* c, components)
    {
        // Ignore built-in types
        if (c->value("built-in").toBool() == true)
        {
            continue;
        }

        // Generate only types defined in the model file
        if (c->value("model-file") != QFileInfo(_filepath).fileName())
        {
            continue;
        }

        Template tpl;
        tpl.setTemplateFileName(_templateDir.filePath("DefaultComponent.tpl.h"));
        tpl.setOutputFileName(_outputFilename);

        tpl.insert("type", c->value("type-name").toString());

        QList<ReflexModelItem*> inputs = ReflexModelQuery(c)
                .childrenByType(InputItem);
        foreach(ReflexModelItem* i, inputs)
        {
            QString stub = QString("void run_%1();").arg(i->name());
            tpl.insert("activityStubs", stub);
        }

        QString fileName = QFileInfo(c->value("model-file").toString()).baseName();
        QString include = QString("#include \"model_%1.h\"")
                .arg(fileName);

        tpl.insert("includes", include);
        tpl.save();
    }

}

void CodeGenerator::createSources()
{
    readModelFile(_filepath);
    if (hasErrors())
    {
        return;
    }

    QList<ReflexModelItem*> components = ReflexModelQuery(_model)
            .subtree(ComponentItem, ReflexModelQuery::Recursive);
    foreach (ReflexModelItem* c, components)
    {
        // Ignore built-in types
        if (c->value("built-in").toBool() == true)
        {
            continue;
        }

        // Generate only types defined in the model file
        if (c->value("model-file") != QFileInfo(_filepath).fileName())
        {
            continue;
        }

        Template tpl;
        tpl.setTemplateFileName(_templateDir.filePath("DefaultComponent.tpl.cc"));
        tpl.setOutputFileName(_outputFilename);

        tpl.insert("type", c->value("type-name").toString());

        QList<ReflexModelItem*> inputs = ReflexModelQuery(c)
                .childrenByType(InputItem);
        foreach(ReflexModelItem* i, inputs)
        {
            QString stub = QString("void %1::run_%2()\n{\n}")
                    .arg(i->parent()->value("type-name").toString())
                    .arg(i->name());
            tpl.insert("activityStubs", stub);
        }

        QString fileName = QFileInfo(c->value("model-file").toString()).baseName();
        QString include = QString("#include \"%1.h\"")
                .arg(fileName);

        tpl.insert("includes", include);
        tpl.save();
    }
}

void CodeGenerator::readModelFile(const QString& fileName)
{
    QFile file(fileName);
    VERIFY_X(file.exists(),
             trUtf8("File '%1' does not exist").arg(fileName));

    VERIFY_X(file.open(QIODevice::ReadOnly),
             trUtf8("Could not open model file '%1'").arg(fileName));

    TypeLibrary library;
    ReflexModelScriptReader reader;
    reader.setLibrary(&library);
    reader.setModelFile(fileName);

    VERIFY_X(!reader.hasErrors(),
             trUtf8("Error reading model file '%1'").arg(reader.errorString()));

    _model = reader.model();
}
