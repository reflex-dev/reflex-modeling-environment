/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef COMPONENTCODEGENERATOR_H_
#define COMPONENTCODEGENERATOR_H_

#include <ErrorHandler.h>

#include <QtCore/QDir>
#include <QtCore/QString>
#include "AbstractItemRenderer.h"

class ReflexModelItem;
class ReflexModelFragment;
class QVariant;

class DefaultComponentRenderer : public AbstractItemRenderer
{
public:
    DefaultComponentRenderer();

protected:
    virtual void enterItem(ReflexModelItem* item);
    virtual void leaveItem(ReflexModelItem* item);

    virtual void renderItem(ReflexModelItem* item);
    virtual void renderNestedComponents();
    virtual void renderActivities();
    virtual void renderInputs();

    virtual void renderOutputs();
    virtual void renderOutput(ReflexModelItem* item);
    virtual void renderConnections();

    virtual QString outputFile();
    virtual QString templateFile();

    void renderPriorityMethods();
    void setPriorities();
    void setTimeslots();

    static QString typeNameForItem(ReflexModelItem* item);
    static QString cleanTypename(const QString& typeName);
    static QString cleanTypename(const QVariant& typeName);

protected:
    static QStringList renderedTypes;
};

#endif /* COMPONENTCODEGENERATOR_H_ */
