/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "RenderingEnvironment.h"
#include "DefaultComponentRenderer.h"
#include "XcoreComponentRenderer.h"

#include <ReflexModelFragment.h>
#include <ReflexModelQuery.h>

#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QWeakPointer>

RenderingEnvironment* RenderingEnvironment::_instance = 0;

QSharedPointer<AbstractItemRenderer> RenderingEnvironment::createRenderer(ReflexModelItem* item)
{
    QString architecture = item->value("architecture").toString();
    if (architecture == "xcore")
    {
        return QSharedPointer<AbstractItemRenderer>(new XcoreComponentRenderer());
    }
    else
    {
        return QSharedPointer<AbstractItemRenderer>(new DefaultComponentRenderer());
    }
}

/**
\brief Propagate all information top -> down

Only special case 'core' is implemented. This must be generalized.
 */
void RenderingEnvironment::flattenModel(ReflexModelFragment* model)
{

    // Flatten model information
    QList<ReflexModelItem*> items = ReflexModelQuery(*model)
                .childrenByType(AllItems, ReflexModelQuery::Recursive);
    foreach (ReflexModelItem* i, items)
    {
        const QHash<QString, QVariant> values = i->values();
        const QHash<QString, QVariant> parentValues = i->parent()->values();
        QSet<QString> keys = i->values().keys().toSet();
        QSet<QString> parentKeys = parentValues.keys().toSet();

        QSet<QString> missingParentKeys = parentKeys.subtract(keys);

        // do not propagate some values down:
        missingParentKeys.remove("header-file");

        foreach(const QString& missingKey, missingParentKeys)
        {
            i->setValue(missingKey, parentValues[missingKey]);
        }
    }
}

void RenderingEnvironment::render(ReflexModelFragment* model)
{
    Q_ASSERT(model != 0);
    _model = model;

    RenderingEnvironment::flattenModel(model);

    // First round: attach a renderer to each item
    QList<ReflexModelItem*> components = ReflexModelQuery(*model)
            .subtree(ComponentItem, ReflexModelQuery::Recursive);
    foreach (ReflexModelItem* c, components)
    {
        _item = c;
        // Ignore built-in types
        if (c->value("built-in").toBool() == true)
        {
            continue;
        }

        // Generate only types defined in the model file
        if (c->value("model-file") != model->root()->value("model-file"))
        {
            continue;
        }

        // If type filter is enabled, produce only wanted types
        //    if ((!_types.isEmpty()) && (!_types.contains(c->value("type-name").toString())))
        //    {
        //     continue;
        //    }

        QSharedPointer<AbstractItemRenderer> renderer = createRenderer(c);
        _renderers.insert(c, renderer);
    }

    // Leave each renderer the chance, to do model transformations
    foreach (ReflexModelItem* c, components)
    {
        if (!_renderers.contains(c))
            continue;

        QSharedPointer<AbstractItemRenderer> renderer = _renderers[c];
        _item = c;
        renderer->enterItem(c);

        if (renderer->hasErrors())
        {
            raiseError(QObject::tr("Error: %1").arg(renderer->errorString()));
            return;
        }
    }

    // Traverse again, but generate code
    foreach (ReflexModelItem* c, components)
    {
        if (!_renderers.contains(c))
            continue;

        QSharedPointer<AbstractItemRenderer> renderer = _renderers[c];
        _item = c;
        renderer->renderItem(c);

        if (renderer->hasErrors())
        {
            raiseError(QObject::tr("Error: %1").arg(renderer->errorString()));
            return;
        }
    }

    // Post-order traversal, leaving each renderer the chance to
    // clean up its work and export data to parent renderers
    for (int i = components.size() - 1; i >= 0; i-- )
    {
        ReflexModelItem* c = components.at(i);
        if (!_renderers.contains(c))
            continue;

        QSharedPointer<AbstractItemRenderer> renderer = _renderers[c];
        _item = c;
        renderer->leaveItem(c);

        if (renderer->hasErrors())
        {
            raiseError(QObject::tr("Error: %1").arg(renderer->errorString()));
            return;
        }
    }
}

RenderingEnvironment* RenderingEnvironment::instance()
{
    if (!_instance)
    {
        _instance = new RenderingEnvironment();
    }
    return _instance;
}
