/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef REFLEXCODEGENERATOR_H_
#define REFLEXCODEGENERATOR_H_

#include <ErrorHandler.h>
#include <ReflexModelFragment.h>
#include <TypeLibrary.h>

#include <QtCore/QHash>
#include <QtCore/QDir>

class CodeGenerator: public QObject, public ErrorHandler
{
    Q_OBJECT
public:
    CodeGenerator();
    void parseArguments();

public slots:
    void start();

private:
    void createModelHeaders();
    void createHeaders();
    void createSources();

    void readModelFile(const QString& fileName);

    ReflexModelFragment _model;
    TypeLibrary _library;

    // Options
    QString _command;
    QString _filepath;
    QString _outputFilename;
    QDir _templateDir;
};

#endif /* REFLEXCODEGENERATOR_H_ */
