/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef RENDERENVIRONMENT_H
#define RENDERENVIRONMENT_H

#include <ErrorHandler.h>

#include <QtCore/QDir>
#include <QtCore/QGlobalStatic>
#include <QtCore/QScopedPointer>
#include <QtCore/QSharedPointer>
#include <QtCore/QStack>

#include "Template.h"

class AbstractItemRenderer;
class ReflexModelFragment;
class ReflexModelItem;
class Template;

/*!
\brief Global code generation object.

The rendering environment controls the the code generation process and
provides central functionality for all rendering objects.

 */
class RenderingEnvironment : public ErrorHandler
{
    friend class CodeGenerator;
public:

    enum RenderCommand
    {
        ModelCommand
    };

    inline const QString command() const { return _command; }
    inline ReflexModelItem* item() const { return _item; }
    inline ReflexModelItem* item() { return _item; }
    inline const ReflexModelFragment* model() const { return _model; }
    inline ReflexModelFragment* model() { return _model; }
    QSharedPointer<AbstractItemRenderer> rendererByItem(ReflexModelItem* item);

    inline const QString& outputFilename() const { return _outputFilename; }
    inline const QDir& templateDir() const { return _templateDir; }
    void setTemplate(const QSharedPointer<Template>& tpl);
    inline QSharedPointer<Template> tpl() { return _template; }

    static RenderingEnvironment* instance();
    static QSharedPointer<AbstractItemRenderer> createRenderer(ReflexModelItem* item);


protected:
    void render(ReflexModelFragment* model);

    QString _command;
    QString _outputFilename;
    QDir _templateDir;

private:
    static void flattenModel(ReflexModelFragment* model);

    static RenderingEnvironment* _instance;

    ReflexModelFragment* _model;
    ReflexModelItem* _item;
    QSharedPointer<Template> _template;
    QMap<ReflexModelItem*, QSharedPointer<AbstractItemRenderer> > _renderers;

};

inline void RenderingEnvironment::setTemplate(const QSharedPointer<Template> &tpl)
{
    this->_template = tpl;
}

inline QSharedPointer<AbstractItemRenderer>
    RenderingEnvironment::rendererByItem(ReflexModelItem* item)
{
    Q_ASSERT(_renderers.contains(item));
    return _renderers.value(item);
}


//Q_GLOBAL_STATIC(RenderingEnvironment, renv);


#endif // RENDERENVIRONMENT_H
