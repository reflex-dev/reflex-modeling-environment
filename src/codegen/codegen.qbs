import qbs 1.0

StaticLibrary {
    name: "codegen"

    Depends { name : "Qt.core" }
    Depends { name : "cpp" }

    Depends { name : "common" }

    cpp.defines : [
        "RME_VERSION=" + '"' + project.version + '"',
        "RME_RELATIVE_SEARCH_PATH=" + '"' + project.relativeSearchPath + '"'
    ]

    files: [
        "CodeGenerator.*",
        "DefaultComponentRenderer.*",
        "RenderingEnvironment.cpp",
        "RenderingEnvironment.h",
        "AbstractItemRenderer.cpp",
        "XcoreComponentRenderer.cpp"
    ]

    Export {
        Depends { name : "cpp" }
        cpp.includePaths : "."
    }
}
