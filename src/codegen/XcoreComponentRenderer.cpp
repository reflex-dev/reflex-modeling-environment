/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "XcoreComponentRenderer.h"
#include "RenderingEnvironment.h"

#include <ReflexModelConnection.h>
#include <ReflexModelItem.h>
#include <ReflexModelQuery.h>
#include <ReflexModelFragment.h>

#include <QtCore/QDebug>
#include <QtCore/QDateTime>

QSet<QString> XcoreComponentRenderer::_cores;
QSet<XcoreComponentRenderer::CorePair>
    XcoreComponentRenderer::_crossCoreConnections;
QHash<XcoreComponentRenderer::CorePair, QString>
    XcoreComponentRenderer::_channelEnums;
QMultiMap<QString, const ReflexModelConnection*>
    XcoreComponentRenderer::_inConnPerCore;
QMultiMap<QString, const ReflexModelConnection*>
    XcoreComponentRenderer::_outConnPerCore;

QString XcoreComponentRenderer::templateFile()
{
    QString command = env()->command();
    bool isRoot = env()->item()->isRoot();

    if ((command == "model-header") && isRoot)
        return env()->templateDir().filePath("XcoreRootModelHeader.tpl.h");
    else if ((command == "model-header"))
        return env()->templateDir().filePath("DefaultComponentModel.tpl.h");
    else if ((command == "main-source") && isRoot)
        return env()->templateDir().filePath("main.tpl.xc");
    else if (command == "main-source")
        return "";
    else if ((command == "startup-source") && isRoot)
        return env()->templateDir().filePath("xcore_StartupSource_file.tpl.cc");
    else if ((command == "startup-source") && env()->item()->parent()->isRoot())
        return env()->templateDir().filePath("xcore_StartupSource_functions.tpl.cc");
    else if (command == "startup-source")
        return "";
    else
        Q_ASSERT(false);
}

void XcoreComponentRenderer::enterItem(ReflexModelItem* item)
{
    typedef QPair<QString, QString> ReplacementPair;
    QMap<QString, ReplacementPair> xcoreMap;
    xcoreMap["reflex::Event"] =
            ReplacementPair("reflex::xcore::XcoreEvent", "reflex/sinks/XcoreEvent.h");
    xcoreMap["reflex::SingleValue1"] =
            ReplacementPair("reflex::xcore::XcoreSingleValue1", "reflex/sinks/XcoreSingleValue.h");
    xcoreMap["reflex::Sink0"] =
            ReplacementPair("reflex::xcore::ChannelOutput0", "reflex/io/ChannelOutput.h");
    xcoreMap["reflex::Sink1"] =
            ReplacementPair("reflex::xcore::ChannelOutput1", "reflex/io/ChannelOutput.h");

    // Model transformation, that replaces core-crossing event-channels with
    // xlink event channels
    foreach(const ReflexModelConnection& connection, env()->item()->connections())
    {
        ReflexModelItem* src = connection.source();
        ReflexModelItem* dst = connection.destination();
        QString srcCore = src->value("core").toString();
        QString dstCore = dst->value("core").toString();

        // Identify core-crossing event channels
        if (srcCore != dstCore)
        {
            _crossCoreConnections.insert(
                qMakePair(srcCore, dstCore)
                );
            _outConnPerCore.insert(srcCore, &connection);
            _inConnPerCore.insert(dstCore, &connection);

            // Replace output type by xcore equivalent
            if (xcoreMap.contains(src->value("type-name").toString()))
            {
                ReplacementPair tuple = xcoreMap.value(src->value("type-name").toString());
                QString typeName = tuple.first;
                QString headerFile = tuple.second;
                src->setValue("original-type-name", src->value("type-name"));
                src->setValue("type-name", typeName);
                src->setValue("header-file", headerFile);
            }

            // Replace input type by xcore equivalent
            if (xcoreMap.contains(dst->value("type-name").toString()))
            {
                ReplacementPair tuple = xcoreMap.value(dst->value("type-name").toString());
                QString typeName = tuple.first;
                QString headerFile = tuple.second;
                dst->setValue("original-type-name", dst->value("type-name"));
                dst->setValue("type-name", typeName);
                dst->setValue("header-file", headerFile);
            }
        }
    }

    const QString core = item->value("core").toString();

    // assume, that 2nd level components run each on its own core
    if (item->parent() && item->parent()->isRoot())
    {
        _cores.insert(core);
    }

    this->test = item->name();

    _tpl = QSharedPointer<Template>(new Template());
    _tpl->setTemplateFileName(templateFile());

    if (item->isRoot())
    {
        _tpl->setOutputFileName(env()->outputFilename());
        _tpl->insert("time", QDateTime::currentDateTime().toLocalTime().toString());
        _tpl->insert("generator", QString("rme codegen version %1").arg(RME_VERSION));
        _tpl->insert("model-file", item->value("model-file").toString());
        env()->setTemplate(_tpl);
    }
}


void XcoreComponentRenderer::leaveItem(ReflexModelItem* item)
{
    const QString command = env()->command();

    if ((command == "startup-source") && (item->isRoot()))
        env()->tpl()->save();
    else if ((command == "startup-source") && (item->parent()->isRoot()))
        env()->tpl()->insert("functions", _tpl);
    else if ((command == "main-source") && (item->isRoot()))
        env()->tpl()->save();
    else if (command == "model-header")
    {
        QString typeName = item->value("type-name").toString();
        if (renderedTypes.contains(typeName))
            return;
        else
            renderedTypes << typeName;

        if (item->isRoot())
            env()->tpl()->save();
        else
            env()->tpl()->insert("classes", _tpl);
    }
}

void XcoreComponentRenderer::renderItem(ReflexModelItem* item)
{
    if (item->isRoot())
    {
        extractMetadata(*item->owner());
    }

    const QString command = env()->command();
    if ((command == "model-header") && item->isRoot())
        renderForModelHeader(item);
    else if ((command == "main-source") && (item->isRoot()))
        renderForMainSource(item);
    else if ((command == "startup-source") && (item->isRoot()))
        renderStartupSource(item);
    else if (command == "startup-source")
        ;
    else if (item->isRoot())
        ;
    else
        DefaultComponentRenderer::renderItem(item);
}

void XcoreComponentRenderer::renderOutput(ReflexModelItem* item)
{
    // Use default method for unmodified outputs
    if (item->value("original-type-name").isNull())
    {
        DefaultComponentRenderer::renderOutput(item);
        return;
    }

    QString includes;
    QString declarations;
    QString getter;
    QString setter;

    includes = QString("#include \"%1\"").arg(
                item->value("header-file").toString());
    declarations = QString("%1 _%2;").arg(
                    typeNameForItem(item),
                    item->name());
    getter = QString("%1* %2() { return &_%2; }").arg(
                typeNameForItem(item),
                item->name());
    setter = QString("void set_%1(%2* input) { this->_%1 = input; }").arg(
                item->name(),
                typeNameForItem(item));

    _tpl->insert("includes", includes);
    _tpl->insert("outputs", declarations);
    _tpl->insert("outputGets", getter);
    _tpl->insert("outputSets", setter);
}

void XcoreComponentRenderer::renderForModelHeader(ReflexModelItem *item)
{
    _tpl->insert("nrOfInstances", QString::number(_cores.count()));

    // Forward/backward connections count as one channel
    quint32 nrOfChannels =
            (_inConnPerCore.uniqueKeys() + _outConnPerCore.uniqueKeys())
            .toSet().size() - 1;

    foreach(const QString& channelEnum, _channelEnums)
    {
        _tpl->insert("channels", QString("%1,").arg(channelEnum));
    }

    foreach (const QString& core, _cores)
    {
        quint32 nrOfInChannels = _inConnPerCore.values(core).count();
        quint32 nrOfOutChannels = _outConnPerCore.values(core).count();

        QString startupDeclaration =
                QString("void start_%1(%2);")
                .arg(core)
                .arg(
                    static_cast<QStringList>(
                        QStringList::fromVector(
                             QVector<QString>(
                                 nrOfInChannels + nrOfOutChannels,
                                 "chanend"
                             )
                        )
                    ).join(", ")
                );
        _tpl->insert("startupFunctions", startupDeclaration);
    }
}

void XcoreComponentRenderer::renderForMainSource(ReflexModelItem *item)
{
    // Forward/backward connections count as one channel
    quint32 nrOfChannels =
            (_inConnPerCore.uniqueKeys() + _outConnPerCore.uniqueKeys())
            .toSet().size() - 1;
    _tpl->insert("nrOfChannels", QString::number(nrOfChannels));

    foreach (const QString& core, _cores)
    {
        QList<const ReflexModelConnection*> outConnections =
                _outConnPerCore.values(core);
        QList<const ReflexModelConnection*> inConnections =
                _inConnPerCore.values(core);

        QStringList enums;
        foreach (const ReflexModelConnection* connection, inConnections)
        {
            CorePair pair;
            pair.first = connection->source()->value("core").toString();
            pair.second = core;
            enums << QString("channels[%1]").arg(_channelEnums.value(pair));
        }
        foreach (const ReflexModelConnection* connection, outConnections)
        {
            CorePair pair;
            pair.first = core;
            pair.second = connection->destination()->value("core").toString();
            enums << QString("channels[%1]").arg(_channelEnums.value(pair));
        }

        QString startupDeclaration =
                QString("start_%1(%2);")
                .arg(core)
                .arg(enums.join(", "));
        _tpl->insert("startupFunctions", startupDeclaration);
    }

    QString include = QString("#include \"model_%1\"")
            .arg(item->value("model-file")
            .toString()
            .replace(".rml", ".h"));
    _tpl->insert("includes", include);
}

void XcoreComponentRenderer::renderStartupSource(ReflexModelItem* item)
{
    // we need:
    // - incoming connections per core
    // - resourceHandler names per core
    // - incoming channel-name to link-handler
    // - outgoing link names

    // assume, that each direct child of root runs on its own core
    foreach (ReflexModelItem* child, item->children().byType(ComponentItem))
    {
        QString core = child->value("core").toString();
        Template* coreTpl = env()->rendererByItem(child)->tpl().data();
        QList<const ReflexModelConnection*> outConnections =
                _outConnPerCore.values(core);
        QList<const ReflexModelConnection*> inConnections =
                _inConnPerCore.values(core);

        QStringList inChannelNames;
        QStringList outChannelNames;
        QStringList channelDeclarations;
        foreach (const ReflexModelConnection* connection, inConnections)
        {
            CorePair pair;
            pair.first = connection->source()->value("core").toString();
            pair.second = core;
            QString name = _channelEnums.value(pair);
            inChannelNames << name;
            channelDeclarations << "chanend " + name;
        }
        foreach (const ReflexModelConnection* con, outConnections)
        {
            CorePair pair;
            pair.first = core;
            pair.second = con->destination()->value("core").toString();
            QString channelName = _channelEnums.value(pair);
            outChannelNames << channelName;
            channelDeclarations << "chanend " + channelName;

            // build an accessor string for the output in the
            // source component
            QString srcAccessor = QString("app.%1()")
                    .arg(con->source()->name());
            QString dstAccessor = QString("%1->in_%2()")
                    .arg(con->destination()->parent()->name())
                    .arg(con->destination()->name());

            coreTpl->insert("connections", QString("connect(%1, %2, %3);")
                            .arg(srcAccessor)
                            .arg(dstAccessor)
                            .arg(channelName));

        }
        inChannelNames.removeDuplicates();
        outChannelNames.removeDuplicates();
        channelDeclarations.removeDuplicates();


        for (quint32 i = 0; i < inChannelNames.size(); i++)
        {
            const QString& name = inChannelNames.at(i);
            QString channelHandlerName = QString("handler_%1").arg(i);
            coreTpl->insert("initXlinks", QString("app.%1.setResourceId(%2);")
                            .arg(channelHandlerName)
                            .arg(name));
        }

        coreTpl->insert("channels", channelDeclarations.join(", "));

        QString headerFile =
                child->value(
                        "header-file",
                        child->value("type-name").toString() + ".h"
                ).toString();
        QString include = QString("#include \"%1\"").arg(headerFile);

        QString instancePointer = QString("%1* %2;")
                .arg(child->value("type-name").toString())
                .arg(child->name());

        env()->tpl()->insert("instancePointers", instancePointer);
        env()->tpl()->insert("includes", include);

        coreTpl->insert("type-name", child->value("type-name").toString());
        coreTpl->insert("pointerName", child->name());
        coreTpl->insert("core", core);
    }

    // for each core do:
    //   write $core$
    //   write global pointer $pointer$ item->name
    //   foreach incoming connection do:
    //      write appName->linkName
    //      write connection: outgoing link-name, path to input
}

void XcoreComponentRenderer::renderNestedComponents()
{
    DefaultComponentRenderer::renderNestedComponents();
    ReflexModelItem* item = env()->item();

    // Add channel handlers for incoming xLink channels
    if ((item->isRoot() == false) && (item->parent()->isRoot() == true))
    {
        QString core = item->value("core").toString();
        QList<const ReflexModelConnection*> inConnections =
                _inConnPerCore.values(core);

        for (quint32 i = 0; i < inConnections.size(); i++)
        {
            tpl()->insert("components",
                          QString("reflex::xcore::ChannelHandler handler_%1;")
                          .arg(i));
        }

        tpl()->insert("includes", "#include <reflex/io/ChannelHandler.h>");


    }
}

void XcoreComponentRenderer::extractMetadata(const ReflexModelFragment& model)
{
    // _channelEnums: enum representations of channels
    foreach(const CorePair& pair, _crossCoreConnections)
    {
        if (_channelEnums.contains(qMakePair(pair.second, pair.first)))
        {
            _channelEnums.insert(pair,
                     QString("%1_%2")
                     .arg(pair.second)
                     .arg(pair.first));
        }
        else
        {
            _channelEnums.insert(pair,
                     QString("%1_%2")
                     .arg(pair.first)
                     .arg(pair.second));
        }
    }
}
