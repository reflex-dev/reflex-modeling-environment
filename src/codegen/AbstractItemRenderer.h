/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef RENDERER_H
#define RENDERER_H

#include <QtCore/QDir>
#include <QtCore/QSharedPointer>
#include <ErrorHandler.h>

class ReflexModelItem;
class ReflexModelFragment;
class RenderingEnvironment;
class Template;

/*!
\brief Basic renderer class for model items.

The rendering process itself is controlled by a global RenderingEnvironment
object.

 */
class AbstractItemRenderer : public ErrorHandler
{
    friend class RenderingEnvironment;

public:
    virtual ~AbstractItemRenderer() {}

    RenderingEnvironment* env();
    RenderingEnvironment* environment();
    QSharedPointer<Template> tpl();

protected:
    virtual void enterItem(ReflexModelItem* item) = 0;
    virtual void renderItem(ReflexModelItem* item) = 0;
    virtual void leaveItem(ReflexModelItem* item) = 0;

    virtual QString templateFile() = 0;
    virtual QString outputFile() = 0;


    QSharedPointer<Template> _tpl;
};

inline QSharedPointer<Template> AbstractItemRenderer::tpl() { return _tpl; }

#endif // RENDERER_H
