/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef XCORECOMPONENTRENDERER_H
#define XCORECOMPONENTRENDERER_H

#include "DefaultComponentRenderer.h"

#include <ReflexModelConnection.h>
#include <QtCore/QSet>
#include <QtCore/QPair>
#include <QtCore/QHash>
#include <QtCore/QMultiHash>

/*!
\brief Renderer for components on the xcore processor.

This renderer assumes, that all direct children of root run on their
own core. This has to be ensured by a preceeding model transformation.

 */
class XcoreComponentRenderer : public DefaultComponentRenderer
{
public:
    typedef QPair<QString, QString> CorePair;

    static QSet<QString> _cores;
    static QSet<CorePair> _crossCoreConnections;    // could be extracted from cross core connections
    static QHash<CorePair, QString> _channelEnums;
    static QMultiMap<QString, const ReflexModelConnection*> _inConnPerCore;
    static QMultiMap<QString, const ReflexModelConnection*> _outConnPerCore;

protected:
    virtual void enterItem(ReflexModelItem* item);
    virtual void leaveItem(ReflexModelItem* item);
    virtual void renderItem(ReflexModelItem* item);
    virtual void renderNestedComponents();
    virtual void renderOutput(ReflexModelItem* item);

    virtual QString templateFile();

    void renderForModelHeader(ReflexModelItem* item);
    void renderForMainSource(ReflexModelItem* item);
    void renderStartupSource(ReflexModelItem* item);

    static void extractMetadata(const ReflexModelFragment& model);

    QString test;

};

#endif // XCORECOMPONENTRENDERER_H
