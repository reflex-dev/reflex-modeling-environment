/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "DefaultComponentRenderer.h"
#include "RenderingEnvironment.h"

#include <ReflexModelItem.h>
#include <ReflexModelFragment.h>
#include <ReflexModelPath.h>
#include <ReflexModelQuery.h>
#include <Template.h>

#include <QtCore/QDateTime>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>

#include <QtCore/QtDebug>

QStringList DefaultComponentRenderer::renderedTypes;

DefaultComponentRenderer::DefaultComponentRenderer()
{

}

void DefaultComponentRenderer::renderNestedComponents()
{
    /* Nested components */
    QStringList declarations;
    QStringList includes;

    foreach(ReflexModelItem* component, env()->item()->children().byType(ComponentItem))
    {
        declarations << QString("::%1 %2;")
                        .arg(typeNameForItem(component), component->name());

        if (!component->values().contains("header-file"))
        {
            includes << QString("#include \"%1.h\"")
                        .arg(cleanTypename(component->value("type-name")));
        }
        else
        {
            QString header = component->value("header-file").toString();
            if (!header.isEmpty())
            {
                includes << QString("#include \"%1\"").arg(header);
            }
        }
    }

    includes.removeDuplicates();
    _tpl->insert("components", declarations);
    _tpl->insert("includes", includes);

}

void DefaultComponentRenderer::renderActivities()
{
    _tpl->insert("includes", QString("#include \"reflex/scheduling/ActivityFunctor.h\""));

    QStringList declarations;
    QStringList stubs;
    QStringList inits;

    foreach(ReflexModelItem* input, env()->item()->children().byType(InputItem))
    {
        declarations << QString("reflex::ActivityFunctor<%1, &%1::_run_%2> act_%2;").arg(
                            cleanTypename(env()->item()->value("type-name")), input->name());
        stubs << QString("void _run_%1() { static_cast<T*>(this)->run_%1(); }").arg(input->name());
        inits << QString("act_%1(*this)").arg(input->name());
    }

    _tpl->insert("activities", declarations);
    _tpl->insert("activityStubs", stubs);
    if (inits.size() > 0)
    {
        _tpl->insert("initList", QString(": %1").arg(inits.join(", ")));
    }
}

void DefaultComponentRenderer::renderInputs()
{
    /* Inputs */
    QStringList includes;
    QStringList declarations;
    QStringList inits;
    QStringList getter;
    foreach(ReflexModelItem* input, env()->item()->children().byType(InputItem))
    {
        includes << QString("#include \"%1\"").arg(input->value("header-file").toString());
        declarations << QString("%1 _in_%2;").arg(typeNameForItem(input)).arg(input->name());
        getter << QString("%1* in_%2() { return &_in_%2; }").arg(typeNameForItem(input)).arg(input->name());
        inits << QString("_in_%1.init(&act_%2);").arg(input->name()).arg(input->name());
    }

    includes.removeDuplicates();
    _tpl->insert("includes", includes);
    _tpl->insert("inputs", declarations);
    _tpl->insert("initializations", inits);
    _tpl->insert("inputGets", getter);
}

void DefaultComponentRenderer::renderOutputs()
{
    foreach(ReflexModelItem* output, env()->item()->children().byType(OutputItem))
    {
        renderOutput(output);
    }
}

void DefaultComponentRenderer::renderOutput(ReflexModelItem* item)
{
    QString includes;
    QString declarations;
    QString getter;
    QString setter;

    includes = QString("#include \"%1\"").arg(
                    item->value("header-file").toString());
    declarations = QString("%1* _%2;").arg(
                        typeNameForItem(item),
                        item->name());
    getter = QString("%1* %2() { return _%2; }").arg(
                  typeNameForItem(item),
                  item->name());
    setter = QString("void set_%1(%2* input) { this->_%1 = input; }").arg(
                  item->name(),
                  typeNameForItem(item));

    _tpl->insert("includes", includes);
    _tpl->insert("outputs", declarations);
    _tpl->insert("outputGets", getter);
    _tpl->insert("outputSets", setter);
}


void DefaultComponentRenderer::renderConnections()
{
    /* Connections pointing into nested components */

    QStringList connections;

    foreach(const ReflexModelConnection& connection, env()->item()->connections())
    {
        ReflexModelItem* src = connection.source();
        ReflexModelItem* dst = connection.destination();

        QString inputAccessorString = QString("in_%1()").arg(dst->name());
        // Destination is in a nested component
        if (dst->parent() != env()->item())
        {
            inputAccessorString.prepend(dst->parent()->name() + ".");
        }

        QString outputAccessorString;
        // Outputs may define their own connection methods
        if (!src->value("connect").isNull())
        {
            outputAccessorString = QString("%1(%2);")
                    .arg(src->value("connect").toString())
                    .arg(inputAccessorString);
        }
        else
        {
            outputAccessorString = QString("set_%1(%2);")
                    .arg(src->name())
                    .arg(inputAccessorString);
        }
        // Source is in a nested component
        if (src->parent() != env()->item())
        {
            outputAccessorString.prepend(src->parent()->name() + ".");
        }

        connections << outputAccessorString;
    }

    _tpl->insert("connections", connections);
}

/*!
\brief Emits code to set the priority of components from within root item.

This method assumes priority scheduling.
 */
void DefaultComponentRenderer::setPriorities()
{
    Q_ASSERT(env()->item() == env()->model()->root());

    QList<ReflexModelItem*> components;
    components << env()->model()->root();
    components << ReflexModelQuery(env()->item()).
                  childrenByType(ComponentItem, ReflexModelQuery::Recursive);

    foreach(ReflexModelItem* item, components)
    {
        // Ignore components without priority, they
        // use default priority
        if (item->value("priority").isNull())
        {
            continue;
        }

        int priority = item->value("priority").toInt();
        QString path = ReflexModelPath::fromItem(item).join('.');
        if (!path.isEmpty())
        {
            path.append('.');
        }

        QString pattern = "%1setPriority(%2);";
        _tpl->insert("initializations", pattern.arg(path).arg(priority));
    }
}

/*!
\brief Emits methods to set the priority of all activities in the component.

This method assumes priority scheduling.
 */
void DefaultComponentRenderer::renderPriorityMethods()
{

    if (env()->item()->isBuiltIn())
    {
        return;
    }

    QStringList priorities;
    foreach(ReflexModelItem* item,
            ReflexModelQuery(env()->item()).childrenByType(InputItem))
    {
        const QString pattern = "act_%1.setPriority(value);";
        priorities << pattern.arg(item->name());
    }

    _tpl->insert("priorities", priorities);
}

/*!
\brief Emits code to register activities in timeslots.

This method assumes time-triggered scheduling.
 */
void DefaultComponentRenderer::setTimeslots()
{
    Q_ASSERT(env()->item() == env()->model()->root());

    QList<ReflexModelItem*> activities;
    // Retrieve a list of all reachable activities (=inputs) in the model,
    // even for built-in components
    activities << ReflexModelQuery(env()->item()).
                  childrenByType(InputItem, ReflexModelQuery::Recursive);

    foreach(ReflexModelItem* item, activities)
    {
        if (item->value("timeslot").isNull())
        {
            QString path = ReflexModelPath::fromItem(item).join('.');
            fprintf(stderr, "%s\n", qPrintable(
                        QObject::tr("Warning: No timeslot is set for '%1'. The corresponding activity will never be executed.")
                        .arg(path)));
            continue;
        }

        QStringList path;
        path << ReflexModelPath::fromItem(item->parent());
        path << QString("act_%1").arg(item->name());
        const QString pattern = "getSystem().scheduler.registerActivity(&%1, %2);";

        const QStringList timeslots = item->value("timeslot").toString().simplified().split(' ');
        foreach(const QString& timeslot, timeslots)
        {
            _tpl->insert("initializations", pattern.arg(path.join('.')).arg(timeslot));
        }
    }
}

QString DefaultComponentRenderer::typeNameForItem(ReflexModelItem* item)
{
    QString result = item->value("type-name").toString();

    if (item->value("template").toInt() > 0)
    {
        QStringList templateArgs;
        for (int i = 1; i <= item->value("template").toInt(); i++)
        {
            QString arg = item->value(QString("T%1").arg(i)).toString();
            if (arg.isEmpty() == false)
            {
                templateArgs << arg;
            }
        }

        if (templateArgs.size() > 0)
        {
            result += '<' + templateArgs.join(", ") + '>';
        }
    }
    return result;
}

void DefaultComponentRenderer::enterItem(ReflexModelItem* item)
{
    if ((env()->command() == "model-header")
            && (renderedTypes.contains(item->value("type-name").toString())))
    {
        return;
    }

    ReflexModelFragment* model = item->owner();

    _tpl = QSharedPointer<Template>(new Template());
    _tpl->setTemplateFileName(templateFile());
    _tpl->setOutputFileName(outputFile());

    if ((env()->command() == "model-header")
            && (item->isRoot()))
    {
        QSharedPointer<Template> rootFileTpl =
                QSharedPointer<Template>(new Template());
        rootFileTpl->setOutputFileName(env()->outputFilename());
        rootFileTpl->setTemplateFileName(
                    env()->templateDir()
                    .filePath("DefaultModelHeader.tpl.h")
        );
        rootFileTpl->insert("time", QDateTime::currentDateTime().toLocalTime().toString());
        rootFileTpl->insert("generator", QString("rme codegen version %1").arg(RME_VERSION));
        rootFileTpl->insert("model-file", model->root()->value("model-file").toString());
        rootFileTpl->insert("type", cleanTypename(model->root()->value("type-name")));
        env()->setTemplate(rootFileTpl);
    }
    else if ((env()->command() == "main-source")
            && (item->isRoot()))
    {
        _tpl->insert("time", QDateTime::currentDateTime().toLocalTime().toString());
        _tpl->insert("generator", QString("rme codegen version %1").arg(RME_VERSION));
        _tpl->insert("model-file", model->root()->value("model-file").toString());
        env()->setTemplate(_tpl);
    }
    else if ((env()->command() == "startup-source")
            && (item->isRoot()))
    {
        _tpl->insert("time", QDateTime::currentDateTime().toLocalTime().toString());
        _tpl->insert("generator", QString("rme codegen version %1").arg(RME_VERSION));
        _tpl->insert("model-file", model->root()->value("model-file").toString());
        env()->setTemplate(_tpl);
    }
}


void DefaultComponentRenderer::leaveItem(ReflexModelItem* item)
{
    if (env()->command() == "model-header")
    {
        QString typeName = item->value("type-name").toString();
        if (renderedTypes.contains(typeName))
        {
            return;
        }
        else
        {
            renderedTypes << typeName;
        }
        env()->tpl()->insert("classes", _tpl);
    }

    if (item->isRoot())
    {
        env()->tpl()->save();
    }
}

void DefaultComponentRenderer::renderItem(ReflexModelItem* item)
{
    if ((env()->command() == "model-header")
            && (renderedTypes.contains(item->value("type-name").toString())))
    {
        return;
    }

    if ((env()->command() == "main-source")
            && (item->isRoot()))
    {
        _tpl->insert("type-name", item->value("type-name").toString());
        _tpl->insert("name", item->name());

        if (item->values().contains("header-file"))
        {
            QString include =
                    QString("#include \"%1\"")
                    .arg(item->value("header-file").toString());
            _tpl->insert("includes", include);
        }
        else
        {
            QString sourceFile = item->value("model-file").toString();
            qDebug() << "model-file: " << sourceFile;
            QString header = QFileInfo(sourceFile).baseName() + ".h";
            QString include = QString("#include \"%1\"").arg(header);
            qDebug() << "include: " << include;
            _tpl->insert("includes", include);
        }
    }
    else if (env()->command() == "model-header")
    {
        _tpl->insert("includes", QString("#include \"reflex/types.h\""));
        _tpl->insert("type", cleanTypename(item->value("type-name")));

        renderNestedComponents();
        renderActivities();
        renderInputs();
        renderOutputs();
        renderConnections();

        QString scheduling = env()->model()->root()->value(
                    "scheduling", "FifoScheduling").toString();

        // Determine application scheduling
        if ((scheduling == "PriorityScheduling")
                || (scheduling == "NonpreemptivePriorityScheduling")
                || (scheduling == "SimplePriorityScheduling"))
        {
            renderPriorityMethods();
            // Only root component must set priorities
            if (item == env()->model()->root())
            {
                setPriorities();
            }
        }
        else if (scheduling == "TimeTriggeredScheduling")
        {
            // Only root component must set timeslots
            if (item == env()->model()->root())
            {
                setTimeslots();
            }
        }
        else if (scheduling == "FifoScheduling")
        {
        }
    }
}

QString DefaultComponentRenderer::outputFile()
{
    if ((env()->command() == "main-source") && (env()->item()->isRoot()))
        return env()->outputFilename();
    if ((env()->command() == "startup-source") && (env()->item()->isRoot()))
        return env()->outputFilename();
    else
        return "";
}

QString DefaultComponentRenderer::templateFile()
{
    QString command = env()->command();
    bool isRoot = (env()->item() == env()->model()->root());

    if (command == "model-header")
        return env()->templateDir().filePath("DefaultComponentModel.tpl.h");
    else if ((command == "main-source") && isRoot)
        return env()->templateDir().filePath("main.tpl.cc");
    else if ((command == "startup-source") && isRoot)
        return env()->templateDir().filePath("startup.tpl.cc");
    else return "";
}

/*!
 * \brief Remove preceeding namespaces
 */
QString DefaultComponentRenderer::cleanTypename(const QString& typeName)
{
    if (typeName.contains("::"))
        return typeName.split("::").last();
    else
        return typeName;
}

QString DefaultComponentRenderer::cleanTypename(const QVariant& typeName)
{
    return cleanTypename(typeName.toString());
}
