/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef REFLEXMODELPATH_H_
#define REFLEXMODELPATH_H_

#include <QtCore/QStringList>

class ReflexModelFragment;
class ReflexModelItem;
class QChar;

/*!
\brief String representation of a model item.

Every model item can also be seen as a list of names defining a path through
the model tree.
 */
class ReflexModelPath : public QStringList
{
public:
    static ReflexModelPath fromItem(const ReflexModelItem* item);

};

#endif /* REFLEXMODELPATH_H_ */
