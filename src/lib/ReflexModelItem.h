/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _REFLEXMODELITEM_H
#define _REFLEXMODELITEM_H

#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QSet>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include <QtCore/QVariant>

#include "ModelItemList.h"
#include "ModelItemTypes.h"
#include "ReflexModelConnection.h"

class ReflexModelFragment;

/*!
\brief A node in the REFLEX model-tree.


 */
class ReflexModelItem
{
public:
    friend class DisconnectCommand;
    friend class ReflexModelConnection;
    friend class ReflexModelFragment;

    friend class TestReflexModelItem;
    friend class TestReflexModelFragment;

    ReflexModelItem(ItemTypeFlag type, const QString& typeName, const QString& name = "");
    ReflexModelItem(const ReflexModelItem& other);
    ~ReflexModelItem();
    const QSet<ReflexModelConnection>& connections() const;
    const ModelItemList& children() const;
    QSet<ReflexModelConnection> incoming() const;
    bool inherits(const QString& key, const QVariant& value) const;
    bool isBuiltIn() const;
    bool isConnected() const;
    bool isRoot() const;
    QString name() const;
    ReflexModelConnection outgoing() const;
    ReflexModelFragment* owner() const;
    ReflexModelItem* parent() const;
    void setValue(const QString& key, const QVariant& value);
    ItemTypeFlag type() const;
    QVariant value(const QString& key) const;
    QVariant value(const QString& key, const QVariant& defaultValue) const;
    QHash<QString, QVariant> values() const;

protected:
    QSharedPointer<ReflexModelItem> clone() const;

private:
    ReflexModelItem();

    //! The assignment-operator has been disabled for safety reasons.
    ReflexModelItem& operator=(const ReflexModelItem&);

    ModelItemList _children;
    QHash<QString, QVariant> _data;
    ReflexModelFragment* _owner;
    ReflexModelItem* _parent;
    ItemTypeFlag _type;

    QSet<ReflexModelConnection> _connections;
    ReflexModelConnection _destination;
    QSet<ReflexModelConnection> _sources;
};


inline ReflexModelItem::~ReflexModelItem() {}

/*!
Returns a list of all direct children items.

The list contains inputs, outputs and sub-components in their inclusion order.
This method returns an immutable reference. For modifications or adding and
removing items, use the interface of ReflexModelFragment.

\see ReflexModelFragment::insert(),
     ReflexModelFragment::remove,
     ReflexModelFragment::replace()

 */
inline const ModelItemList& ReflexModelItem::children() const { return _children; }


/*!
Returns all nested connections if the item is a ComponentItem.

This method returns an unordered set of connections for which the item is
responsible. If the item is not a component, an empty set is returned instead.

The set is immutable. For modifications in connections, use
ReflexModelFragment.

\see ReflexModelFragment::connect(),
     ReflexModelFragment::disconnect()

 */
inline const QSet<ReflexModelConnection>& ReflexModelItem::connections() const { return _connections; }


/*!
Returns all connection objects which point into the item.

In the Reflex model multiple output items may point to the same input. This
method returns all related output objects. If the item is not an input, an
empty set is teturned instead.

 */
inline QSet<ReflexModelConnection> ReflexModelItem::incoming() const { return _sources; }


/*!
Returns true if the item is built-in item.

Convenience method which shortens access to key "built-in".

\see ReflexModelItem::value()

 */
inline bool ReflexModelItem::isBuiltIn() const
{
    return _data.value("built-in").toBool();
}

/*!
Returns the name of the item.

\see ModelItemList::byName()
 */
inline QString ReflexModelItem::name() const {	return _data.value("name").toString(); }


/*!
Returns the connection object pointing to a related input item.

If the item is neither an output nor is it connected, an invalid connection
object is returned and ReflexModelConnection::isNull() is true.

\see ReflexModelConnection::isNull(),
     ReflexModelConnection::destination()

 */
inline ReflexModelConnection ReflexModelItem::outgoing() const { return _destination; }


/*!
Returns the owner fragment of the item.

 */
inline ReflexModelFragment* ReflexModelItem::owner() const { return _owner; }


/*!
Returns the parent component of the item or null when being the root item.

Every item has a parent item, in which context it lives.

\see ReflexModelItem::children()
 */
inline ReflexModelItem* ReflexModelItem::parent() const { return _parent; }

/*!
Returns the type of the item.

Model items can come in different roles: input, output, component.

 */
inline ItemTypeFlag ReflexModelItem::type() const { return _type; }


/*!
Returns the value stored for a \a key from the internal key-value database.

Every item can have an arbitrary database filled with QVariant and indexed
by QString keys.
 */
inline QVariant ReflexModelItem::value(const QString & key) const { return _data.value(key); }

/*!
Returns the value stored for a \a key from the internal key-value database.

If \a key is not set, \a defaultValue is returned instead. Every item can
have an arbitrary database filled with QVariant and indexed by QString
keys.
 */
inline QVariant ReflexModelItem::value(const QString& key,
                                       const QVariant& defaultValue) const
{
    return _data.value(key, defaultValue);
}

/*!
Returns the whole internal key-value database of the item.
 */
inline QHash<QString, QVariant> ReflexModelItem::values() const { return _data; }

#endif
