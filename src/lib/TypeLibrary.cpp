/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "TypeLibrary.h"

#include <QtDebug>
#include <QtCore/QStringList>

TypeLibrary::TypeLibrary()
{
    ReflexModelFragment fragment;

    QSharedPointer<ReflexModelItem> item = QSharedPointer<ReflexModelItem> (
                new ReflexModelItem(InputItem, "InputItem"));
    fragment.insert(item, 0);
    fragment.root()->setValue("built-in", true);
    fragment.root()->setValue("default-name", "in");
    fragment.root()->setValue("hasActivity", true);
    fragment.root()->setValue("template", 3);
    TypeLibrary::insert("InputItem", fragment);

    item = QSharedPointer<ReflexModelItem> (new ReflexModelItem(OutputItem, "OutputItem"));
    fragment.insert(item, 0);
    fragment.root()->setValue("built-in", true);
    fragment.root()->setValue("default-name", "out");
    TypeLibrary::insert("OutputItem", fragment);
}

bool TypeLibrary::contains(const QString& type) const
{
    Q_ASSERT(!type.isEmpty());
    return _library.contains(type);
}

ReflexModelItem* TypeLibrary::find(const QString& type) const
{
    Q_ASSERT(!type.isEmpty());
    if (_library.contains(type))
    {
        return _library.value(type).root();
    }
    else
    {
        return 0;
    }
}

TypeLibrary::Map::Iterator TypeLibrary::insert(const QString& type, const ReflexModelFragment& model)
{
    Q_ASSERT(model.root());
    Q_ASSERT(!type.isEmpty());

    if (_library.contains(type))
    {
        remove(type);
    }

    Map::Iterator i = _library.insert(type, model);
    return i;
}

void TypeLibrary::remove(const QString& type)
{
    _library.remove(type);
}

ReflexModelFragment TypeLibrary::instanciate(const QString& typeName, const QString& name, const QString& t1,
                                             const QString& t2, const QString& t3) const
{
    /* Try to match typeName */
    if (_library.contains(typeName))
    {
        ReflexModelFragment result = _library.value(typeName);
        result.root()->setValue("name", name);
        if (!t1.isEmpty())
        {
            result.root()->setValue("T1", t1);
        }
        if (!t2.isEmpty())
        {
            result.root()->setValue("T2", t2);
        }
        if (!t3.isEmpty())
        {
            result.root()->setValue("T3", t3);
        }
        return result;
    }
    else
    {
        ReflexModelFragment defaultFragment;
        defaultFragment.insert(
                    QSharedPointer<ReflexModelItem> (
                        new ReflexModelItem(ComponentItem, typeName, name)), 0);
        return defaultFragment;

    }
}

void TypeLibrary::replaceWith(const TypeLibrary& library)
{
    _library = library._library;
    defaultScope = library.defaultScope;
}
