/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelItem.h"
#include "ReflexModelFragment.h"
#include "ReflexModelQuery.h"

/*!
Creates an item with a given \a type, \a typeName and \a name.

 */
ReflexModelItem::ReflexModelItem(ItemTypeFlag type, const QString & typeName,
        const QString & name)
{
    _owner = 0;
    _parent = 0;
    _type = type;
    setValue("type-name", typeName);
    setValue("name", name);
}

/*!
Creates an item by cloning an \a other item.

This method creates a shallow copy of the item with the same type, metadata,
parent and owner fragment. But children items and connections are not cloned.

 */
ReflexModelItem::ReflexModelItem(const ReflexModelItem& other)
{
    this->_owner = other._owner;
    this->_parent = other._parent;
    this->_type = other._type;
    this->_data = other._data;
}

/*!
Returns a copy of this item.

This method creates a shallow copy of this item and returns a shared pointer
which can be stored in a new model fragment or somewhere else. Children and
connections are not cloned.

To perform a deep copy of an item, use ReflexModelFragment::clone() instead.

 */
QSharedPointer<ReflexModelItem> ReflexModelItem::clone() const
{
    return QSharedPointer<ReflexModelItem>(new ReflexModelItem(*this));
}


/*!
Returns true, if the item or one of its parents contains \a value in \a key.

 */
bool ReflexModelItem::inherits(const QString& key, const QVariant& value) const
{
    for (const ReflexModelItem* i = this; i != 0; i = i->parent())
    {
        if (i->value(key) == value)
        {
            return true;
        }
    }

    return false;
}

/*!
Returns true, if the item is connected either to an input or an output.

 */
bool ReflexModelItem::isConnected() const
{
    return !_destination.isNull() || (_sources.size() > 0);
}

/*!
Returns true if the item is the model's root item.
*/
bool ReflexModelItem::isRoot() const
{
    Q_ASSERT(_owner != 0);
    return this == owner()->root();
}

/*!
Adds \a value to the item's metadata with \a key as the index.
 */
void ReflexModelItem::setValue(const QString& key, const QVariant& value)
{
    _data.insert(key, value);
}

