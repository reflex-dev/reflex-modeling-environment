/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelConnection.h"
#include "ReflexModelItem.h"

ReflexModelConnectionData::ReflexModelConnectionData()
{
    destination = 0;
    parent = 0;
    source = 0;
    attached = false;
}

ReflexModelConnection::ReflexModelConnection()
{
    d = new ReflexModelConnectionData();
}

/*!
 The parameters \a source and destination must not be null. The connection object is attached by default.
 */
ReflexModelConnection::ReflexModelConnection(ReflexModelItem* parent, ReflexModelItem* source, ReflexModelItem* destination)
{
    Q_ASSERT(source && destination);

    d = new ReflexModelConnectionData();
    d->destination = destination;
    d->parent = parent;
    d->source = source;
    d->attached = false;
    setAttached(true);
}

ReflexModelItem* ReflexModelConnection::parent() const
{
    return d->parent;
}

/*!
 Re-connects itself to source and destination if \a attached is true. Setting
 \a attached true on an already attached object does not change the connection
 state.
 */
void ReflexModelConnection::setAttached(bool attached)
{
    Q_ASSERT(!isNull());

    if (attached)
    {
        d->destination->_sources.insert(*this);
        d->source->_destination = *this;
    }
    else
    {
        d->destination->_sources.remove(*this);
        d->source->_destination = ReflexModelConnection();
    }

    d->attached = attached;
}

void ReflexModelConnection::setDestination(ReflexModelItem* item)
{
    Q_ASSERT(item != 0);
    Q_ASSERT(!isNull());

    if (item == d->destination)
    {
        return;
    }

    if (d->attached == true)
    {
        d->destination->_sources.remove(*this);
        item->_sources.insert(*this);
    }

    d->destination = item;
}

void ReflexModelConnection::setSource(ReflexModelItem* item)
{
    Q_ASSERT(item != 0);
    Q_ASSERT(!isNull());

    if (item == d->source)
    {
        return;
    }

    if (d->attached == true)
    {
        d->source->_destination = ReflexModelConnection();
        item->_destination = *this;
    }

    d->source = item;
}
