/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelScriptReader.h"
#include "TypeLibrary.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>
#include <QtCore/QDebug>

QStringList ReflexModelScriptReader::importPaths;

QRegExp ReflexModelScriptReader::connectionReg = QRegExp("^([\\w_\\.]+)\\s?\\-+\\>\\s([\\w_\\.]+)$");
QRegExp ReflexModelScriptReader::definitionReg = QRegExp("^([\\w_:]+)(?:\\s\\(([\\w_:]+)\\))?(?:\\s([\\w_]+)\\s?)?:$");
QRegExp ReflexModelScriptReader::importReg = QRegExp("^import ([\\w_\\-\\.]*)$");
QRegExp ReflexModelScriptReader::instanciationReg = QRegExp("^([\\w:]+)(?:<\\s?(?:([\\w:{0,0}\\*]+))(?:\\s?,\\s?([\\w:]+))?(?:\\s?,\\s?([\\w:]+))?\\s?>)?\\s([\\w_]*)$", Qt::CaseInsensitive, QRegExp::RegExp2);
QRegExp ReflexModelScriptReader::keyValueReg = QRegExp("^([\\w_-]+)\\s?:(?!:)\\s?(.*)$");

ReflexModelScriptReader::ReflexModelScriptReader()
{

}

ReflexModelScriptReader::ReflexModelScriptReader(
        const ReflexModelScriptReader& other)
{
    visitedFiles = other.visitedFiles;
    _library = other._library;
}


void ReflexModelScriptReader::parseLine(const QString& line)
{
    QRegExp lineReg = QRegExp("(\\s*)(.*)");
    lineReg.exactMatch(line);
    QString leadingSpaces = lineReg.cap(1);
    QString expression = lineReg.cap(2);

    // Ignore empty lines
    if (expression.isEmpty())
    {
        //qDebug() << "Ignore empty line.";
        return;
    }

    // Ignore comments
    if (expression.startsWith('#'))
    {
        //qDebug() << "Ignore comment.";
        return;
    }

    // Consume leading indentations
    QStack<Context>::Iterator currentContext = contexts.begin();
    while (!leadingSpaces.isEmpty() && (currentContext != contexts.end()))
    {
        const QString& indent = currentContext->indent;
        VERIFY_X(leadingSpaces.startsWith(indent),
                 QString("Indentation format has to be indentical on each level. "
                         "Expected '%1', but got '%2'")
                 .arg(indent)
                 .arg(leadingSpaces));

        leadingSpaces.remove(0, indent.size());
        currentContext++;
    }

    // Enter or leave context depending on the indentation level
    if (!leadingSpaces.isEmpty())
    {
        enterContext(leadingSpaces);
    }
    else if (currentContext != contexts.end())
    {
        while(contexts.end() != currentContext)
        {
            leaveContext();
        }
    }

    // Remove multiple white spaces inside the line to simplify pattern matching
    expression = expression.simplified();

    if (importReg.exactMatch(expression))
    {
        QString importValue = importReg.cap(1);
        importValue.replace('.', '/');
        QString fileName = QString("%1.rml").arg(importValue);

        if (visitedFiles.contains(QFileInfo(fileName).baseName()))
            return;
        else
            visitedFiles << QFileInfo(fileName).baseName();

        bool found = false;
        foreach(QDir dir, importPaths)
        {
            if (dir.exists(fileName))
            {
                ReflexModelScriptReader reader(*this);
                reader.setModelFile(dir.filePath(fileName));
                visitedFiles = reader.visitedFiles;
                VERIFY_X(!reader.hasErrors(), reader.errorString());
                found = true;
                break;
            }
        }
        VERIFY_X(found == true,
                 QString("The model file '%1' could not been found in the library. "
                         "Be sure to add all folders to the library-path.")
                 .arg(fileName));
    }
    else if (definitionReg.exactMatch(expression))
    {
        QString typeName = definitionReg.cap(1);
        QString baseType = definitionReg.cap(2);
        QString name = definitionReg.cap(3);
        //qDebug() << QString("'%1' is a definition.").arg(expression) << typeName << baseType << name;

        VERIFY_X(_library->contains(typeName) == false,
                 QString("The component '%1' has already been defined earlier.")
                 .arg(typeName));

        ReflexModelFragment fragment;
        if (baseType.isEmpty() == false)
        {
            VERIFY_X(_library->contains(baseType),
                     QString("Base type '%1' has not been defined.")
                     .arg(baseType));
            fragment = _library->instanciate(baseType, name);
            fragment.root()->setValue("type-name", typeName);
        }
        else
        {
            fragment = _library->instanciate(typeName, name);
        }

        ReflexModelItem* parentItem = contexts.size() > 0 ? contexts.top().item.data() : 0;
        ReflexModelItem* insertedItem = _model.insert(fragment, parentItem);
        insertedItem->setValue("model-file", _fileName);
        recentContext.item = _model.pointerToItem(insertedItem);
        recentContext.mode = Context::Definition;
    }
    else if (instanciationReg.exactMatch(expression))
    {
        QString typeName = instanciationReg.cap(1);
        QString t1 = instanciationReg.cap(2);
        QString t2 = instanciationReg.cap(3);
        QString t3 = instanciationReg.cap(4);
        QString name = instanciationReg.cap(5);
        //qDebug() << QString("'%1' is an item instanciation.").arg(expression);

        VERIFY_X(_library->contains(typeName),
                 QString("Type '%1' is not defined.").arg(typeName));

        ReflexModelFragment fragment =  _library->instanciate(typeName, name, t1, t2, t3);
        ReflexModelItem* parentItem = contexts.top().item.data();
        ReflexModelItem* insertedItem = _model.insert(fragment, parentItem);
        recentContext.item = _model.pointerToItem(insertedItem);
        recentContext.mode = Context::Instanciation;
    }
    else if (keyValueReg.exactMatch(expression))
    {
        QString key = keyValueReg.cap(1);
        QString value = keyValueReg.cap(2).trimmed();
        //qDebug() << QString("'%1' is a key value pair with key: '%2' and value: '%3'.").arg(expression).arg(key).arg(value);
        // Remove possible quotes
        if ((value.startsWith("\"")) && (value.endsWith("\"")))
        {
            value = value.mid(1,value.length()-2);
        }
        contexts.top().item->setValue(key, value);
    }
    else if (connectionReg.exactMatch(expression))
    {
        QString srcPath = connectionReg.cap(1);
        QString dstPath = connectionReg.cap(2);
        //		qDebug() << QString("'%1' is a connection between '%2' and '%3'.")
        //						.arg(expression).arg(srcPath).arg(dstPath);

        ReflexModelItem* parentItem = contexts.top().item.data();
        ReflexModelItem* srcItem = cd(parentItem, srcPath);

        VERIFY(srcItem != 0);
        VERIFY_X(srcItem->type() == OutputItem,
                 QString("Cannot start connection at '%1'"
                         "because it is not an output item.")
                 .arg(srcItem->name()));

        ReflexModelItem* dstItem = cd(parentItem, dstPath);
        VERIFY(dstItem != 0);
        VERIFY_X(dstItem->type() == InputItem,
                 QString("Cannot route connection to '%1' "
                         "because it is not an input item.")
                 .arg(dstItem->name()));

        _model.connect(srcItem, dstItem);
    }
    else
    {
        raiseError(QString("'%1' is unknown syntax.").arg(expression));
    }

}

void ReflexModelScriptReader::enterContext(const QString& indent)
{
    //qDebug() << "Enter context.";
    recentContext.indent = indent;
    contexts.push(recentContext);
}

void ReflexModelScriptReader::leaveContext()
{
    //qDebug() << QString("Leave context.");
    recentContext = contexts.pop();

    if (recentContext.mode == Context::Definition)
    {
        // qDebug() << "Inserting " << recentContext.item->value("type-name").toString();
        _library->insert(recentContext.item->value("type-name").toString(),
                         _model.clone(recentContext.item.data()));
    }

}


/**
 * Searches an element in the model via an absolute or
 * relative path.
 */
ReflexModelItem* ReflexModelScriptReader::cd(ReflexModelItem* parent, const QString& path)
{
    Q_ASSERT(parent);

    QStringList chunks = path.split(QChar('.'), QString::SkipEmptyParts);
    ReflexModelItem* item = parent;
    foreach (const QString& chunk, chunks)
    {
        if (item->type() != ComponentItem)
        {
            raiseError(QString("'%1' can not have a child '%2' "
                               "because it is not a component.")
                       .arg(item->name())
                       .arg(chunk));
            return 0;
        }
        ReflexModelItem* child = ReflexModelQuery(item).childByName(chunk);
        if (!child)
        {
            raiseError(QString("'%1' does not have a child with name '%2'")
                       .arg(item->name()).arg(chunk));
            return 0;
        }
        item = child;
    }

    return item;
}

void ReflexModelScriptReader::setModelFile(const QString& fileName)
{
    VERIFY_X(_library != 0, "No library has been specified.");

    QFileInfo info = QFileInfo(fileName);
    this->_fileName = info.fileName();
    importPaths.append(info.absolutePath());
    importPaths.removeDuplicates();
    QFile file(fileName);

    VERIFY_X(file.exists(),
             QString("The model file '%1' does not exist.").arg(fileName));
    VERIFY_X(file.open(QIODevice::ReadOnly),
             QString("The model file '%1' could not be opended. %2")
             .arg(fileName)
             .arg(file.errorString()));

    QTextStream in(&file);

    VERIFY_X(!in.atEnd(), "The model file is empty.");

    QString firstLine = in.readLine();
    VERIFY_X(firstLine == "Reflex model file version 0.2",
             "This model file is not supported.");

    for (line = 2; !in.atEnd(); line++)
    {
        parseLine(in.readLine());
        VERIFY_X(!hasErrors(), QString("In '%1' line %2: %3")
                 .arg(_fileName)
                 .arg(line)
                 .arg(errorString()));
    }

    if (contexts.isEmpty() && (recentContext.mode == Context::Definition))
    {
        _library->insert(recentContext.item->value("type-name").toString(),
                         _model.clone(recentContext.item.data()));
    }
    else
    {
        while (!contexts.isEmpty())
        {
            leaveContext();
        }
    }
}

void ReflexModelScriptReader::setLibrary(TypeLibrary* library)
{
    this->_library = library;
}

