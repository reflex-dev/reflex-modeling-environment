/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelFragment.h"
#include "ReflexModelItem.h"
#include "ReflexModelScriptWriter.h"
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QTextCodec>

ReflexModelScriptWriter::ReflexModelScriptWriter(QIODevice* device) :
    ReflexModelWriter(device), _stream(device)
{
    _stream.setCodec(QTextCodec::codecForName("UTF-8"));
    level = 0;
}

void ReflexModelScriptWriter::writeModel(const ReflexModelFragment& model, ReflexModelItem* rootItem)
{
    this->_model = model;
    if (rootItem == 0)
    {
        rootItem = model.root();
    }
    this->handle(ModelBeginEvent);
    if (rootItem)
    {
        this->traverse(rootItem);
    }
    this->handle(ModelEndEvent);
}

void ReflexModelScriptWriter::traverse(ReflexModelItem* element)
{
    Q_ASSERT(element);
    begin(element);
    foreach(ReflexModelItem* c, element->children().byType(ComponentItem))
    {
        traverse(c);
    }

    foreach(ReflexModelItem* a, element->children().byType(ActivityItem))
    {
        Q_ASSERT(a != 0);
        begin(a);
        end(a);
    }

    foreach(ReflexModelItem* i, element->children().byType(InputItem))
    {
        Q_ASSERT(i != 0);
        begin(i);
        end(i);
    }

    foreach(ReflexModelItem* o, element->children().byType(OutputItem))
    {
        Q_ASSERT(o != 0);
        begin(o);
        end(o);
    }
    end(element);
}

void ReflexModelScriptWriter::handle(RendererEvent event)
{
    switch (event)
    {
    case ModelBeginEvent:
        echo("# Reflex model file version 0.1");
        //begin(_model.root());
        break;
    case ModelEndEvent:
        //end(_model.root());
        break;
    default:
        break;
    }
}

void ReflexModelScriptWriter::begin(ReflexModelItem* element)
{
    QString typeName = element->value("type-name").toString();
    QString name = element->name();
    bool isBuiltIn = element->value("built-in").toBool();

    if (element->value("template").toBool() == true)
    {
        QStringList templates;
        templates << element->value("T1").toString() << element->value("T2").toString() << element->value(
                         "T3").toString();
        templates.removeAll("");
        typeName = QString("%1<%2>").arg(typeName).arg(templates.join(","));
    }

    switch (element->type())
    {
    case ComponentItem:
        echo(QString("add %1 %2").arg(typeName, name));
        level++;
        if (!isBuiltIn)
        {
            echo(QString("cd %1").arg(element->name()));
        }
        break;
    case ActivityItem:
        echo(QString("add %1 %2").arg(typeName, name));
        break;
    case InputItem:
        if (element->parent() && !element->parent()->value("built-in").toBool())
        {
            echo(QString("add %1 %2").arg(typeName, name));
        }
        _pendingInputs.append(element);
        break;
    case OutputItem:
        if (element->parent() && !element->parent()->value("built-in").toBool())
        {
            echo(QString("add %1 %2").arg(typeName, name));
        }
    default:
        break;
    }

    if (!isBuiltIn)
    {
        QHash<QString, QVariant> values = element->values();
        for (QHash<QString, QVariant>::ConstIterator i = values.constBegin(); i != values.constEnd(); i++)
        {
            if ((i.key() == "name") || (i.key() == "type-name"))
            {
                continue;
            }
            echo(QString("set %1 %2").arg(i.key()).arg(i.value().toString()));
        }
    }
}

void ReflexModelScriptWriter::end(ReflexModelItem* element)
{
    bool isBuiltIn = element->value("built-in").toBool();

    switch (element->type())
    {
    case ComponentItem:
        if (isBuiltIn == false)
        {
            foreach(const ReflexModelConnection& connection, element->connections())
            {
                ReflexModelItem* src = connection.source();
                ReflexModelItem* dst = connection.destination();

                QString srcName = src->name();
                QString dstName = dst->name();

                if (src->parent() != element)
                {
                    srcName.prepend(src->parent()->name() + "/");
                }

                if (dst->parent() != element)
                {
                    dstName.prepend(dst->parent()->name() + "/");
                }

                echo(QString("connect %1 %2").arg(srcName).arg(dstName));
            }

            if (element != _model.root())
            {
                echo("cd ..");
            }
        }
        level--;
        break;
    case InputItem:
    {
        //		ReflexModelItem* dst = _model.destination(element);
        //		if (dst)
        //		{
        //			echo(QString("connect %1 %2").arg(element->name()).arg(dst->name()));
        //		}
    }
        break;
    case OutputItem:
        break;
    default:
        break;
    }
}

void ReflexModelScriptWriter::echo(const QString& line)
{
    for (int i = 0; i < level; i++)
    {
        _stream << "\t";
    }
    _stream << line << "\n";
}
