/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelItem.h"
#include "ReflexModelFragment.h"
#include "ReflexModelQuery.h"

#include <QtCore/QtGlobal>
#include <QtCore/QDebug>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>

ReflexModelQuery::ReflexModelQuery(const ReflexModelFragment& model) :
    _root(model.root())
{
    _recursionLevel = 0;
    _currentItem = 0;
}

ReflexModelQuery::ReflexModelQuery(ReflexModelItem* rootItem) : _root(rootItem)
{
    _recursionLevel = 0;
    _currentItem = 0;
}


QList<ReflexModelItem*> ReflexModelQuery::childrenByType(TypeFlags types, ReflexModelQuery::QueryFlags flags)
{
    if (_recursionLevel == 0)
    {
        _currentItem = _root;
    }
    _recursionLevel++;

    foreach(const QSharedPointer<ReflexModelItem>& item, _currentItem->children())
    {
        if (item->type() & types)
        {
            _result.append(item.data());
        }
        if ((flags & Recursive) && (item->type() & ComponentItem))
        {
            ReflexModelItem* oldItem = _currentItem;
            _currentItem = item.data();
            childrenByType(types, flags);
            _currentItem = oldItem;
        }
    }

    _recursionLevel--;
    return _result;
}

ReflexModelItem* ReflexModelQuery::childByName(const QString& name)
{
    foreach(const QSharedPointer<ReflexModelItem>& item, _root->children())
    {
        if (item->name() == name)
        {
            return item.data();
        }
    }

    return 0;
}

QList<ReflexModelItem*> ReflexModelQuery::subtree(TypeFlags types, ReflexModelQuery::QueryFlags flags)
{
    return QList<ReflexModelItem*>() << _root <<  childrenByType(types, flags);
}


