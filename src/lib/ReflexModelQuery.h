/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _REFLEXMODELQUERY_H
#define _REFLEXMODELQUERY_H

#include <QtCore/QList>

class ReflexModelFragment;
class ReflexModelItem;

/*!
 Helper class to traverse a ReflexmodelFragment.
 */
class ReflexModelQuery
{
public:
    /*!

     */
    enum QueryFlag
    {
        NoFlags = 0,
        Recursive = 1 << 0,
        BuiltIn = 1 << 1
    };
    Q_DECLARE_FLAGS(QueryFlags, QueryFlag);

    ReflexModelQuery(const ReflexModelFragment& model);
    ReflexModelQuery(ReflexModelItem* rootItem);

    ReflexModelItem* childByName(const QString& name);

    QList<ReflexModelItem*> childrenByType(TypeFlags types, QueryFlags flags = NoFlags);
    QList<ReflexModelItem*> subtree(TypeFlags types, QueryFlags flags = NoFlags);

    void reset(ReflexModelItem* rootItem = 0);

private:
    QList<ReflexModelItem*> _result;
    ReflexModelItem* _root;
    ReflexModelItem* _currentItem;
    int _recursionLevel;
};



#endif
