/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef TEMPLATE_H_
#define TEMPLATE_H_

#include <QtCore/QFile>
#include <QtCore/QMap>
#include <QtCore/QStringList>
#include <QtCore/QSharedPointer>

/**
 \brief Intermediate storage for generated code and string replacement engine

 A template takes values and attaches them to a single key. Nested template
 structures are allowed.

 In the end, a template can be rendered with expandValues and the whole content
 can be safed to an output file. Setting an output file is not necessary for
 nested templates that are just expanded.
 */
class Template
{
public:
    void insert(const QString& key, const QString& value);
    void insert(const QString& key, const QStringList& values);
    void insert(const QString& key, const QSharedPointer<Template>& tpl);

    void setOutputFileName(const QString& name);
    void setTemplateFileName(const QString& name);

    QString expandValues();
    void save();

private:
    QStringList contentByKey(const QString& key) const;

    QString _tplFilename;
    QString _outFilename;
    QMap<QString, QStringList> _map;
    QMultiMap<QString, QSharedPointer<Template> > _subTpl;
};

#endif /* TEMPLATE_H_ */
