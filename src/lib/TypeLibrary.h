/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef TYPELIBRARY_H_
#define TYPELIBRARY_H_

#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QRegExp>
#include <QtCore/QString>

#include "ReflexModelFragment.h"

//! Manager class for element and component types.
/*!
 Modelleditor-Sicht
 * Scanne Bibliothek
 * gib ReflexModelFragment für irgendeinen Typen zurück
 * Builtin-ReflexModelIteme enthalten ausschließlich Infos:
 * Type
 * TypeName
 * Name
 * Builtin-flag
 * Inputs, Outputs und Activities)
 * Weitere Konfigurationsoptionen

 * Eingabe: Type<blabla,blabla>
 * Ausgabe: am besten passende Typ

 Zuordnung Model
 */
class TypeLibrary
{
public:
    typedef QMap<QString, ReflexModelFragment> Map;

    enum
    {
        InvalidIndex = -1
    };

    TypeLibrary();

    bool contains(const QString& type) const;

    ReflexModelItem* find(const QString& type) const;
    virtual Map::Iterator insert(const QString& type, const ReflexModelFragment& item);
    virtual void remove(const QString& type);

    ReflexModelFragment instanciate(const QString& type, const QString& name = "", const QString& t1 = "",
                                    const QString& t2 = "", const QString& t3 = "") const;

    class const_iterator : public QMap<QString, ReflexModelFragment>::const_iterator
    {
    public:
        friend class TypeLibary;
        const_iterator(QMap<QString, ReflexModelFragment>::const_iterator);
    };

    const_iterator begin() const;
    const_iterator end() const;

protected:
    void replaceWith(const TypeLibrary& library);

    Map _library;
    QString defaultScope;
};

inline TypeLibrary::const_iterator::const_iterator(
        QMap<QString, ReflexModelFragment>::const_iterator i)
    : QMap<QString, ReflexModelFragment>::const_iterator(i)
{
}

inline TypeLibrary::const_iterator TypeLibrary::begin() const
{
    return _library.constBegin();
}

inline TypeLibrary::const_iterator TypeLibrary::end() const
{
    return _library.constEnd();
}


#endif /* TYPELIBRARY_H_ */
