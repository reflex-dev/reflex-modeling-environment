/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef REFLEXMODELCONNECTION_H_
#define REFLEXMODELCONNECTION_H_

#include <QtCore/QExplicitlySharedDataPointer>
#include <QtCore/QList>
#include <QtCore/QHash>
#include <QtCore/QSharedData>

class ReflexModelItem;

//! \internal
struct ReflexModelConnectionData : public QSharedData
{
    ReflexModelConnectionData();

    ReflexModelItem* destination;
    ReflexModelItem* parent;
    ReflexModelItem* source;
    bool attached;
};

/*!
 \brief Represents a connection between two model items.

 Connection objects are defined by a source and destination item. They are
 typically stored in ReflexModelItem objects.

 Connections can be made between
 - output and input (real connection)
 - input and input (virtual connection)

 Connections objects are either valid connections (source and destination is
 set) or null (ReflexModelConnection::isNull() == true). If a connection is
 null, the only way to make it a valid is, to replace it.

 A connection can be temporarily disabled by calling
 ReflexModelConnection::setAttached(false). This is useful when storing a
 connection on the redo/undo stack while it is hidden from the model fragment.

 ReflexModelConnection is internally shared and therefore produces low
 overhead.
 */
class ReflexModelConnection
{
public:
    //! Creates an empty, detached connection that returns true on ReflexModelConnection::isNull().
    ReflexModelConnection();

    //! Creates a connection object between \a source and \a destination with \a parent as the owner item.
    ReflexModelConnection(ReflexModelItem* parent, ReflexModelItem* source, ReflexModelItem* destination);

    //! Returns the destinating input of the connection
    inline ReflexModelItem* destination() const { return d->destination; }

    //! Returns true if the connection object
    inline bool isAttached() const { return d->attached; }

    //! Returns true if either source or destination is not set.
    inline bool isNull() const { return d->parent == 0; }

    //! Returns the component that holds this connection.
    ReflexModelItem* parent() const;

    //! When \a attached is false, the connection is releases itself from source and destination.
    void setAttached(bool attached);

    //! Re-routes the connection to another \a item.
    void setDestination(ReflexModelItem* item);

    //! Set's \a item as the new source of this connection.
    void setSource(ReflexModelItem* item);

    //! Returns the source item of this connection.
    inline ReflexModelItem* source() const { return d->source; }

    inline bool operator==(const ReflexModelConnection& other) const {	return d == other.d; }
    inline bool operator!=(const ReflexModelConnection& other) const {	return d != other.d; }

    //! Returns true if a connection between \a source and \a destination is semantically allowed.
    static bool isAllowedBetween(ReflexModelItem* source, ReflexModelItem* destination);

private:
    QExplicitlySharedDataPointer<ReflexModelConnectionData> d;

    friend class ReflexModelItem;
    friend class ReflexModelFragment;
    friend uint qHash(const ReflexModelConnection&, uint);
};

inline uint qHash(const ReflexModelConnection& connection, uint seed = 0) { return qHash(connection.d.data(), seed); }

#endif /* REFLEXMODELCONNECTION_H_ */
