/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/

#include "ModelFileRenderer.h"

#include <QtDebug>

QByteArray ModelFileRenderer::content() const
{
    return _content;
}

void ModelFileRenderer::handle(RendererEvent event)
{
    switch (event)
    {
    case ModelBeginEvent:
        echo("# Reflex model file.");
        //begin(_model.root());
        break;
    case ModelEndEvent:
        //end(_model.root());
        break;
    default:
        break;
    }
}

void ModelFileRenderer::begin(Element* element)
{
    QString typeName = element->typeName();
    QString name = element->name();
    bool isBuiltIn = element->value("built-in").toBool();

    switch (element->type())
    {
    case Element::ComponentType:
        echo(QString("touch %1 %2").arg(typeName, name));
        if (!isBuiltIn)
        {
            echo(QString("cd %1").arg(element->name()));
        }
        break;
    case Element::ActivityType:
        echo(QString("touch %1 %2").arg(typeName, name));
        break;
    case Element::InputType:
        if (element->parent() && !element->parent()->isBuiltIn())
        {
            echo(QString("touch %1 %2").arg(typeName, name));
        }
        _pendingInputs.append(element);
        break;
    case Element::OutputType:
        if (element->parent() && !element->parent()->isBuiltIn())
        {
            echo(QString("touch %1 %2").arg(typeName, name));
        }

        if (_model.destination(element))
        {
            _pendingOutputs.append(element);
        }
    default:
        break;
    }

    if (!isBuiltIn)
    {
        QStringList keys = element->keys();
        foreach(const QString& key, keys)
        {
            if ((key == "name") || (key == "type-name"))
            {
                continue;
            }
            QString value = element->value(key).toString();
            echo(QString("set %1 %2").arg(key, value));
        }
    }
}

void ModelFileRenderer::end(Element* element)
{
    bool isBuiltIn = element->value("built-in").toBool();

    switch (element->type())
    {
    case Element::ComponentType:
        if (!isBuiltIn)
        {
            QMutableListIterator<Element*> i(_pendingOutputs);
            while (i.hasNext())
            {
                Element* src = i.next();
                Element* dst = _model.destination(src);
                /* Connections into this component */
                if (((src->parent() == element) || (src->parent() && (src->parent()->parent() == element)))
                        && ((dst->parent() == element) || (dst->parent() && (dst->parent()->parent()
                                                                             == element))))
                {
                    QString srcPath = "/" + src->path().join("/");
                    QString dstPath = "/" + dst->path().join("/");
                    echo(QString("connect %1 %2").arg(srcPath).arg(dstPath));
                    i.remove();
                }
            }

            if (element != _model.root())
            {
                echo("cd ..");
            }
        }
        break;
    case Element::InputType:
    {
        Element* dst = _model.destination(element);
        if (dst)
        {
            echo(QString("connect %1 %2").arg(element->name()).arg(dst->name()));
        }
    }
        break;
    case Element::OutputType:
        break;
    default:
        break;
    }
}

void ModelFileRenderer::echo(const QString& line)
{
    _content.append(line);
    _content.append("\n");
}
