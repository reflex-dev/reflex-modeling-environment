/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _REFLEXMODELFRAGMENT_H
#define _REFLEXMODELFRAGMENT_H

#include "ReflexModelConnection.h"
#include "ReflexModelItem.h"

class ReflexModelFragmentIterator;
class ReflexModelQuery;

/*!
 Reflex model fragment.
 */
class ReflexModelFragment
{
public:
    friend class ReflexModelFragmentIterator;
    friend class ReflexModelQuery;
    friend class TestReflexModelFragment;

    ReflexModelFragment();

    ReflexModelFragment(const ReflexModelFragment& other);

    ReflexModelFragment& operator=(const ReflexModelFragment& other);

    ReflexModelConnection connect(ReflexModelItem* source, ReflexModelItem* destination);

    ReflexModelFragment clone(ReflexModelItem* startItem = 0) const;

    ReflexModelItem* create(ReflexModelItem* parent, ItemTypeFlag type, const QString& typeName, const QString& name = "");

    void disconnect(const ReflexModelConnection& connection);

    //! Adds \a item as a child to \a parent with an optional \a row as destination. The \a item is not cloned.
    virtual void insert(const QSharedPointer<ReflexModelItem>& item, ReflexModelItem* parent, int row = -1);

    //! Adds \a fragment as child to \a parent with an optional \a row as destination. All items of \a fragment are cloned.
    ReflexModelItem* insert(const ReflexModelFragment& fragment, ReflexModelItem* parent, int row = -1);

    //! Moves \a item before \a row in \a parentItem.
    void move(ReflexModelItem* item, ReflexModelItem* parentItem, int row);

    virtual void remove(ReflexModelItem* item);

    ReflexModelItem* root() const;

    const QSharedPointer<ReflexModelItem>& pointerToItem(ReflexModelItem* item) const;

    void replace(ReflexModelItem* oldItem, const QSharedPointer<ReflexModelItem>& newItem);

    void setValue(ReflexModelItem* item, const QString& key, const QVariant& value);

    QString toString(ReflexModelItem* item = 0) const;

    static ReflexModelFragment defaultApplicationModel();

protected:
    QSharedPointer<ReflexModelItem> rootItem;

private:
    static void changeOwner(ReflexModelItem* item, ReflexModelFragment* fragment);
};
#endif
