/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "ReflexModelFragment.h"
#include "ReflexModelQuery.h"

#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QHash>

ReflexModelFragment::ReflexModelFragment()
{

}

ReflexModelFragment::ReflexModelFragment(const ReflexModelFragment& other)
{
    if (!other.rootItem.isNull())
    {
        rootItem = other.clone().rootItem;
        changeOwner(rootItem.data(), this);
    }
}

ReflexModelFragment& ReflexModelFragment::operator=(const ReflexModelFragment& other)
{
    if (!other.rootItem.isNull())
    {
        rootItem = other.clone().rootItem;
        changeOwner(rootItem.data(), this);
    }
    else
    {
        rootItem.clear();
    }

    return *this;
}

ReflexModelConnection ReflexModelFragment::connect(ReflexModelItem* source, ReflexModelItem* destination)
{
    Q_ASSERT(source != 0);
    Q_ASSERT(destination != 0);
    Q_ASSERT(((source->type() == OutputItem) && (destination->type() == InputItem)) || ((source->type() == InputItem) && (destination->type() == InputItem)));

    ReflexModelItem* parent = 0;
    /* Different connections are possible */
    /* src and dst are the same component */
    if (source->parent() == destination->parent())
    {
        parent = source->parent();
    }
    /* src is in a nested component */
    else if (source->parent()->parent() == destination->parent())
    {
        parent = source->parent()->parent();
    }
    /* dst in in a nested component */
    else if (source->parent() == destination->parent()->parent())
    {
        parent = source->parent();
    }
    /* src AND dst are in nested components */
    else if (source->parent()->parent() == destination->parent()->parent())
    {
        parent = source->parent()->parent();
    }
    /* Everything else is not allowed */
    else
    {
        Q_ASSERT(false);
    }

    ReflexModelConnection connection(parent, source, destination);
    parent->_connections.insert(connection);
    return connection;
}

/*!

 1. Create shallow copies of all sub-items
 2. Change parent node for each item
 3. Put cloned children back into items
 4. Clone connections

 Only connections that have both, source and destination in the new subtree
 are considered. Other connections are lost.

 */
ReflexModelFragment ReflexModelFragment::clone(ReflexModelItem* startItem) const
{
    typedef QHash<ReflexModelItem*, QSharedPointer<ReflexModelItem> > LookupTable;
    typedef QList<QSharedPointer<ReflexModelItem> > ItemList;

    if (startItem == 0)
    {
        startItem = rootItem.data();
    }

    Q_ASSERT(startItem->_owner == this);

    LookupTable lookupTable;
    lookupTable.insert(startItem, startItem->clone());

    foreach(ReflexModelItem* item, ReflexModelQuery(startItem).childrenByType(AllItems, ReflexModelQuery::Recursive))
    {
        lookupTable.insert(item, item->clone());
    }

    for (LookupTable::Iterator i = lookupTable.begin(); i != lookupTable.end(); i++)
    {
        ReflexModelItem* oldItem = i.key();
        ReflexModelItem* newItem = i.value().data();

        if (newItem != lookupTable.value(startItem).data())
        {
            newItem->_parent = lookupTable.value(oldItem->parent()).data();
        }
        else
        {
            newItem->_parent = 0;
        }

        for (ItemList::Iterator child = oldItem->_children.begin(); child != oldItem->_children.end(); child++)
        {
            newItem->_children.append(lookupTable.value(child->data()));
        }

        for (QSet<ReflexModelConnection>::Iterator connection = oldItem->_connections.begin(); connection
             != oldItem->_connections.end(); connection++)
        {
            ReflexModelItem* oldSrc = connection->source();
            ReflexModelItem* oldDst = connection->destination();

            if (lookupTable.contains(oldSrc) && lookupTable.contains(oldDst))
            {
                ReflexModelItem* newParent = lookupTable.value(connection->parent()).data();
                ReflexModelItem* newSrc = lookupTable.value(oldSrc).data();
                ReflexModelItem* newDst = lookupTable.value(oldDst).data();

                ReflexModelConnection newConnection(newParent, newSrc, newDst);
                newItem->_connections.insert(newConnection);
            }

        }
    }

    ReflexModelFragment clonedFragment;
    clonedFragment.rootItem = lookupTable[startItem];
    changeOwner(clonedFragment.rootItem.data(), &clonedFragment);
    return clonedFragment;
}

ReflexModelItem* ReflexModelFragment::create(ReflexModelItem* parent, ItemTypeFlag type,
                                             const QString& typeName, const QString& name)
{
    QSharedPointer<ReflexModelItem> item(new ReflexModelItem(type, typeName, name));
    insert(item, parent);
    return item.data();
}

void ReflexModelFragment::disconnect(const ReflexModelConnection& connection)
{
    Q_ASSERT(!connection.isNull());

    ReflexModelItem* parent = connection.parent();
    const_cast<ReflexModelConnection&> (connection).setAttached(false);
    parent->_connections.remove(connection);
}

void ReflexModelFragment::insert(const QSharedPointer<ReflexModelItem>& item, ReflexModelItem* parent,
                                 int row)
{
    Q_ASSERT(item != 0);

    changeOwner(item.data(), this);

    if (parent != 0)
    {
        Q_ASSERT(parent->_owner == this);
        Q_ASSERT(row <= parent->_children.size());

        if (row < 0)
        {
            row = parent->_children.size();
        }
        parent->_children.insert(row, item);
    }
    else
    {
        rootItem = item;
    }

    item->_parent = parent;
}

ReflexModelItem* ReflexModelFragment::insert(const ReflexModelFragment& fragment, ReflexModelItem* parent,
                                             int row)
{
    Q_ASSERT(fragment.root());

    ReflexModelFragment clonedFragment = fragment.clone();
    insert(clonedFragment.rootItem, parent, row);
    return clonedFragment.root();
}

/*!
 \brief Removes the whole subtree starting at \a item.

 deletes connections pointing from this element or child elements outside the tree
 deletes connections pointing into this element or any children from outside tree
 removes all children and this item from the fragment

 Infos to revert operation:
 - parent item
 - item

 */
void ReflexModelFragment::remove(ReflexModelItem* item)
{
    Q_ASSERT(item != 0);

    if (item == rootItem.data())
    {
        rootItem.clear();
        return;
    }
    else
    {
        /* Remove connections pointing outside the subtree */
        QSet<ReflexModelItem*> subItems = ReflexModelQuery(item).childrenByType(AllItems,
                                                                                ReflexModelQuery::Recursive).toSet();
        foreach(ReflexModelItem* subItem, subItems)
        {
            if (!subItem->isConnected())
            {
                continue;
            }

            if (subItem->_destination.isAttached() && !subItems.contains(
                        subItem->_destination.destination()))
            {
                this->disconnect(subItem->_destination);
            }
            else
                while (!subItem->_sources.isEmpty())
                {
                    this->disconnect(*(subItem->_sources.begin()));
                }
        }

        /* Finally delete this item */
        ReflexModelItem* parentItem = item->parent();
        for (int i = 0; i < parentItem->_children.size(); i++)
        {
            if (parentItem->_children.at(i) == item)
            {
                parentItem->_children.removeAt(i);
                break;
            }
        }
    }
}

ReflexModelItem* ReflexModelFragment::root() const
{
    return rootItem.data();
}

void ReflexModelFragment::setValue(ReflexModelItem* item, const QString& key, const QVariant& value)
{
    Q_ASSERT(item != 0);

    item->setValue(key, value);
}

QString ReflexModelFragment::toString(ReflexModelItem* item) const
{
    Q_ASSERT(item != 0);

    QString result;
    foreach(const QSharedPointer<ReflexModelItem>& childItem, item->_children )
    {
        result += QString("Node 0x%1 -> 0x%2 (%3)\n").arg(reinterpret_cast<quint64> (item), 0, 16).arg(
                    reinterpret_cast<quint64> (childItem.data()), 0, 16).arg(childItem->name());
    }

    return result;
}

ReflexModelFragment ReflexModelFragment::defaultApplicationModel()
{
    ReflexModelFragment fragment;
    QSharedPointer<ReflexModelItem> app = QSharedPointer<ReflexModelItem> (
                new ReflexModelItem(ComponentItem, "NodeConfiguration", "app"));
    fragment.insert(app, 0);
    return fragment;
}

/*!

 */
void ReflexModelFragment::move(ReflexModelItem* item, ReflexModelItem* parentItem, int row)
{
    if (item->parent() == parentItem)
    {
        int oldRow = item->parent()->children().indexOf(item);
        parentItem->_children.move(oldRow, row);
    }
    else
    {
        QSharedPointer<ReflexModelItem> backupItem = pointerToItem(item);
        ReflexModelFragment::remove(item);
        ReflexModelFragment::insert(backupItem, parentItem, row);
    }
}

const QSharedPointer<ReflexModelItem>& ReflexModelFragment::pointerToItem(ReflexModelItem* item) const
{
    Q_ASSERT(item != 0);

    if (item == root())
    {
        return rootItem;
    }
    else
    {
        for (int i = 0; i < item->parent()->_children.size(); i++)
        {
            if (item->parent()->_children.at(i) == item)
            {
                return item->parent()->_children.at(i);
            }
        }
    }

    Q_ASSERT(false); // this should never happen
}

/*
 This method has two cases:
 Trivial: oldItem is the root item and therefore just
 needs to be replaced.

 Not trivial: oldItem is a normal item
 - remove old item completely
 - insert new item

 */
void ReflexModelFragment::replace(ReflexModelItem* oldItem,
                                  const QSharedPointer<ReflexModelItem>& newItem)
{
    Q_ASSERT(newItem.isNull() == false);
    Q_ASSERT(newItem->_owner != this);

    // trivial case: remove whole tree
    if ((oldItem == root()) || (oldItem == 0))
    {
        changeOwner(newItem.data(), this);
        newItem->_parent = 0;
        rootItem = newItem;
    }
    else
    {
        ReflexModelItem* parent = oldItem->parent();
        int row = oldItem->parent()->children().indexOf(oldItem);
        this->remove(oldItem);
        this->insert(newItem, parent, row);
    }
}

void ReflexModelFragment::changeOwner(ReflexModelItem* item, ReflexModelFragment* fragment)
{
    foreach(ReflexModelItem* child, ReflexModelQuery(item).childrenByType(AllItems, ReflexModelQuery::Recursive))
    {
        child->_owner = fragment;
    }
    item->_owner = fragment;
}


