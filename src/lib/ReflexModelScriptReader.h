/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _REFLEXMODELSCRIPTREADER_H
#define _REFLEXMODELSCRIPTREADER_H

#include <QtCore/QRegExp>
#include <QtCore/QStack>
#include <QtCore/QStringList>

#include "ErrorHandler.h"
#include "ReflexModelFragment.h"
#include "ReflexModelQuery.h"

class TypeLibrary;

class ReflexModelScriptReader: public ErrorHandler
{
public:
    ReflexModelScriptReader();
    ReflexModelScriptReader(const ReflexModelScriptReader& other);
    TypeLibrary* library() const;
    ReflexModelFragment model() const;
    void setLibrary(TypeLibrary* library);
    void setModelFile(const QString& fileName);

    static QStringList importPaths;

private:
    struct Context
    {
        enum Mode
        {
            Unknown,
            Instanciation,
            Definition
        };

        Mode mode;
        QSharedPointer<ReflexModelItem> item;
        QString indent;
    };

    void parseLine(const QString& line);

    ReflexModelItem* cd(ReflexModelItem* parent, const QString& path);
    void enterContext(const QString& indent);
    void leaveContext();

    QStack<Context> contexts;
    Context recentContext;
    TypeLibrary* _library;
    quint32 line;
    ReflexModelFragment _model;
    QString _fileName;
    QStringList visitedFiles;

    static QRegExp connectionReg;
    static QRegExp definitionReg;
    static QRegExp importReg;
    static QRegExp instanciationReg;
    static QRegExp keyValueReg;
};

inline TypeLibrary* ReflexModelScriptReader::library() const
{
    return _library;
}

inline ReflexModelFragment ReflexModelScriptReader::model() const
{
    return _model;
}

#endif
