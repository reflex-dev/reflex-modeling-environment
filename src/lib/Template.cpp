/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "Template.h"

#include <QtCore/QRegExp>
#include <QtCore/QTextStream>

void Template::save()
{
    Q_ASSERT(!_outFilename.isEmpty());
    QFile _outFile(_outFilename);
    Q_ASSERT(_outFile.open(QIODevice::WriteOnly));
    QTextStream out(&_outFile);

    out << expandValues();
}

QString Template::expandValues()
{
    if (_tplFilename.isEmpty())
    {
        return "";
    }

    Q_ASSERT_X(QFile::exists(_tplFilename),
               "expandValues()",
               qPrintable(QString("File '%1' does not exist.").arg(_tplFilename)));
    QFile _tplFile(_tplFilename);
    Q_ASSERT(_tplFile.open(QIODevice::ReadOnly));
    QTextStream in(&_tplFile);

    QStringList result;

    QRegExp fullLinePattern("^([\\s]*)\\$([\\w_-]*)\\$");
    QRegExp inlinePattern("\\$([\\w_-]*)\\$");
    inlinePattern.setMinimal(true);

    while (!in.atEnd())
    {
        QString line = in.readLine();

        // Solely patterns, keep white spaces before them on every line
        if (fullLinePattern.exactMatch(line))
        {
            QString leadingSpace = fullLinePattern.cap(1);
            QString key = fullLinePattern.cap(2);
            QStringList values = contentByKey(key);
            line = leadingSpace + values.join("\n" + leadingSpace);
        }
        // Inline patterns embedded in text. Replace them inline.
        else
        {
            for (int pos = line.indexOf(inlinePattern, 0); pos != -1; pos = line.indexOf(inlinePattern, pos))
            {
                QString key = inlinePattern.capturedTexts().at(1);
                QStringList replacement = contentByKey(key);
                line.replace(pos, inlinePattern.matchedLength(), replacement.join("\n"));
            }
        }

        result.append(line);
    }

    return result.join('\n');
}

QStringList Template::contentByKey(const QString& key) const
{
    QStringList result;

    if (_map.contains(key))
    {
        result = _map.value(key);
    }
    else if (_subTpl.contains(key))
    {
        foreach(const QSharedPointer<Template>& tpl, _subTpl.values(key))
        {
            QString content = tpl->expandValues();
            result.append(content.split("\n"));
        }
    }

    return result;
}

void Template::setTemplateFileName(const QString& name)
{
    _tplFilename = name;
}

void Template::setOutputFileName(const QString& name)
{
    _outFilename = name;
}

void Template::insert(const QString& key, const QString& value)
{
    _map[key].append(value);
}

void Template::insert(const QString& key, const QStringList& values)
{
    _map[key].append(values);
}

void Template::insert(const QString& key, const QSharedPointer<Template>& tpl)
{
    _subTpl.insertMulti(key, tpl);
}
