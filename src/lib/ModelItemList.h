/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef MODELITEMLIST_H_
#define MODELITEMLIST_H_

#include "ModelItemTypes.h"

#include <QtCore/QList>
#include <QtCore/QSharedPointer>

class ReflexModelItem;

class ModelItemList: public QList<QSharedPointer<ReflexModelItem> >
{
public:
    QList<ReflexModelItem*> byType(TypeFlags types) const;
    ReflexModelItem* byName(const QString& name) const;
    int indexOf(ReflexModelItem* item, int from = 0) const;

private:

};

#endif /* MODELITEMLIST_H_ */
