import qbs 1.0

StaticLibrary {
    name: "common"

    Depends { name : "Qt.core" }
    Depends { name : "cpp" }

    cpp.defines : [
        "RME_VERSION=" + '"' + project.version + '"'
    ]

    files: [
        "ModelItemList.cpp",
        "ErrorHandler.*",
        "ReflexModelConnection.cpp",
        "ReflexModelFragment.cpp",
        "ReflexModelFragmentIterator.cpp",
        "ReflexModelItem.cpp",
        "ReflexModelPath.cpp",
        "ReflexModelQuery.cpp",
        "ReflexModelScriptReader.cpp",
        "ReflexModelWriter.cpp",
        "Template.cpp",
        "TypeLibrary.cpp",

    ]

    Group {
        files: "/*.h"
        qbs.install: true
        qbs.installDir: "include/"
    }

    Export {
        Depends { name : "cpp" }
        cpp.includePaths : "./"
    }
}
