/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef _REFLEXMODELFRAGMENTITERATOR_H
#define _REFLEXMODELFRAGMENTITERATOR_H

#include <QtCore/QFlags>
#include <QtCore/QList>

class ReflexModelItem;
class ReflexModelFragment;

/*!

 */
class ReflexModelFragmentIterator
{
public:
    /*!

     */
    enum Flag
    {
        Activities = 1 << 0, Components = 1 << 1, Inputs = 1 << 2, Outputs = 1 << 3, AllTypes = Activities
        | Inputs | Outputs | Components, Recursive = 1 << 4, BuiltIn = 1 << 5

    };

    /*!

     */
    typedef QFlags<Flag> Flags;

    ReflexModelFragmentIterator(ReflexModelItem* rootItem, Flags flags);

    ReflexModelItem* item() const;

    ReflexModelItem* operator *() const;

    ReflexModelFragmentIterator & operator ++();

private:
    ReflexModelFragment* model;
    ReflexModelItem* _root;
    Flags _flags;
    QList<ReflexModelItem*> _items;
    ReflexModelItem* currentItem;
};
#endif
