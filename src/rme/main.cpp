/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "CodeGenerator.h"

#include <QtCore/QDebug>
#include <QtCore/QCommandLineOption>
#include <QtCore/QCommandLineParser>
#include <QtCore/QTimer>
#include <QtCore/QCoreApplication>

char** pointerToName;
char* originalName;
QByteArray applicationName;


void setupApplicationNameModification(char* argv[])
{
	pointerToName = &argv[0];
	originalName = argv[0];
	applicationName = originalName;
}

void setApplicationName(const char* name)
{
	applicationName = originalName;
	applicationName.append(" ");
	applicationName.append(name);
	*pointerToName = applicationName.data();
}

void restoreApplicationName()
{
	*pointerToName = originalName;
}

int main(int argc, char *argv[])
{
	setupApplicationNameModification(argv);

	QCoreApplication app(argc, argv);
	QCoreApplication::setOrganizationName("reflex");
	QCoreApplication::setApplicationName("reflex-modeling-environment");
	QCoreApplication::setApplicationVersion(RME_VERSION);

	QCommandLineParser parser;

	parser.addPositionalArgument("command",
			"The command to execute. Available are: codegen, version.",
			"COMMAND");
	parser.addHelpOption();

	parser.parse(QCoreApplication::arguments());
	const QStringList args = parser.positionalArguments();
	const QString command = args.isEmpty() ? QString() : args.first();

	if (command == "codegen")
	{
		setApplicationName("codegen");
	    CodeGenerator generator;
	    generator.parseArguments();
	    QTimer::singleShot(0, &generator, SLOT(start()));
	    return app.exec();
	}
	else if (command == "version")
	{
		fprintf(stderr, "%s\n", RME_VERSION);
		return 0;
	}
	else
	{
		parser.showHelp();
	}

	restoreApplicationName();
}


