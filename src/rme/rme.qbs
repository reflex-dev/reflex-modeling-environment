import qbs 1.0

Application {
    name: "rme"

    Depends { name : "Qt.core" }
    Depends { name : "cpp" }

    Depends { name : "common" }
    Depends { name : "codegen" }

    cpp.defines : [
        "RME_VERSION=" + '"' + project.version + '"'
    ]

    files: [
        "main.cpp"
    ]

    Group {
        fileTagsFilter : "application"
        qbs.install : true
        qbs.installDir : "bin"
    }
}
