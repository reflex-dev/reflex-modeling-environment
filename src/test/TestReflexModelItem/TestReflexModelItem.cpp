#include "TestReflexModelItem.h"
#include "ReflexModelItem.h"

#include <QtCore/QScopedPointer>


void TestReflexModelItem::init()
{
	component = new ReflexModelItem(ComponentItem, "TestComponent", "testComponent");
	input = new ReflexModelItem(InputItem, "TestInput", "testInput");
}

void TestReflexModelItem::initMembers()
{
	QVERIFY(component->_owner == 0);
	QVERIFY(component->_parent == 0);
}

void TestReflexModelItem::copy()
{
	QScopedPointer<ReflexModelItem> component2(new ReflexModelItem(*component));
	QVERIFY(component->type() == component2->type());
	QVERIFY(component->name() == component2->name());
	QVERIFY(component->value("type-name") == component2->value("type-name"));
}

void TestReflexModelItem::metadata()
{
	component->setValue("testValue", "test");
	QVERIFY(component->value("testValue").toString() == "test");
}

void TestReflexModelItem::cleanup()
{
	delete component;
	delete input;
}

QTEST_MAIN(TestReflexModelItem);

