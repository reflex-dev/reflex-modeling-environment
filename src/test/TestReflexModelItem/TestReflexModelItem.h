#ifndef TESTELEMENT_H_
#define TESTELEMENT_H_

#include <QtTest/QtTest>


class ReflexModelItem;

class TestReflexModelItem: public QObject
{
Q_OBJECT

private slots:
	void init();
	void initMembers();
	void copy();
	void metadata();
	void cleanup();
private:
	ReflexModelItem* component;
	ReflexModelItem* input;
};

#endif /* TESTELEMENT_H_ */
