#include "ReadOnlyReflexModel.h"
#include "ReflexModelFragment.h"
#include "ReflexModelItem.h"
#include "TestReadOnlyReflexModel.h"
#include "modeltest.h"

#include <QtCore/QScopedPointer>
#include <QtDebug>

typedef QSharedPointer<ReflexModelItem> ItemPointer;

void TestReadOnlyReflexModel::modeltest()
{
	ReadOnlyReflexModel model;
	//ModelTest(&model, this);
}

void TestReadOnlyReflexModel::setAndGetModel()
{
	ReflexModelFragment fragment;
	ItemPointer item = ItemPointer(new ReflexModelItem(ComponentItem, "Test", "test"));
	fragment.insert(item, 0);
	ReadOnlyReflexModel model;
	model.setModel(fragment);
	QVERIFY(model.root());
	QVERIFY(model.root()->name() == "test");
}

QTEST_MAIN(TestReadOnlyReflexModel)
