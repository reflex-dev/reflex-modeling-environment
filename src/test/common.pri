TARGET = test

OBJECTS_DIR = .obj
MOC_DIR = .moc
UI_DIR = .moc
RCC_DIR = .moc

CONFIG -= debug_and_release
CONFIG += release
CONFIG -= debug

QT = \
    core \
    testlib
    

LIBS += \
    -L$$PWD/../generator -lgenerator \
    -L$$PWD/../lib -lrme
     
PRE_TARGETDEPS += \
    ../../lib/librme.a \
    ../../generator/libgenerator.a \
    
    
INCLUDEPATH += \
    ../../lib \
    ../../editor \
    ../../generator