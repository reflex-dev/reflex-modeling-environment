#include "TestReflexModelFragment.h"
#include "ReflexModelItem.h"

#include <QtDebug>

void TestReflexModelFragment::init()
{
	compItem = QSharedPointer<ReflexModelItem> (
			new ReflexModelItem(ComponentItem, "NodeConfiguration", "app"));
	inItem = QSharedPointer<ReflexModelItem> (
			new ReflexModelItem(InputItem, "SingleValue1<uint8>", "in"));
	actItem = QSharedPointer<ReflexModelItem> (
			new ReflexModelItem(ActivityItem, "Activity", "act"));
	outItem = QSharedPointer<ReflexModelItem> (new ReflexModelItem(OutputItem, "Sink0", "out"));
}

void TestReflexModelFragment::emptyRoot()
{
	ReflexModelFragment fragment;
	QVERIFY(fragment.root() == 0);
}

void TestReflexModelFragment::insertRoot()
{
	ReflexModelFragment fragment;
	fragment.insert(compItem, fragment.root());
	QVERIFY(fragment.root() == compItem);
	QVERIFY(compItem->owner() == &fragment);
	QVERIFY(compItem->parent() == 0);
}

void TestReflexModelFragment::insertSingleItems()
{
	ReflexModelFragment fragment;
	fragment.insert(compItem, 0);

	fragment.insert(inItem, compItem.data());
	QVERIFY(inItem->_parent == compItem);

	fragment.insert(actItem, compItem.data());
	QVERIFY(actItem->_parent == compItem);
}

void TestReflexModelFragment::connect()
{
	ReflexModelFragment fragment;
	fragment.insert(compItem, 0);
	fragment.insert(inItem, compItem.data());
	fragment.insert(actItem, compItem.data());
	fragment.insert(outItem, compItem.data());
	ReflexModelConnection connection = fragment.connect(outItem.data(), inItem.data());

	QVERIFY(compItem->_connections.size() == 1);
	QVERIFY(compItem->_connections.contains(connection));
	QVERIFY(connection.parent() == compItem.data());
	QVERIFY(connection.source() == outItem.data());
	QVERIFY(connection.destination() == inItem.data());

	QVERIFY(inItem->isConnected());
	QVERIFY(outItem->isConnected());
	QVERIFY(outItem->_destination == connection);
	QVERIFY(inItem->_sources.contains(connection));
}

void TestReflexModelFragment::clone()
{
	ReflexModelFragment fragment;
	fragment.insert(compItem, 0);
	fragment.insert(inItem, compItem.data());
	fragment.insert(actItem, compItem.data());
	fragment.insert(outItem, compItem.data());
	fragment.connect(outItem.data(), inItem.data());

	ReflexModelFragment fragment2 = fragment.clone(fragment.root());

	QVERIFY(fragment.root() != fragment2.root());
	QVERIFY(fragment2.root()->owner() == &fragment2);
	QVERIFY(isSimilar(fragment.root(), fragment2.root()));

	// check connections as well
	QVERIFY(fragment.root()->children().byName("out")->_destination != fragment2.root()->children().byName("out")->_destination);
	QVERIFY(fragment2.root()->children().byName("in")->_sources.contains(fragment2.root()->children().byName("out")->_destination));
	QVERIFY(fragment2.root()->children().byName("out")->_destination.destination() == fragment2.root()->children().byName("in"));
	QVERIFY(fragment2.root()->children().byName("in")->_sources.begin()->source() == fragment2.root()->children().byName("out"));
}


//void TestReflexModelFragment::remove()
//{
//	ReflexModelFragment fragment = createFragment();
//	ReflexModelFragment fragment2 = fragment;
//
//	// Remove single element
//	ReflexModelFragment backup = fragment.remove(inItem);
//	QVERIFY(backup.items.size() == 1 );
//	QVERIFY(backup.items.begin().value() == inItem );
//	QVERIFY(backup.items.values(backup.root()).size() == 0);
////	QVERIFY(backup.connections.size() == 1);
////	QVERIFY(backup.connections.value(inItem) == actItem);
//
//	// Remove whole tree
//	backup = fragment2.remove(fragment2.root());
//	QVERIFY(backup.items.size() == 4 );
//	QVERIFY(backup.root() != 0 );
//	QVERIFY(backup.items.values(backup.root()).size() == 3);
////	QVERIFY(backup.connections.size() == 2);
//}

ReflexModelFragment TestReflexModelFragment::createFragment()
{
	ReflexModelFragment fragment;
	fragment.insert(compItem, 0);
	fragment.insert(inItem, compItem.data());
	fragment.insert(actItem, compItem.data());
	fragment.insert(outItem, compItem.data());
	return fragment;
}

bool TestReflexModelFragment::isSimilar(const ReflexModelItem* item1, const ReflexModelItem* item2)
{
	if (item1->_data != item2->_data)
	{
		return false;
	}

	if (item1->_children.size() != item2->_children.size())
	{
		return false;
	}

	if (item1->_connections.size() != item2->_connections.size())
	{
		return false;
	}

	return true;
}

QTEST_MAIN(TestReflexModelFragment)
