#ifndef TESTREFLEXMODELFRAGMENT_H_
#define TESTREFLEXMODELFRAGMENT_H_

#include <QtTest/QtTest>
#include "ReflexModelFragment.h"


class TestReflexModelFragment: public QObject
{
Q_OBJECT

private slots:
//	void init();
	void init();

	void emptyRoot();
	void insertRoot();
	void insertSingleItems();
	void connect();
	void clone();

//	void copyConstructor();
//	void remove();

private:
	ReflexModelFragment createFragment();

	static bool isSimilar(const ReflexModelItem* item1, const ReflexModelItem* item2);

	QSharedPointer<ReflexModelItem> compItem;
	QSharedPointer<ReflexModelItem> actItem;
	QSharedPointer<ReflexModelItem> inItem;
	QSharedPointer<ReflexModelItem> outItem;

};

#endif /* TESTREFLEXMODELFRAGMENT_H_ */
