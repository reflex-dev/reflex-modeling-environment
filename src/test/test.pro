TEMPLATE = subdirs

CONFIG += \
	ordered

SUBDIRS = \
    TestReflexModelFragment \
    TestReflexModelItem \
    TestReadOnlyReflexModel \
    TestReflexModelScriptReader