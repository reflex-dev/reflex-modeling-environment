Usage
=====
Rme is a all-in-one tool and provides several sup-programs:

:codegen:   Code generator for components and applications
:version:   Shows the current version


Code Generation
---------------
A code generator for reflex applications. It creates C++ headers and sources,
one for each run. Depending on the command, it creates model headers, main-
or startup sources.

.. figure:: images/codegen-process.png

   The code generation process.

Model headers contain and initialize all objects related to the event-flow as
there are inputs, outputs, nested components and connections. These files are
not supposed to be edited by the user. Furthermore, user-code is dervied from
such classes and only one method for each activity has to be implemented. The
main idea behind this splitted approach is to keep structural code seperate
from the behavioral code which is likely to be modified by the programmer and
not specified in the model.

.. figure:: images/model-uml-structure.png

   Model-classes follow the curiously recurring template pattern to call
   activity methods in the user-code without the overhead of virtual method
   calls.

The code generator does currently not support any optimizations to the model.


Integration into the REFLEX Build Process
-----------------------------------------
The rme codegenerator integrates seamlessly into the Qbs Build System which
is used by the REFLEX framework. But it can be used with any other build
tool as well. Qbs integration is provided by a module `rme` which is installed
along with the Reflex Modeling Environment.

The following example shows, how a typical Qbs file would look like:

.. code-block:: ruby

    import qbs 1.0
    import ReflexProject

    ReflexProject {


    Product {
        type : "application"

        Depends { name : "cpp" }
        Depends { name : "rme" }
        // Add REFLEX dependencies here

        Group {
            files : "MyApplication.rml"
            rme.codegenCommands : [
                "model-header",
                "main-source"
            ]
        }

        files : [
            "MyApplication.cc",
            // Add more files here
        ]
    }
    }

In the above build script, one RML file describes the whole application
structure. Which files are generated, is controlled via the ``codegenCommands``
property of the ``rme`` module. The command ``model-header`` specifies
that `codegen` is invoked to generate a C++ header file containing structure
code for ``MyApplication.rml`` with the name ``model_MyApplication.h``. The
``cpp`` include-path is automatically updated by the build-system.

Generated model headers contain only code for types that have been defined
in the model file. If the model is distributed over several files, each of
them has to be referenced in the build script.


Ideas for more Tools
--------------------
The tool-set could be enhanced easily to make all kind of model modifications
accessible via command line. Also it would be desirable, to read from and write
to stdio which would make the arrangement of pipes possible.

:testgen:   Code generator to create test-suites for components
:extract:   Extracts component definitions from a model file into a seperate file
:check:     Checks a model to meet different constraints
