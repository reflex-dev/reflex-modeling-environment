Setup
=====

System Requirements
-------------------
- Qt library 5.2+
- Qt Build Suite (Qbs) 1.3.0+

Qbs is available stand-alone or as part of QtCreator.


Building Rme
------------
Rme builds using the Qt Build Suite Qbs::

    qbs build profile:<YOUR_QT_PROFILE>

No configuration options are provided.


Installing Rme
--------------
Qbs can be installed to any folder and executed from there.

    qbs install --install-root <PATH/TO/INSTALLROOT>

After installing Rme, You likely want to update Your local ``qbs``
configuration with the new install location::

    qbs config preferences.qbsSearchPaths "/PATH/TO/INSTALL_ROOT/bin:PATH/TO/REFLEX"

Or if per-profile search-paths are used::

    qbs config profiles.<YOUR_PROFILE>.preferences.qbsSearchPaths "/PATH/TO/INSTALL_ROOT/bin:PATH/TO/REFLEX"

That's it. You can now use Rme to generate code and include it into a Qbs
build work-flow.
