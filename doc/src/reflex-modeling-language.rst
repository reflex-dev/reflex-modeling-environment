REFLEX Modeling Language (RML)
==============================
The REFLEX modeling language (RML) is a domain specific modeling language to
describe the structure of REFLEX applications. The following code represents a
small REFLEX application and will be used to describe all language features:

.. code-block:: ruby

    Reflex model file version 0.2

    import reflex
    import VirtualTimer

    NodeConfiguration (reflex::System):
        # Inputs
        reflex::Event generateValue
        reflex::SingleValue2<int16, int16> printResult

        # Outputs
        reflex::Sink1<uint8> out_value

        # Instanciation of nested components
        reflex::VirtualTimer timeoutTimer

        # Definition of a nested component
        Converter converter:
            reflex::SingleValue1<uint8> rawValue
            reflex::Sink2<int16, int16> out_result

        # Connections
        timeoutTimer.out --> generateValue
        out_value --> converter.rawValue
        converter.out_result --> printResult


Basic Concepts
--------------
RML is a domain-specific modeling language and aims to ease and speed up 
development of REFLEX applications. The used text-format provides optimal
readability, is very efficient and works well together with version control
systems. RML directly reflects the application structure with a component tree
and connections in between. Items in this tree are components, inputs and
outputs. They can be instanciated from a model-library or defined in-line.
The former is useful for already built-in components whereas the latter might
be only used for application-specific components. Every model item can be
customized uing key-value pairs.

Types in the model map to C++ classes in the generated code. Therefore only
characters are allowed, that are also valid C++ classes. In addition, double
colons may be used, to express namespaces in C++. The model itself handles
namespaces as part of the type. For example the model type ``reflex::Event``
would map to the class ``Event`` in namespace ``reflex``.

Activities are always implicit, that is, every input is bundled with an
activity of the same name. For each input in the model, the code generator
instanciates one activity of the same name. There is no need to include
activities in the model which is another step towards efficient modeling.

Each hierarchical level represents a component or an other item as there are
inputs or outputs. The root of an application tree is always the
NodeConfiguration component due to the current structure of the REFLEX library.
A new level is is expressed with indentations, at minimum one or more white 
spaces/tabs. Inside a level, the indentation format must not change.

A RML file must begin with the following line to make the parser recognice it
as a valid file:

.. code-block:: ruby

  Reflex model file version 0.2


Comments
--------
Lines beginning with an asterisk ``#`` are recogniced as comments and therefore
are completely ignored:

.. code-block:: ruby

  # This whole line is a comment

Leading spaces are automatically removed. Comments are
not allowed to follow a regular command. Therefore in-line comments are not
accepted.


The ``import`` Directive
------------------------ 
Model items, like components, inputs and special outputs can be defined in
separate files and may be combined into libraries. The model parser may look
in different folders and takes the first file that matches ``MyComponent.rml``:

.. code-block:: ruby

  import MyComponent

It is also possible to group and address files in sub-folders:

.. code-block:: ruby

  import my-module.MyComponent
  
The lookup paths from where files can be imported are set in the tools.


Definitions
-----------
Components and items can be defined in place or seperatly in dedicated files. A
definition contains at least the type it defines, ends with a colon and is
followed by a line with a new indentation level:

.. code-block:: ruby

  MyComponent:
    # A new level begins here
    # Instanciation of inputs and outputs
    # More definitions
    ...
    
When defining in place it is often desired to name the component:

.. code-block:: ruby

  MyComponent name:
    # Inputs and outputs
    ...
    
RML allows simple inheritance to extend an already existing type:

.. code-block:: ruby

  # A file in the library
  BaseComponent:
    Event input
    Sink0 output
    
  # The application file
  MyComponent (BaseComponent) name:
    # More inputs and outputs
    ...


Instanciations
--------------
For re-using an existing component in a model, it has to be instanciated. Any
type that has been imported or defined can be used. The instanciation command
requires a type and name as argument:

.. code-block:: ruby

  Component myInstance
  reflex::Event myInput

Some model types like reflex::SingleValue are templates. They can be
instantiated very similar to C++ code:

.. code-block:: ruby

  reflex::Queue1<reflex::Buffer*> myInput
  reflex::SingleValue3<uint8, int16, uint8> myInput


Connections ``-->``
-------------------
Event channels connect outputs to inputs. They are expressed on component
level and may even address nested components within 1 hierarchy level:

.. code-block:: ruby

  myOutput --> input
  nestedComponent.out_tx --> otherComponent.rx

The number of dashes in this expressions does not matter, so that ``->`` or
``-->`` are both accepted as valid connections. 


Key-Value Pairs
---------------
Each model item contains arbitrary data which is stored as key-value pair in
the model file. This data might be used during analyzation or code generation.
Any text is accepted until the end of line:

.. code-block:: ruby

  Component instance
    # key-value pairs define meta-data
    header-file : reflex/misc/Component.h

Values are inherited from base types and can be overridden. The following keys
are defined:
 
 ================ ========== ==================================================
 Key              Type       Description
 ================ ========== ==================================================
 type-name        String     Item type; often omitted in direct form because of
                             the instanciation or definition line
                             
 name             String     Item name; often omitted in direct form because of
                             the instanciation or definition line
                             
 default-name     String     If no name is specified for instanciation, this one
                             is taken. This is useful for the model editor which
                             supports drag and drop instanciation.
                             
 built-in         boolean    True if the item belongs into the library and no
                             code should be generated for it; otherwise
                             false (default).

 header-file      String     C++ header which has to be included into the
                             generated code in order to use this type.
                             
 template         int        Parameter count if the type is a template. By
                  {0..3}     default this is 0 for all types.
                             If this value is set, the component must be 
                             instanciated as template.
                             
 T1, T2, T3       String     Template parameters. If these values are set for
                             template type, then they serve as default
                             paremters during instanciation. Otherwise they are
                             ignored.
                             
                             These values are often omitted and rather set
                             during instanciation.
                             
 connect          String     Name of the method which connects the output.
                             This is helpful for library components which do 
                             not obey the default naming ``set_out_XX(..)``
                             scheme.
                             
 priority         int        Priority of a component which is assigned to all
                             of its activities. For built-in component types,
                             where the internal structure is not known,
                             codegen assumes that a ``setPriority()`` exists.
                             
                             This value is only considered when `scheduling` 
                             is set to a priority scheduling scheme.
                             
 timeslot         int        Assigns an activity to (multiple) timeslots when
                             using `TimeTriggeredScheduling`. Timeslot is
                             configured for input items in the model since
                             this always implies an activity.
                             ``timeslot`` can be a single value or a list of
                             multiple values. Examples:

                             .. code-block:: ruby
                             
                               NodeConfiguration app:
                                   scheduling : TimeTriggeredScheduling
                                   
                                   Event singleEvent
                                       timeslot : 0
                                       
                                   Event multiEvent
                                       timeslot : 0 10 20
                               
                             
                             
                             
 scheduling       Enum       Defines the scheduling scheme for the application.
                             Valid values are `FifoScheduling` (default),
                             `PriorityScheduling`, 
                             `NonpreemptivePriorityScheduling`, 
                             `SimplePriorityScheduling`, 
                             `TimeTriggeredScheduling`. This entry is only
                             considered for the root component.
                             
 ================ ========== ==================================================

Undefined key-value pairs are parsed and included into the model but do not
have any effect.
