Command-Line Reference
======================

Rme
---
::

    Usage: rme [options] COMMAND

    Options:
      -h, --help  Displays this help.

    Arguments:
      command     The command to execute. Available are: codegen, version.


Codegen
-------
::

    Usage: rme [options] COMMAND

    Options:
      -h, --help  Displays this help.

    Arguments:
      command     The command to execute. Available are: codegen, version.
    ~/Projekte/reflex/reflex-modeling-environment(master) $ rme codegen --help
    Usage: rme codegen [options] command file

    Codegen translates reflex model files into code. Implemented commands are:
    - model-header   : Structural header file containing nested components,
                       inputs, activities, outputs, get-/set-methods
    - header-stub    : An empty component header file deriving from a model-
                       header. It contains empty methods for activities and can.
                       be used as a starting-point for implementation.
    - source-stub    : An empty component source file related to a header-stub.
    - main-source    : A main.cc file setting up a reflex::mcu::CoreApplication
                       object and creating an application object on the stack.
                       The scheduler is also started.
    - startup-source : Creates a startup.cc file which initializes all used
                       core threads and connects cross-core event-channels. This
                       can not be done in the main.xc file. This command is
                       available for the xcore architecture only.

    Options:
      -h, --help                      Displays this help.
      --output, -o <file>             Place the output into <file>
      --template-directory, -t <dir>  Directory from which code templates are being
                                       read
      --import-path, -I <dir>         Add the directory <dir> to the list of direct
                                      ories to be searched for RML files

    Arguments:
      command                         Tells the code generator what to do. Allowed
                                      values are: model-header, header-stub, source
                                      -stub, main-source, startup-source
      file                            Reflex model file
