Reflex Modeling Environment Manual
==================================
The REFLEX Modeling Environment contains several command-line tools that ease
and speed-up application development with the REFLEX framework. It uses the
REFLEX modeling language (RML), a domain-specific language to express the
structure of applications and components.

.. NOTE::
   Please report bugs and suggestions to the
   `Bug Tracker <http://bitbucket.org/reflex-dev/>`_.

.. toctree::
   :maxdepth: 2

   setup
   usage
   reference
