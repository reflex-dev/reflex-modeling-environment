import qbs 1.0

Project {

    property string version : "0.2.0"
    property string relativeSearchPath : "../"

    references: [
        "src/codegen/codegen.qbs",
        "src/lib/lib.qbs",
        "src/rme/rme.qbs",
        "src/transform/transform.qbs",
        "share/share.qbs",
        "tests/blackbox/blackbox.qbs",
        "tests/unit/unit.qbs"
    ]
}
