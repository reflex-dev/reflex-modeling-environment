import qbs 1.0

Product {
    type : "application"
    name : "test_blackbox"

    Depends { name : "cpp" }
    Depends { name : "Qt.core" }
    Depends { name : "Qt.test" }

    files : [
        "test_blackbox.cpp",
        "test_blackbox.h"
    ]

    Group {
        qbs.install : true
        qbs.installDir : "bin"
        fileTagsFilter : "application"
    }

    Group {
        files : "data"
        qbs.install : true
        qbs.installDir : "tests/blackbox/"
    }
}
