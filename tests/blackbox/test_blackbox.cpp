/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "test_blackbox.h"

#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtCore/QProcess>
#include <QtCore/QDebug>
#include <QtCore/QSysInfo>

QString TestRunner::executableFilePath;

TestRunner::TestRunner() : _environment(QProcessEnvironment::systemEnvironment())
{
    isFailureExpected = false;

    exitCode = -1;
}

int TestRunner::exec()
{
    QProcess process;
    process.setProcessEnvironment(_environment);
    process.start(executableFilePath, arguments);
    if (!process.waitForStarted() || !process.waitForFinished(MaxTimeoutMs))
    {
        stdErr = process.readAllStandardError();
        stdOut = process.readAllStandardOutput();
        sanitizeOutput(&stdErr);
        sanitizeOutput(&stdOut);
        if (isFailureExpected == false)
            qDebug("%s", qPrintable(process.errorString()));
        return -1;
    }

    stdErr = process.readAllStandardError();
    stdOut = process.readAllStandardOutput();
    sanitizeOutput(&stdErr);
    sanitizeOutput(&stdOut);
    if ((process.exitStatus() != QProcess::NormalExit
             || process.exitCode() != 0) && (isFailureExpected == false))
    {
        qDebug() << process.arguments();
        if (!stdErr.isEmpty())
            qDebug("%s", stdErr.constData());
        if (!stdOut.isEmpty())
            qDebug("%s", stdOut.constData());
    }
    return (process.exitStatus() == QProcess::NormalExit) ?
                process.exitCode() : -1;
}

void TestRunner::sanitizeOutput(QByteArray* byteArray)
{
#ifdef Q_OS_WIN
    ba->replace('\r', "");
#else
    Q_UNUSED(byteArray);
#endif
}

TestRunner TestRunner::createCodegenTestRunner()
{
    TestRunner test;
    test.arguments << "codegen";
    return test;
}

// Recursive copy
static void cp(const QString& sourceDirPath, const QString& targetDirPath)
{
    QDir currentDir;
    QDirIterator dir(sourceDirPath,
                     QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden);
    while (dir.hasNext())
    {
        dir.next();
        const QString targetPath = targetDirPath
                + QLatin1Char('/')
                + dir.fileName();
        currentDir.mkpath(targetPath);
        cp(dir.filePath(), targetPath);
    }

    QDirIterator file(sourceDirPath, QDir::Files | QDir::Hidden);
    while (file.hasNext())
    {
        file.next();
        const QString targetPath = targetDirPath
                + QLatin1Char('/')
                + file.fileName();
        QFile::remove(targetPath);
        QVERIFY(QFile::copy(file.filePath(), targetPath));
    }
}

QString TestBlackbox::testRootFolder;
QString TestBlackbox::workingDirectory;

void TestBlackbox::initTestCase()
{
    QString executable = QCoreApplication::applicationDirPath() + "/rme";
#ifdef Q_OS_WIN
    executable += QLatin1String(".exe");
#endif
    TestRunner::executableFilePath = QDir::cleanPath(executable);

    testRootFolder = QDir::cleanPath(
                QCoreApplication::applicationDirPath()
                + "/../tests/blackbox/"
    );
    workingDirectory = QDir::currentPath();

    QVERIFY(QDir(workingDirectory + "/data").removeRecursively());
    cp(testRootFolder, workingDirectory);
}

void TestBlackbox::generateModelHeader()
{
    QDir::setCurrent(workingDirectory + "/data/model-header");
    TestRunner test = TestRunner::createCodegenTestRunner();
    test.arguments << "model-header"
                   << "application.rml";

    // Test for default output filename
    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("application.h").exists());
    QVERIFY(QFile("application.h").size() > 0);

    // Test for custom output filename
    test.arguments << "-o" << "custom-application.h";
    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("custom-application.h").exists());
    QVERIFY(QFile("custom-application.h").size() > 0);
}

void TestBlackbox::generateMainSource()
{
    QDir::setCurrent(workingDirectory + "/data/main-source");
    TestRunner test = TestRunner::createCodegenTestRunner();
    test.arguments << "main-source"
                   << "application.rml";

    // Test for default output filename
    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("main.cc").exists());
    QVERIFY(QFile("main.cc").size() > 0);

    // Test for custom output filename
    test.arguments << "-o" << "custom-main.cc";
    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("custom-main.cc").exists());
    QVERIFY(QFile("custom-main.cc").size() > 0);
}

void TestBlackbox::generateStartupSource()
{
    QDir::setCurrent(workingDirectory + "/data/startup-source");
    TestRunner test = TestRunner::createCodegenTestRunner();
    test.arguments << "startup-source"
                   << "application.rml";

    // Test for default output filename
    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("startup.cc").exists());
    QVERIFY(QFile("startup.cc").size() > 0);

    // Test for custom output filename
    test.arguments << "-o" << "custom-startup.cc";
    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("custom-startup.cc").exists());
    QVERIFY(QFile("custom-startup.cc").size() > 0);
}

/*
 Generate code only for components that are defined within the model file.
 */
void TestBlackbox::componentsInFile()
{
    QDir::setCurrent(workingDirectory + "/data/components-in-file");
    TestRunner test = TestRunner::createCodegenTestRunner();
    test.arguments << "model-header"
                   << "application.rml";

    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("application.h").exists());
    QFile contentFile("application.h");
    QVERIFY(contentFile.open(QIODevice::ReadOnly | QIODevice::Text));
    QByteArray content = contentFile.readAll();
    QVERIFY(content.contains("class Application"));
    QVERIFY(!content.contains("class ExternalComponent"));
}

void TestBlackbox::addIncludeForheaderFile()
{
    QDir::setCurrent(workingDirectory + "/data/header-file-include");
    TestRunner test = TestRunner::createCodegenTestRunner();
    test.arguments << "model-header"
                   << "application.rml";

    QCOMPARE(test.exec(), 0);
    QVERIFY(QFile("application.h").exists());
    QFile contentFile("application.h");
    QVERIFY(contentFile.open(QIODevice::ReadOnly | QIODevice::Text));
    QByteArray content = contentFile.readAll();
    QVERIFY(!content.contains("#include \"\""));
    QVERIFY(!content.contains("#include \".h\""));
    QVERIFY(!content.contains("#include \"Undefined.h\""));
    QVERIFY(content.contains("#include \"Implicit.h\""));
    QVERIFY(content.contains("#include \"ExplicitHeader.h\""));
}

void TestBlackbox::importPaths()
{
    QDir::setCurrent(workingDirectory + "/data/import-paths");
    TestRunner test = TestRunner::createCodegenTestRunner();
    test.arguments << "-I" << QDir("sub-absolute").absolutePath()
                   << "-I" << "sub-relative/"
                   << "model-header"
                   << "application.rml";
    QCOMPARE(test.exec(), 0);
}


QTEST_MAIN(TestBlackbox)
