/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#ifndef TEST_BLACKBOX_H_
#define TEST_BLACKBOX_H_

#include <QtTest/QTest>
#include <QtCore/QProcessEnvironment>
#include <QtCore/QStringList>

class TestRunner
{
public:
    TestRunner();
    virtual int exec();

    static TestRunner createCodegenTestRunner();

    // Input parameters
    QStringList arguments;
    bool isFailureExpected;

    // Results
    int exitCode;
    QByteArray stdErr;
    QByteArray stdOut;

    // The test environment is always the same
    static QString executableFilePath;

    enum
    {
        MaxTimeoutMs = 5 * 1000
    };

private:
    static void sanitizeOutput(QByteArray* byteArray);

    QProcessEnvironment _environment;
};

class TestBlackbox: public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void generateModelHeader();
    void generateStartupSource();
    void generateMainSource();
    void componentsInFile();
    void addIncludeForheaderFile();
    void importPaths();

private:
    static QString testFolder();
    static QString rmeExecutableFilePath();


    static QString testRootFolder;
    static QString workingDirectory;
};

#endif
