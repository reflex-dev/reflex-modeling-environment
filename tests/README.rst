Testing
=======

The Reflex Modeling Environment contains multiple test applications.

Blackbox Tests
    run Rme as dedicated application, give some input and test for correct
    output files or exit behaviour. These test cover especially the command
    line interface and the overall code generation.

Unit Tests
    run detailed tests on a certain class. They cover the internal model
    classes and the RML reader.

Manual Integration Tests
    contain REFLEX applications that can be compiled toegther with the
    REFLEX framework. These tests cover the code generation in more detail.


Running Blackbox and Unit Tests
-------------------------------
1. Compile and install Rme::

        /project-dir $ qbs install --install-root /install-root

   This will also compile all blackbox and unit test cases and produce
   binaries for them.

2. Switch to a working-directory

   In order to prevent the pollution of input data, execute the test
   application from within a working-directory which is different from
   ``install-root/bin``::

        ~/project-dir $ cd /working-directory

3. Run the test applications::

        /working-dir $ /install-root/bin/test_unit
        /working-dir $ /install-root/bin/test_blackbox

   The test applications may copy necessary test data into the working
   directory and wipe out old data before starting.


Compiling and Running Manual Integration Tests
----------------------------------------------
1. At first You need a working installation of
   `REFLEX <https://bitbucket.org/reflex-dev/reflex>`_.

2. Make sure, that Qbs is set up correctly, a ``posix`` build profile exists
   and that the ``rme`` module is in Your profile's module search path.
   Have a look into the `Rme documentation
   <http://rme.readthedocs.org/en/master/>`_.

3. Switch to a test application::

        cd tests/manual/singlecore-application

4. Build and install the test application::

        qbs install profile:posix --install-root /tmp/test

5. Run the application::

        /tmp/test/bin/singlecore-application

   The output should look like::

        Test Event: success
        Test SingleValue1: success
        Test SingleValue2: success
        Test SingleValue3: success
        Test Channel Connection: success
        Test Virtual Timer: success


