#ifndef TestVirtualTimer_H
#define TestVirtualTimer_H

#include "model_TestVirtualTimer.h"

class TestVirtualTimer : public model::TestVirtualTimer<TestVirtualTimer>
{
    friend class model::TestVirtualTimer<TestVirtualTimer>;

public:
    TestVirtualTimer();

private:
    void run_event();
};

#endif
