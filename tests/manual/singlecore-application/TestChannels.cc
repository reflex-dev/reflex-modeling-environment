#include <reflex/CoreApplication.h>
#include <stdio.h>
#include "TestChannels.h"

TestChannels::TestChannels()
{
    /* Write something into the event channels */
    out_start()->notify();
}

void TestChannels::run_event()
{
    printf("Test Channel Connection: success\n");
}

