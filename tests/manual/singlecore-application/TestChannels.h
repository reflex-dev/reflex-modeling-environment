#ifndef TEST_CHANNELS_H
#define TEST_CHANNELS_H

#include "model_TestChannels.h"

class TestChannels : public model::TestChannels<TestChannels>
{
    friend class model::TestChannels<TestChannels>;

public:
    TestChannels();

private:
    void run_event();
};

#endif
