#include "TestVirtualTimer.h"
#include <stdio.h>

using namespace reflex;

TestVirtualTimer::TestVirtualTimer()
{
    /* Setup component here */
    timer.set(1000);
}

void TestVirtualTimer::run_event()
{
    printf("Test Virtual Timer: success\n");
}
