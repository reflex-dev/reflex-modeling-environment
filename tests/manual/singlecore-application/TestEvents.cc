#include <reflex/CoreApplication.h>
#include <stdio.h>
#include "TestEvents.h"

TestEvents::TestEvents()
{
    /* Check API functions */
    reflex::Sink0* testChannel = in_event();
    set_out_0(testChannel);

    /* Write something into the event channels */
    in_event()->notify();
    in_data1()->assign(1);
    in_data2()->assign(1, 2);
    in_data3()->assign(1, 2, 3);
}

void TestEvents::run_event()
{
    printf("Test Event: success\n");
}

void TestEvents::run_data1()
{
    printf("Test SingleValue1: ");
    in_data1()->get();
    printf("success\n");
}

void TestEvents::run_data2()
{
    printf("Test SingleValue2: ");
    uint32 v1;
    uint32 v2;
    in_data2()->get(v1, v2);
    printf("success\n");
}


void TestEvents::run_data3()
{
    printf("Test SingleValue3: ");
    uint32 v1;
    uint32 v2;
    uint32 v3;
    in_data3()->get(v1, v2, v3);
    printf("success\n");
}
