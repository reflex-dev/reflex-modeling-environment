import qbs 1.0
import ReflexProject

ReflexProject {

    Product {
        name : "singlecore-application"
        type : "application"

        Depends { name : "cpp" }
        Depends { name : "core" }
        Depends { name : "platform" }
        Depends { name : "buffer" }
        Depends { name : "virtualtimer" }
        Depends { name : "rme" }

        files: [
            "TestChannels.rml",
            "TestChannels.cc",
            "TestEvents.rml",
            "TestEvents.cc",
            "TestVirtualTimer.rml",
            "TestVirtualTimer.cc"
        ]

        Group {
            files : [
                "singlecore-application.rml",
            ]

            rme.codegenCommands : [
                "model-header",
                "main-source",
                "header-stub",
                "source-stub"
            ]
        }

        cpp.includePaths : "."

        Group {
            fileTagsFilter: "application"
            qbs.install: true
            qbs.installDir: "bin"
        }
    }
}
