#ifndef TEST_EVENTS_H
#define TEST_EVENTS_H

#include "model_TestEvents.h"

class TestEvents : public model::TestEvents<TestEvents>
{
    friend class model::TestEvents<TestEvents>;

public:
    TestEvents();

private:
    void run_event();
    void run_data1();
    void run_data2();
    void run_data3();
};

#endif
