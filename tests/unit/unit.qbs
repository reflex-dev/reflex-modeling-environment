import qbs 1.0

Product {
    type : "application"
    name : "test_unit"

    Depends { name : "cpp" }
    Depends { name : "common" }
    Depends { name : "Qt.core" }
    Depends { name : "Qt.test" }

    files : [
        "test_unit.cpp",
        "test_unit.h"
    ]

    Group {
        qbs.install : true
        qbs.installDir : "bin"
        fileTagsFilter : "application"
    }

    Group {
        files : "data"
        qbs.install : true
        qbs.installDir : "tests/unit/"
    }
}
