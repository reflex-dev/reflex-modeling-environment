/****************************************************************************
** Copyright (C) 2014-2015 The REFLEX Developers. See the AUTHORS file at
** the top-level directory of this distribution.
**
** This file is part of the REFLEX Modeling Environment (RME).
**
** BEGIN_LICENSE
** The REFLEX Modeling Environment (RME) is free software: you can
** redistribute it and/or modify it under the terms of the GNU General
** Public License as published by the Free Software Foundation, either
** version 3 of the License, or (at your option) any later version.
**
** RME is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
** or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with RME. If not, see <http:**www.gnu.org/licenses/>.
** END_LICENSE
**
****************************************************************************/
#include "test_unit.h"

#include <ModelItemList.h>
#include <ReflexModelScriptReader.h>
#include <TypeLibrary.h>

#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QDebug>

QString TestUnit::testRootFolder;

void TestUnit::resetAndRead(const QString& modelFilePath,
                            TestUnit::Result expectedResult)
{
    ReflexModelScriptReader::importPaths.clear();
    ReflexModelScriptReader::importPaths.append(QDir::currentPath());

    reader = QSharedPointer<ReflexModelScriptReader>(
                new ReflexModelScriptReader()
    );

    library = QSharedPointer<TypeLibrary>(new TypeLibrary());
    reader->setLibrary(library.data());

    reader->setModelFile(modelFilePath);

    if (expectedResult == Success)
    {
        QVERIFY2((reader->hasErrors() == false),
                 qPrintable(reader->errorString()));
    }
    else if (expectedResult == Fail)
    {
        QVERIFY2((reader->hasErrors() == true),
                 "Reading this file should produce an error");
    }

    model = QSharedPointer<ReflexModelFragment>(
                new ReflexModelFragment(reader->model())
    );
}

void TestUnit::initTestCase()
{
    testRootFolder = QDir::cleanPath(
                QCoreApplication::applicationDirPath()
                + "/../tests/unit/data/"
    );

    QVERIFY(QDir(testRootFolder).exists() == true);
}

void TestUnit::defineCompoundComponent()
{
    resetAndRead(testRootFolder + "/component/compound.rml");

    QVERIFY(model->root() != 0);
    ReflexModelItem* root = model->root();
    QCOMPARE(root->type(), ComponentItem);
    QCOMPARE(root->name(), QString("app"));
    QCOMPARE(root->children().size(), 3);
    ReflexModelItem* parentIn = root->children().byName("in");
    ReflexModelItem* parentOut = root->children().byName("out");
    ReflexModelItem* inner = root->children().byName("inner");
    QVERIFY(parentIn != 0);
    QVERIFY(parentOut != 0);
    QVERIFY(inner != 0);
    QCOMPARE(parentIn->type(), InputItem);
    QCOMPARE(parentOut->type(), OutputItem);
    QCOMPARE(inner->type(), ComponentItem);
    QCOMPARE(parentIn->parent(), root);
    QCOMPARE(parentOut->parent(), root);
    QCOMPARE(inner->parent(), root);
    QCOMPARE(inner->children().size(), 2);
    QVERIFY(parentOut->isConnected());
    QVERIFY(parentIn->isConnected());

    ReflexModelItem* innerIn = inner->children().byName("in");
    ReflexModelItem* innerOut = inner->children().byName("out");
    QVERIFY(innerIn != 0);
    QVERIFY(innerOut != 0);
    QCOMPARE(innerIn->type(), InputItem);
    QCOMPARE(innerOut->type(), OutputItem);
    QCOMPARE(innerIn->parent(), inner);
    QCOMPARE(innerOut->parent(), inner);
    QVERIFY(innerOut->isConnected());
    QVERIFY(innerIn->isConnected());

    QCOMPARE(root->connections().size(), 2);
    foreach (const ReflexModelConnection& con, root->connections())
    {
        QVERIFY(con.isAttached());
        QVERIFY(con.source() != 0);
        QVERIFY(con.destination() != 0);
        if (con.source() == innerOut)
            QCOMPARE(con.destination(), parentIn);
        else if (con.source() == parentOut)
            QCOMPARE(con.destination(), innerIn);
        else
            QFAIL("Connection is illegal");
    }
}

void TestUnit::inheritFromComponent()
{
    resetAndRead(testRootFolder + "/component/inheritance.rml");

    QVERIFY(model->root() != 0);
    ReflexModelItem* root = model->root();
    QCOMPARE(root->type(), ComponentItem);
    QCOMPARE(root->children().size(), 2);
    ReflexModelItem* base = root->children().byName("base");
    ReflexModelItem* inherited = root->children().byName("inherited");
    QVERIFY(base != 0);
    QVERIFY(inherited != 0);
    QCOMPARE(base->type(), ComponentItem);
    QCOMPARE(inherited->type(), ComponentItem);
    QVERIFY(base->children().byName("input") != 0);
    QVERIFY(inherited->children().byName("input") != 0);
    QCOMPARE(base->value("key1").toString(), QString("base"));
    QCOMPARE(base->value("key2").toString(), QString("base"));
    QCOMPARE(inherited->value("key2").toString(), QString("overridden"));
}

void TestUnit::declareAnonymousComponent()
{
    resetAndRead(testRootFolder + "/component/anonymous.rml");

    QVERIFY(model->root() != 0);
    ReflexModelItem* root = model->root();
    QCOMPARE(root->type(), ComponentItem);
    QCOMPARE(root->children().size(), 2);
    ReflexModelItem* i1 = root->children().byName("instance1");
    ReflexModelItem* i2 = root->children().byName("instance2");
    QVERIFY(i1 != 0);
    QVERIFY(i2 != 0);
    QCOMPARE(i1->type(), ComponentItem);
    QCOMPARE(i2->type(), ComponentItem);
    QCOMPARE(i1->value("key1").toString(), QString("base"));
    QCOMPARE(i2->value("key1").toString(), QString("base"));
    QCOMPARE(i2->value("key2").toString(), QString("overridden"));
}

void TestUnit::redeclareComponentFails()
{
    resetAndRead(testRootFolder + "/component/redeclare.rml", Fail);
}

void TestUnit::declareEmptyComponent()
{
    resetAndRead(testRootFolder + "/component/empty.rml");

    QVERIFY(model->root() != 0);
    ReflexModelItem* root = model->root();
    QCOMPARE(root->type(), ComponentItem);
    QCOMPARE(root->value("type-name").toString(), QString("Component"));
    QCOMPARE(root->name(), QString("component"));
}

void TestUnit::useImportedComponent()
{
    resetAndRead(testRootFolder + "/import/application.rml");

    QVERIFY(model->root() != 0);
    ReflexModelItem* root = model->root();
    QCOMPARE(root->type(), ComponentItem);
    QCOMPARE(root->children().size(), 2);
    ReflexModelItem* sublevel = root->children().byName("sublevel");
    ReflexModelItem* external = root->children().byName("external");
    QVERIFY(sublevel != 0);
    QVERIFY(external != 0);
    QCOMPARE(sublevel->type(), ComponentItem);
    QCOMPARE(external->type(), ComponentItem);
}

void TestUnit::keyValuePairs()
{
    resetAndRead(testRootFolder + "/key-value-pairs/application.rml");

    QVERIFY(model->root() != 0);
    ReflexModelItem* root = model->root();
    QCOMPARE(root->type(), ComponentItem);
    QCOMPARE(root->children().size(), 0);
    QCOMPARE(root->name(), QString("overridden"));
    QCOMPARE(root->value("numberValue").toInt(), -47);
    QCOMPARE(root->value("stringValue").toString(),
             QString("stringValue"));
    QCOMPARE(root->value("unescapedString").toString(),
             QString("unescapedString"));
    QCOMPARE(root->value("special-_1").toString(),
             QString("special-_1"));
    QCOMPARE(root->value("tabs").toString(), QString("tabs"));
}

void TestUnit::whiteSpaces()
{
    resetAndRead(testRootFolder + "/format/whitespaces.rml");

    QVERIFY(model->root() != 0);
    ReflexModelItem* root = model->root();
    QCOMPARE(root->type(), ComponentItem);
    QCOMPARE(root->children().size(), 9+2);
}

QTEST_MAIN(TestUnit)
