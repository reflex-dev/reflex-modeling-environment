#include "DisplayDriver.h"
#include <stdio.h>

using namespace examples;
using namespace virtualtimers;
using namespace posix;

using namespace reflex;

DisplayDriver::DisplayDriver()
{
}

void DisplayDriver::run_state()
{
    uint8 state = this->in_state()->get();
    uint8 digits[3];
    digits[0] = state & 0x4 ? '*' : ' ';
    digits[1] = state & 0x2 ? '*' : ' ';
    digits[2] = state & 0x1 ? '*' : ' ';

    Time time = timer.getSystemTime();

    printf("%5u [%c] [%c] [%c]\r", time, digits[0], digits[1], digits[2]);
    fflush(stdout);
}
