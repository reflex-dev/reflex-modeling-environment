import qbs 1.0

// Main project item for the VirtualTimers example application
// and the REFLEX framework.
ReflexProject {
    Product {                           // This is going to be the
        type : "application"             // application binary
        name : "VirtualTimers"

        Depends { name : "cpp" }         // The cpp module is essential

        Depends { name : "core" }        // Needed packages from REFLEX
        Depends { name : "buffer" }
        Depends { name : "devices" }
        Depends { name : "platform" }
        Depends { name : "virtualtimer" }
        Depends { name : "rme" }         // Rreflex modeling environment module

        // ez430chronos platform needs an additional package
        // but only if the example builds for this platform.
        Depends {
            condition : qbs.architecture == "ez430chronos"
            name : "ez430chronos"
        }

        // Platform-independent source and model files
        // An explicit list is used here.
        Group
        {
            prefix : "sources/"
            files : [
                "BlinkApplication.rml",
                "BlinkApplication.cc"
            ]

            // Generate a model-header and a main.cc for the root component.
            rme.codegenCommands : [
                "model-header",
                "main-source"
            ]
        }

        // Platform-dependent source and model files
        // This group item shows the usage of variable folders and
        // wild-card matching
        Group
        {
            prefix : "platform/" + qbs.architecture + "/sources/"
            files : [
                "*.cc",
                "*.rml"
            ]
        }

        // Include paths for the application headers
        cpp.includePaths : [
            "sources",
            "platform/" + qbs.architecture + "/sources"
        ]

        // Import paths for model files
        rme.importPaths : [
            "sources",
            "platform/" + qbs.architecture + "/sources"
        ]

        // This group item applies to files tagged with "application" only
        // which is the application binary. It is marked for installation
        // in the bin folder.
        Group {
            fileTagsFilter: "application"
            qbs.install: true
            qbs.installDir: "bin"
        }
    }
}
