Changelog
=========

Release 0.2.0
-------------
:Release date: 2015-02-xx

- ``codegen`` produces exactly one output file per run. Despite from being
  nice behaviour, this gives a predictable amount of output artifacts
  (always 1). This is important when using the qbs build system.
- ``init`` command has been dropped because the REFLEX directory structure
  has been simplified. Build scripts are not complicated anymore.
- nice documentation is available at http://rme.readthedocs.org
- source code published and licensed under GPL V3
- a test suite now covers basic functionality of the modeling tools


Release 0.1.0
-------------
:Release date: 2014-06-04

- Initial release
