import qbs
import qbs.File

Product {
    name: "rme resources"
    Group {
        files: [
            "rme",
            "qbs"
        ]
        qbs.install: true
        qbs.installDir: "/share"
    }

    Transformer {
        inputs: [ "rme" ]
        Artifact {
            filePath: project.buildDirectory + "/share/rme"
        }
        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = "Copying share/rml to build directory.";
            cmd.highlight = "codegen";
            cmd.sourceCode = function() { File.copy(input.filePath, output.filePath); }
            return cmd;
        }
    }
}
