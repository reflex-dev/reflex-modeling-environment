/***********************************************************************************
Form generated from reading model file '$model-file$'

  Created: $time$
       by: $generator$

WARNING! All changes made in this file will be lost when recompiling model file!
***********************************************************************************/

$includes$

int main()
{
    chan channels[NrOfChannels];

    par
    {
        $startupFunctions$
    }

    return 0;
}
