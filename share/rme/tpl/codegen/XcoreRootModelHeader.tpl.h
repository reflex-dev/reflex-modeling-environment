/***********************************************************************************
Form generated from reading model file '$model-file$'

  Created: $time$
       by: $generator$

WARNING! All changes made in this file will be lost when recompiling model file!
***********************************************************************************/

#ifndef XCOREROOTMODELHEADER_TPL_H
#define XCOREROOTMODELHEADER_TPL_H

#include <xccompat.h>

// Event channels between cores
enum Channels
{
    InvalidChannel = -1,
    $channels$
    NrOfChannels
};

enum
{
    NrOfInstances = $nrOfInstances$
};

// Functions that create and start an application on a core
extern "C" {
    $startupFunctions$
}

// Initialization steps. Each one run's into a synchronization barrier
enum InitStages
{
    EarlyStartup = 1,
    ApplicationCreated,
    ConnectionsReady,
    InitDone
};

#ifdef __cplusplus
$classes$
#endif


#endif // XCOREROOTMODELHEADER_TPL_H
