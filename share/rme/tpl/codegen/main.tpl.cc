/***********************************************************************************
Form generated from reading model file '$model-file$'

  Created: $time$
       by: $generator$

WARNING! All changes made in this file will be lost when recompiling model file!
***********************************************************************************/

$includes$
#include <reflex/CoreApplication.h>

using namespace reflex;
using namespace mcu;

int main()
{
    CoreApplication core;
    $type-name$ $name$;
    CoreApplication::instance()->scheduler()->start();
    return 0;
}
