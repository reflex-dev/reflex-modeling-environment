/***********************************************************************************
Form generated from reading model file '$model-file$'

  Created: $time$
       by: $generator$

WARNING! All changes made in this file will be lost when recompiling model file!
***********************************************************************************/

#ifndef $type$_H
#define $type$_H

$includes$

class $type$ : public model::$type$<$type$>
{
    friend class model::$type$<$type$>;

public:
    $type$();

private:
    $activityStubs$
};

#endif
