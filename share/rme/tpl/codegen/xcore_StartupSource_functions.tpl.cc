
#pragma stackfunction 256
void start_$core$($channels$)
{
    reflex::mcu::CoreApplication coreApp;

    $type-name$ app;

    // Initialize the global application pointer for others
    $pointerName$ = &app;

    // Initialize incoming xlink channels
    $initXlinks$
    barrier.enter(ApplicationCreated);

    // Set up Outbound Connections
    $connections$
    barrier.enter(ConnectionsReady);

    app.sendNotifications();
    barrier.enter(InitDone);

    coreApp.scheduler()->start();
}
