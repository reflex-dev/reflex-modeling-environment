/***********************************************************************************
Form generated from reading model file '$model-file$'

  Created: $time$
       by: $generator$

WARNING! All changes made in this file will be lost when recompiling model file!
***********************************************************************************/
#include <reflex/CoreApplication.h>
#include <reflex/data_types/SimpleStageBarrier.h>
#include <xccompat.h>

$includes$

using namespace reflex;
using namespace xcore;

reflex::xcore::SimpleStageBarrier<NrOfInstances> barrier;

// Every application instance has a global access pointer.
$instancePointers$

$functions$
