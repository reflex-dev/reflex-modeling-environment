$includes$

namespace model
{

template<class T>
class $type$
{
public:
    $type$($parameters$) $initList$
    {
        /* Initializations */
        $initializations$

        /* Connections */
        $connections$
    }

    /* Input getter */
    $inputGets$

    /* Output getter */
    $outputGets$

    /* Output setter */
    $outputSets$

    /* Additional methods */
    void sendNotifications();
    void setPriority(int value);

private:
    /* Activity stubs */
    $activityStubs$

    /* Inputs */
    $inputs$

    /* Outputs */
    $outputs$

public:
    /* Child components */
    $components$

    /* Activities */
    $activities$

};


/*!
\brief Send initial notifications to other components.

Sometimes initial events are inserted into an event channel to stimulate
the application. This is done during application startup. Since all startup
code is completely auto-genrated for the xcore platform, a user hook is
necessary for this. On other platforms, event channel stimulation could
also be done in the application constructor.

This method must be implemented for xcore applications. It doesn't have
any effect on other platforms.
 */
template <class T>
void $type$<T>::sendNotifications()
{
    static_cast<T*>(this)->sendNotifications();
}

/*!
\brief Sets the priority of all activities to \a value.

This method does only have an effect for priority scheduling schemes.
 */
template <class T>
void $type$<T>::setPriority(int value)
{
    $priorities$
}

}
