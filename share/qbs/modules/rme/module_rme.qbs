import qbs 1.0
import qbs.FileInfo
import qbs.File
import qbs.ModUtils


/*!
Module for handling reflex model files.

*/
Module
{
    property path installPath
    property stringList codegenCommands : [ "model-header" ]
    property pathList importPaths : [ "." ]

    Depends { name : "cpp" }

    FileTagger {
        patterns: "*.rml"
        fileTags: ["rml"]
    }

    validate : {
        if (!installPath)
            throw("Module properties rme.installPath not set. ");
    }

    /* Rule for codegen */
    Rule {
        id : compiler
        inputs : ["rml"]

        outputArtifacts : {
            var artifacts = [];
            var commands = input.moduleProperties("rme", "codegenCommands");
            var architecture = product.moduleProperty("qbs", "architecture");

            if (commands.contains("model-header"))
            {
                var fileName = "model_" + input.baseName + ".h";
                artifacts.push({
                    filePath : "generated-files/" + fileName,
                    fileTags : [ "hpp", "model-header" ]
                });
            }
            if (commands.contains("main-source"))
            {
                var suffix = (architecture == "xcore") ? "xc" : "cc";
                var fileTag = (architecture == "xcore") ? "xc" : "cpp";
                var fileName = "model_" + "main." + suffix;
                artifacts.push({
                    filePath : "generated-files/" + fileName,
                    fileTags : [ fileTag, "main-source" ]
                });
            }
            if (commands.contains("startup-source"))
            {
                var fileName = "model_" + "startup.cc";
                artifacts.push({
                    filePath : "generated-files/" + fileName,
                    fileTags : [ "cpp", "startup-source" ]
                });
            }
            if (commands.contains("header-stub"))
            {
                var fileName = input.baseName + ".h";
                artifacts.push({
                    filePath : "generated-files/" + fileName,
                    fileTags : [ "hpp", "header-stub" ]
                });
            }
            if (commands.contains("source-stub"))
            {
                var fileName = input.baseName + ".cc";
                artifacts.push({
                    filePath : "generated-files/" + fileName,
                    fileTags : [ "cpp", "source-stub" ]
                });
            }

            return artifacts;
        }

        outputFileTags : [ "hpp", "cpp", "xc" ]

        prepare: {
            var commandStrings = ModUtils.moduleProperties(input, "codegenCommands");
            var importPaths = ModUtils.moduleProperties(input, "importPaths");

            return commandStrings.map(function(command) {
                var args = [];
                args.push("codegen");
                args.push(command);
                for (var i in importPaths) {
                    args.push("-I");
                    args.push(importPaths[i]);
                }
                args.push("-o");
                args.push(outputs[command][0].filePath);
                args.push(input.filePath);

                var generatorPath = product.moduleProperty("rme", "installPath")
                    + "/rme";

                var cmd = new Command(generatorPath, args);
                cmd.description = "generating file '"
                        + outputs[command][0].fileName
                        + "' from '"
                        + input.fileName
                        + "'";
                cmd.highlight = "codegen";
                return cmd;
            });
        }
    }


    cpp.includePaths : [
        product.buildDirectory + "/generated-files"
    ]
}
